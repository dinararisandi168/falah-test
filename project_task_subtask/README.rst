.. image:: https://itpp.dev/images/infinity-readme.png
   :alt: Tested and maintained by IT Projects Labs
   :target: https://itpp.dev

.. image:: https://img.shields.io/badge/license-MIT-blue.svg
   :target: https://opensource.org/licenses/MIT
   :alt: License: MIT

=======================
Project Task Checklist
=======================

Use subtasks to control your tasks. Be ensure that all your tasks/subtasks are performed and not missed.

Features:

* Added new "Checklist" tab on task's form
* When new subtask is created\changed, message is sent to user that assigned to this subtask
* Only users related to subtask can change subtask parameters
* All subtasks have a certain color, informing about their state
* Each subtask has buttons to switch state:

 * DONE
 * TODO
 * CANCELLED

* Added new "Checklist" menu

 * default filter: "My", "TODO"
 * optional group by: "Project", "Task", "User", "State"

Questions?
==========

To get an assistance on this module contact us by email :arrow_right: help@itpp.dev

Contributors
============
* Rafael Manaev <manawi@it-projects.info>


Further information
===================

Odoo Apps Store: https://apps.odoo.com/apps/modules/12.0/project_task_subtask/


Notifications on updates: `via Atom <https://github.com/it-projects-llc/misc-addons/commits/12.0/project_task_subtask.atom>`_, `by Email <https://blogtrottr.com/?subscribe=https://github.com/it-projects-llc/misc-addons/commits/12.0/project_task_subtask.atom>`_

Tested on `Odoo 12.0  <https://github.com/odoo/odoo/commit/b0b7af93d7287ff9aa9e25deb43f6e52ba231a2a>`_
