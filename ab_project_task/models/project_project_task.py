# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo.addons.ab_role.models.models import _get_uid_from_group
from lxml import etree
from collections import defaultdict
from odoo.exceptions import UserError


class ProjectProject(models.Model):
    # _name = 'project.project'
    _inherit = 'project.project'

    cari = fields.Char(string='Search')
    persentase_progress = fields.Float(string='Progress',store=True, compute="_compute_persentase_progress")
    additional_line = fields.One2many('project.task', 'project_id', string='Tasks Additional', domain=[("is_additional", "=", True)])
    tasks = fields.One2many('project.task', 'project_id', string="Task Activities", domain=[("is_additional", "=", False)])
    is_edit = fields.Boolean(string='Can Create Edit Task', compute="_change_auth", default=False)
    pl_project = fields.Boolean(string='PL This Project', compute="_change_auth", default=False)
    total_progress = fields.Float(string='Total Weight', compute="_amount_total_weigh2")
    pl_pic = fields.Boolean(string='PL and Assign To', compute="_change_auth_task")

    @api.depends('persentase_progress')
    def _compute_persentase_progress(self):
        for rec in self:
            ids = []
            total = 0.0 
            project_id = self.env['project.project'].search([('origin','=', rec.name)])
            for proj in project_id:
                ids.append(proj.id)
                total += proj.persentase_progress
            if len(ids) > 0:
                rec.update({
                    'project_id':[[6,0,ids]],
                    'persentase_progress':total*0,
                    })

    # @api.depends("tasks.weight","tasks.parent_id")
    # def _amount_total_weigh(self):
    #     onechild=[]
    #     weightnyah = 0
    #     for t in self.tasks:
    #         child = self.env['project.task'].search([('id', 'child_of', t.id), ('id', '!=', t.id), ('project_id', '=', self.id)])
    #         jumlah = len(child)
    #         if len(child) == 1:
    #             isi = child.id
    #             onechild.append(isi)
    #         for ortu in onechild:
    #             no_child = self.env['project.task'].search(['|',('parent_id', '=', ortu),('parent_id', '=', False), ('project_id', '=', self.id)])
    #             for i in no_child:
    #                 weightnyah += i.weight
    #         t.project_id.total_progress = weightnyah / len(self.tasks)

    # @api.multi
    # def _message_auto_subscribe(self, vals):
    #     res = super(ProjectProject, self)._message_auto_subscribe(vals)
    #     pass
    #     return res

#     @api.multi
#     def write(self, vals):
# #        raise UserError(('CP Vals %s')%(vals,))
#         res = super(ProjectProject, self).write(vals)
#             def _message_auto_subscribe_notify(self):
#                 pass
#         return res
    
    # @api.depends("tasks.progress")
    # def _progress_update(self):
    #     total_progres = persen = 0
    #     total_bobot = 0
    #     for hasil in self.tasks:
    #         if hasil.progress == 100 and hasil.subtask_count==0:
    #                 total_bobot += hasil.bobot
    #     self.write({"persentase_progress": total_bobot, })

    @api.multi
    def write(self, vals):
#        raise UserError(('CP Vals %s')%(vals,))
        res = super(ProjectProject, self).write(vals)
        return res

    def get_key(self,my_dict,val):
        res = False
        for key, value in my_dict.items():
            if key == val:
                res = value
        return res

    # @api.onchange("tasks")
    # def _amount_progress(self):
    #     total_bobot = 0
    #     for i in self.tasks:
    #         if i.progress == 100:
    #             total_bobot += i.progress
    #     self.write({"persentase_progress": total_bobot, })

    
    @api.depends("tasks.progress", "tasks.weight", "tasks.parent_id",'tasks.distribution')
    def _amount_total_weigh2(self):
        sisa = 100
        total_weight = total_progres = persen = all_task = i_task = 0
        parent_task = []
        over_weight = False
        for xx in self.tasks:
            child_task = self.tasks.filtered(lambda x : x.parent_id==xx.parent_id)
            weight_fix = 0
            ch = 0
            for xxx in child_task:
                if xxx.distribution=='fix':
                    weight_fix += xxx.weight
                    if weight_fix>100:
                        raise UserError(_('Total Weight Melebihi 100%'))
#                    xxx.update({'bobot':  (xxx.weight * xxx.parent_id.weight) /100})
                    ch+=1
            parent_task += [{'parent_id': xx.parent_id.id, 'weight_fix': weight_fix,'count': len(child_task)-ch,}]
            
                
        for line in self.tasks:
#            if line.subtask_count>0:
#                line.write({'bobot': 0})

            if line.distribution=='prorate':
                for p_task in parent_task:
                    if p_task['parent_id']==line.parent_id.id:
                        prorate = (100 - p_task['weight_fix'])/ p_task['count']
                        line.write({'weight': prorate})
#                        break
#                    raise UserError(('child %s \n p_task %s')%(parent_task,p_task))
            if line.weight <0:
                line.weight = 0
                raise UserError(_('Total Weight tidak boleh Minus'))

            # if total_weight>100:
            #     raise UserError(_('Total Weight Melebihi 100%'))
        total_bobot = 0
        for hasil in self.tasks:
            if hasil.subtask_count==0:
                total_bobot += hasil.bobot
                if hasil.progress == 100:
                    total_progres += hasil.bobot
        self.total_progress = total_bobot
        if total_progres > 0:
            persen = total_progres/total_bobot
        else:
            persen = 0
        self.write({"persentase_progress": persen*100, })
        


    @api.onchange("tasks")
    def check_weight_parent(self):
        sisa = 100
        total_weight = total_progres = persen = all_task = i_task = 0
        parent_task = []
        for xx in self.tasks:
            child_task = self.tasks.filtered(lambda x : x.parent_id==xx.parent_id)
            weight_fix = 0
            ch = 0
            for xxx in child_task:
                if xxx.distribution=='fix':
                    weight_fix += xxx.weight
                    if weight_fix>100:
                        raise UserError(_('Total Weight Melebihi 100%'))
                    ch+=1
            parent_task += [{'parent_id': xx.parent_id.id, 'weight_fix': weight_fix,'count': len(child_task)-ch,}]

        for line in self.tasks:
#            if line.subtask_count>0:
#                line.bobot = 0
                
            if line.distribution=='prorate':
                for p_task in parent_task:
                    if p_task['parent_id']==line.parent_id.id:
                        prorate = (100 - p_task['weight_fix'])/ p_task['count']
                        line.weight =  prorate
#                        break
#                    raise UserError(('child %s \n p_task %s')%(parent_task,p_task))
            if line.weight <0:
                line.weight = 0
                raise UserError(_('Total Weight tidak boleh Minus'))

            # if total_weight>100:
            #     raise UserError(_('Total Weight Melebihi 100%'))
        total_bobot = 0
        for hasil in self.tasks:
            if hasil.subtask_count==0:
                total_bobot += hasil.bobot
                if hasil.progress == 100:
                    total_progres += hasil.bobot
        self.total_progress = total_bobot
        if total_progres > 0:
            persen = total_progres/total_bobot
        else:
            persen = 0
        self.persentase_progress = persen*100

    def _change_auth(self):
        for i in self:
            if i.user_id == i.env.user:
                i.pl_project=True
            else:
                i.pl_project=False

            if i.user_id == i.env.user or i.env.user.id in i.asistant_ids.ids:
                i.is_edit=True
            else:
                i.is_edit=False


    @api.multi
    def action_execute(self):
        for t in self.tasks.filtered(lambda t: not t.parent_id):
            child = self.env['project.task'].search([('id', 'child_of', t.id), ('id', '!=', t.id), ('project_id', '=', self.id)])
            # if len(child) > 10:
            #     raise UserError(("Project Task tidak boleh lebih dari 10 level!"))
        
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to Review.Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', 'review', 'group', 'project.group_project_manager')
        return self.write({"state": "excute"})

    @ api.multi
    def action_pm_review(self):

        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to Approve.Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', 'approve', 'group', 'ab_role.group_direktur')
        self.write({"state": "pm_review"})

    @ api.multi
    def action_close(self):
        # res = super(ProjectProject, self)._message_auto_subscribe_notify(partner_ids, template)
        user_ids = []
        pm_group = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        for i in pm_group:
            user_ids.append(i)
        pa_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_admin").id)
        for j in pa_group:
            user_ids.append(j)
        ceo_group = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        for k in ceo_group:
            user_ids.append(k)
            
        # GET PIC USER
        for project in self:
            for tasks in project.tasks: 
                if tasks.user_id:
                    user_ids.append(tasks.user_id.id)

        # PERSONEL SUPPORT GET DATA 
        # for project in self:
        #     for tasks in project.tasks:    
        #         for ts in tasks.timesheet_ids:
        #             if ts.employee_id.user_id:
        #                 user_ids.append(ts.employee_id.user_id.id)
        
        filteran = []
        for i in user_ids:
            if i not in filteran:
                filteran.append(i)

        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in filteran]
        sendto_list = [self.env["res.users"].browse(uid).id for uid in filteran]
        for p in sendto_list:
            email = self.env['send.email.notification']
            sendto = self.env['res.users'].search([('id', '=', p)])
            email.send_project(self.project_no, self.user_id.name, 'user', sendto)
        self.message_post(
            body="CEO WAS APPROVED! PROJECT IS STARTING!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "close"})

    @ api.multi
    def action_to_planing(self):
        self.write({"state": "ps"})

    @api.multi
    def get_formview_id(self, access_uid=None):
        if self.project_on == 'initiate':
            return self.env.ref('project.edit_project').id
        elif self.project_on == 'plan':
            return self.env.ref('ab_project.solutin_design_form').id
        elif self.project_on == 'ps':
            return self.env.ref('ab_project_task.task_project_form').id
        else:
            return self.env.ref('project.edit_project').id

    def refresh_weight(self):
        for x in self.tasks:
            x.compute_wight_task()

