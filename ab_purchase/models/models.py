# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree
from odoo.addons.ab_role.models.models import _get_uid_from_group
from odoo.exceptions import UserError, RedirectWarning, ValidationError


PURCHASE_REQUISITION_STATES = [
    ('draft', 'Draft'),
    ('ongoing', 'Submitted'),
    ('ongoing_general','Submitted'),
    ('in_progress', 'Reviewed PL'),
    ('approved', 'Reviewed PM'),
    ('fat', 'Reviewed FAT'),
    ('open', 'Open'),
    ('done', 'Closed'),
    ('cancel', 'Cancelled')
]

PURCHASE_TYPE = [
    ('routine', 'Routine'),
    ('non-r', 'Non Routine'),
    ('project', 'Project')
]


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    revision = fields.Text(string='Revision', readonly=True)
    attachment_lines = fields.One2many("ir.attachment", "po_id", string="Attachment List")
    description = fields.Text(string="Description", related="requisition_id.descripton", store="True")
    # attachment_ids = fields.Many2many(comodel_name="ir.attachment", string="Attachment List")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'Submitted'),
        ('fat_approve', 'FAT Approved'),
        ('purchase', 'FAT Approved / Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')

    def action_approve(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_hod_fat").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({'state': 'to approve'})

    @api.multi
    def button_confirm_fat(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({'state': 'fat_approve'})

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent', 'to approve', 'fat_approve']:
                continue
            order._add_supplier_to_product()
            order.button_approve()
            if self.requisition_id:
                self.env.cr.execute("""UPDATE public.purchase_requisition
                            SET state = 'done' where id = %s;""" % (self.requisition_id.id))
                self.env.cr.execute("""UPDATE public.purchase_order
                            SET state = 'cancel' where requisition_id = %s and state != 'purchase';""" % (self.requisition_id.id))
        return True

    @api.multi
    def refused_purchase_order(self):
        view_id = self.env.ref('ab_purchase.refuse_po_wizard_form_view')
        return {
            'name': ('Revision Purchase Order'),
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'res_model': 'refuse.po.wizard',
            'view_id': view_id.id,
            'type': 'ir.actions.act_window',
        }


class PurchaseRequisition(models.Model):
    _inherit = 'purchase.requisition'

    # @api.model
    # def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
    #     result = super(PurchaseRequisition, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     doc = etree.XML(result["arch"])
    #     if self.env.context.get("is_project") and (view_type == "tree" or view_type == "form"):
    #         if not self.user_has_groups("ab_role.group_project_admin"):
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "false")
    #             for form in doc.xpath("//form"):
    #                 form.set("create", "false")
    #     elif self.env.context.get("is_request") and (view_type == "tree" or view_type == "form"):
    #         if not self.user_has_groups("ab_role.group_hod_general_affair"):
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "false")
    #             for form in doc.xpath("//form"):
    #                 form.set("create", "false")

    #     result["arch"] = etree.tostring(doc, encoding="unicode")
    #     return result

    @api.depends('line_ids')
    def _amount_all(self):
        for o in self:
            o.amount = sum([l.subtotal for l in o.line_ids])

    descripton = fields.Text(string="Description")
    amount = fields.Monetary("Amount", store=True, compute="_amount_all", track_visibility="onchange")
    revision = fields.Text(string='Revision', readonly=True)
    project_id = fields.Many2one('project.project', string='Project')
    purchase_type = fields.Selection(PURCHASE_TYPE, string='Purchase Type',  default='routine')
    type_non_project = fields.Selection([
        ('routine', 'Routine'),
        ('non-r', 'Non Routine'),
    ], string='Purchase Type', compute='_get_type_for_non_project', store=True, readonly=False)
    attachment_lines = fields.One2many("ir.attachment", "requisition_id", string="Attachment List")
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')

    purpose = fields.Char(string='Purpose')
    employee_id = fields.Many2one(
        "hr.employee",
        string="Employee",
        default=lambda self: self.env["hr.employee"].search(
            [("user_id", "=", self.env.uid)], limit=1
        ),
        states={"draft": [("readonly", False)]},
    )
    department_id = fields.Many2one('hr.department', string='Division', related="employee_id.department_id")
    job_id = fields.Many2one('hr.job', string='Position', related="employee_id.job_id")
    state = fields.Selection(PURCHASE_REQUISITION_STATES,
                             'Status', track_visibility='onchange', required=True,
                             copy=False, default='draft')
    
    state_a = fields.Selection(related='state', store=False, track_visibility=False)
    state_b = fields.Selection(related='state', store=False, track_visibility=False)

    state_blanket_order = fields.Selection(PURCHASE_REQUISITION_STATES,
                                           'Status', required=True,
                                           copy=False, default='draft')

    skip_state = fields.Boolean(string='Skip state', compute="_skip_state")
    jump_to_fat = fields.Boolean(string='Jump to fat', compute="_jump_to_fat")

    def get_log_payment(self):
        csql = """
select * from (select msg.date + interval '7 hour' as tgl_act,p.name as act_by,coalesce(mm.field_desc,'false') as state,mm.new_value_char,msg.body from mail_message msg
join res_partner p on p.id=msg.author_id left join mail_tracking_value mm on mm.mail_message_id=msg.id and mm.field='state'
where msg.model='purchase.requisition' and msg.res_id=%s and msg.subtype_id=2
order by msg.date desc) as data where state<>'false' or length(body)>0
        """
        self.env.cr.execute(csql,(self.id,))
        res = self.env.cr.fetchall()
        return res

    @api.depends('state')
    def _skip_state(self):
        if (self.state and self.purchase_type != 'project') or (self.state != 'ongoing' and self.purchase_type == 'project'):
            self.skip_state = True
        elif self.state == 'ongoing' and self.purchase_type == 'project':
            self.skip_state = False
        elif self.state == 'done':
            self.skip_state = True

    @api.depends('state')
    def _jump_to_fat(self):
        if self.state == 'ongoing' and self.purchase_type != 'project':
            self.jump_to_fat = True

    @api.multi
    def refused_purchase_request(self):
        view_id = self.env.ref('ab_purchase.refuse_pr_wizard_form_view')
        return {
            'name': ('Revision Purchase Request'),
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'res_model': 'refuse.pr.wizard',
            'view_id': view_id.id,
            'type': 'ir.actions.act_window',
        }

    @api.depends('purchase_type')
    def _get_type_for_non_project(self):
        if self.env.context.get("is_request"):
            self.type_non_project = self.purchase_type

    def action_open(self):
        res = super(PurchaseRequisition, self).action_open()
        user_ids = _get_uid_from_group(
            self, self.env.ref("purchase.group_purchase_manager").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'purchase.requisition', 'approve', 'group', 'purchase.group_purchase_manager')
        return res

    def action_submit(self):
        if len(self.line_ids) == 0:
            raise UserError(("Product line is empty!"))
        if self.project_id:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_lod_lod").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",)
            self.write({"state": "ongoing"})
            self.env['send.email.notification'].send(self.id, 'purchase.requisition', 'review', 'group', 'ab_role.group_lod_lod')
        else:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",)
            self.write({"state": "ongoing_general"})
        # group = []
        # if len(self.line_ids) == 0:
        #     raise UserError(("Product line is empty!"))
        # for o in self:
        #     if o.env.context.get("is_project"):
        #         group.append(o.env.ref("purchase.group_purchase_manager").id)
        #         group.append(o.env.ref("ab_role.group_project_leader").id)
        #         for g in group:
        #             user_ids = _get_uid_from_group(
        #                 o, g
        #             )
        #             partner_ids = [
        #                 o.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        #             ]
        #             o.message_post(
        #                 body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
        #                 subject=("Purchase Request Project - %s") % (o.name),
        #                 partner_ids=partner_ids,
        #                 message_type="notification",
        #                 subtype="mail.mt_comment",
        #             )
        #     if o.env.context.get("is_request"):
        #         user_ids = _get_uid_from_group(
        #             o, o.env.ref("ab_role.group_hod_fat").id
        #         )
        #         partner_ids = [
        #             o.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        #         ]
        #         o.message_post(
        #             body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
        #             subject=("Purchase Request - %s") % (o.name),
        #             partner_ids=partner_ids,
        #             message_type="notification",
        #             subtype="mail.mt_comment",
        #         )
        #     o.write({'state': 'ongoing'})

        

    def action_fat_to_open(self):
        return self.write({'state': 'approved'})

    def action_approve(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",)
        self.write({'state': 'approved'})

    def action_pm_to_fat(self):
        # user_ids = _get_uid_from_group(
        #     self, self.env.ref("ab_role.group_hod_fat").id
        # )
        # partner_ids = [
        #     self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        # ]
        # self.message_post(
        #     body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to approve. \nThanks",
        #     subject=self.name,
        #     partner_ids=partner_ids,
        #     message_type="notification",
        #     subtype="mail.mt_comment",
        # )
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.env['send.email.notification'].send(self.id, 'purchase.requisition', 'approve', 'group', 'ab_role.group_direktur')
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",)
        return self.write({'state': 'fat'})

    def action_in_progress(self):
        res = super(PurchaseRequisition, self).action_in_progress()
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'purchase.requisition', 'review', 'group', 'project.group_project_manager')
        return res

    @api.model
    def create(self, vals):
        res = super(PurchaseRequisition, self).create(vals)
        if res.purchase_type:
            res.name = self.env['ir.sequence'].next_by_code('purchase.request')
        return res

    @api.multi
    def cetak_purchase_request(self):
        return self.env.ref("ab_purchase.cetak_purchase_request").report_action(self)

class PurchaseRequisitionLine(models.Model):
    _inherit = 'purchase.requisition.line'

    subtotal = fields.Float(string="Sub Total",compute="_subtotal",store=True)
    description = fields.Text(string="Description")

    @api.depends('price_unit','product_qty')
    def _subtotal(self):
        for i in self:
            i.subtotal = i.price_unit * i.product_qty
        

class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    requisition_id = fields.Many2one("purchase.requisition")
    po_id = fields.Many2one("purchase.order")
    is_purchase = fields.Boolean(string="purchase")


class ResPartner(models.Model):
    _inherit = 'res.partner'

    vendor_number = fields.Char(string='Vendor No.')

    @api.multi
    def download_xlsx_report(self):
        template_report = 'ab_purchase.vendor_report_xlsx'
        return self.env.ref(template_report).report_action(self)
