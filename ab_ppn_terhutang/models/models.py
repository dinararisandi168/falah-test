# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError


class PPNTerhutang(models.Model):
    _name = "ppn.terhutang"
    _description = "PPN Terhutang"
    _inherit = "mail.thread"

    name = fields.Char(
        string="No PPN Terhutang",
        default="/",
        track_visibility="onchange",
    )
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        track_visibility="onchange",
    )
    prepared_id = fields.Many2one(
        "hr.employee",
        string="Prepared",
        track_visibility="onchange",
    )
    approval_id = fields.Many2one(
        "hr.employee",
        string="Approval",
        track_visibility="onchange",
    )
    start_date = fields.Date(string='Mulai')
    
    masa = fields.Date(
        string="Akhir",
        default=fields.Date.today(),
        track_visibility="onchange",
    )
    currency_id = fields.Many2one(
        "res.currency",
        string="Currency",
        required=True,
        default=lambda self: self.env.user.company_id.currency_id,
    )
    kompensasi = fields.Monetary(
        string="Kompensasi lebih Bayar Dari Masa Sebelumnya",
        track_visibility="onchange",
    )
    ppn_harus_bayar = fields.Monetary(
        string="PPN yang harus dibayar",
        track_visibility="onchange",
    )

    terhutang_line = fields.One2many(
        "ppn.terhutang.line", "ppn_terhutang_id", string="PPN TERHUTANG"
    )

    account_id = fields.Many2one(comodel_name='account.account', string='Akun Utang PPn', default=lambda self: self.env.user.company_id.account_id)
    acc_tax_in_id = fields.Many2one(comodel_name='account.account', string='Akun PPN masukan', default=lambda self: self.env.user.company_id.acc_tax_in_id)
    acc_tax_out_id = fields.Many2one(comodel_name='account.account', string='Akun PPN keluaran', default=lambda self: self.env.user.company_id.acc_tax_out_id)
    

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("done", "Done"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        track_visibility="onchange",
    )

    @api.multi
    def action_confirm(self):
        if len(self.terhutang_line) == 3 and self.acc_tax_in_id and self.acc_tax_out_id:
            account_invoice = self.env['account.invoice']
            account_invoice_line = self.env['account.invoice.line']
        
            vendor_bill = account_invoice.create({
                'partner_id' : self.env['ir.model.data'].xmlid_to_object('ab_ppn_terhutang.vendor_pajak').id,
                'date_invoice' : fields.Date.today(),
                'date_due' :  fields.Date.today(),
                'account_id' : self.account_id.id,
                'reference' : self.name,
                'currency_id' : self.env.user.company_id.currency_id.id,
                'type' : 'in_invoice',
                'journal_type' : 'purchase'
            })

            price_in = 0
            for x in self.terhutang_line:
                if x.account_id == self.acc_tax_in_id:
                    price_in += (x.debit - x.credit) * - 1
                    break

            account_invoice_line.create({
                'invoice_id' : vendor_bill.id,
                'name' : self.acc_tax_in_id.name,
                'account_id' : self.acc_tax_in_id.id,
                'quantity' : 1,
                'price_unit' : price_in,
                'price_subtotal' : price_in
            })

            price_out = 0
            for x in self.terhutang_line:
                if x.account_id == self.acc_tax_out_id:
                    price_out += (x.debit - x.credit) * - 1
                    break
            
            self.write({"state": "done"})
        else:
            raise UserError("Silakan lengkapi formulir terlebih dahulu!")

        account_invoice_line.create({
            'invoice_id' : vendor_bill.id,
            'name' : self.acc_tax_out_id.name,
            'account_id' : self.acc_tax_out_id.id,
            'quantity' : 1,
            'price_unit' : price_out,
            'price_subtotal' : price_out
        })

    @api.multi
    def action_set_to_draft(self):
        return {
            'name' : 'Set to Draft',
            'type': 'ir.actions.act_window', 
            'view_type': 'form', 
            'view_mode': 'form',
            'res_model': 'terhutang.wizard', 
            'context' : {'default_ppn_terhutang_id' : self.id},
            'target': 'new', 
        }

    @api.multi
    def set_to_draft(self):
        self.write({"state": "draft"})

    @api.multi
    def button_done(self):
        self.write({"state": "done"})

    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("ppn.terhutang")
        return super(PPNTerhutang, self).create(vals)
    
    @api.onchange('acc_tax_in_id', 'acc_tax_out_id', 'start_date', 'masa')
    def _onchange_acc_tax_in_id(self):
        if self.start_date and self.masa:
            line = []
            debit_in = 0
            credit_in = 0
            if self.acc_tax_in_id:
                tax_in = self.get_debit_credit_in_out(self.acc_tax_in_id.id, self.start_date, self.masa)

                if tax_in:
                    result = tax_in[0]['debit'] - tax_in[0]['credit']
                    if result < 0:
                        debit_in = abs(result)
                    elif result > 0:
                        credit_in = result 
                line.append((0, 0, {'account_id' : self.acc_tax_in_id.id, 'debit' : debit_in, 'credit' : credit_in}))
        
            debit_out = 0
            credit_out = 0
            if self.acc_tax_out_id:
                tax_out = self.get_debit_credit_in_out(self.acc_tax_out_id.id, self.start_date, self.masa)

                if tax_out:
                    result = tax_out[0]['debit'] - tax_out[0]['credit']
                    if result > 0:
                        debit_out = result
                    elif result < 0:
                        credit_out = abs(result) 
                line.append((0, 0, {'account_id' : self.acc_tax_out_id.id, 'debit' : debit_out, 'credit' : credit_out}))

            if self.account_id:
                debit = debit_in + debit_out
                credit = credit_in + credit_out

                if debit > credit:
                    debit = 0
                    credit = abs(debit - credit)
                elif debit < credit:
                    self.ppn_harus_bayar = abs(debit - credit)
                    debit = abs(debit - credit)
                    credit = 0
                else:
                    debit = 0
                    credit = 0
                line.append((0, 0, {'account_id' : self.account_id.id, 'debit' : debit, 'credit' : credit}))

            if self.terhutang_line:
                self.terhutang_line = False
            
            self.terhutang_line = line

    def get_debit_credit_in_out(self, account_id, start_date, end_date):
        sql = """
            SELECT SUM(COALESCE(aml.debit, 0)) AS debit, SUM(COALESCE(aml.credit, 0)) AS credit FROM account_move_line aml
            LEFT JOIN account_move am ON am.id = aml.move_id
            WHERE aml.account_id = %s AND am.date BETWEEN '%s' AND '%s'
            GROUP BY aml.account_id
        """ % (account_id, start_date, end_date)

        self._cr.execute(sql)
        return self._cr.dictfetchall()
class PPNTerhutangLine(models.Model):
    _name = "ppn.terhutang.line"
    _description = "PPN Terhutang Line"

    ppn_terhutang_id = fields.Many2one(comodel_name='ppn.terhutang', string='parent')
    account_id = fields.Many2one(comodel_name='account.account', string='Account')
    debit = fields.Float()
    credit = fields.Float()
