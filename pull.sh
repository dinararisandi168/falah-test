#!/bin/bash
cd /opt/odoo/FALAH
sudo git pull
while true; do
        read -p "Do you want to restart odoo server ? (Y/N)" yn
        case $yn in
                [Yy]* ) sudo systemctl restart odoo-server; break;;
                [Nn]* ) echo "Bye"; exit;;
                * ) echo "Please answer yes or no.";;
        esac
done
