# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime, timedelta, date
from datetime import datetime as dt
from odoo.addons.ab_role.models.models import _get_uid_from_group



ASPEK_TP = [
    # "ASPEK TEKNIS PEKERJAAN",
    "Efektifitas & Efisiensi Kerja",
    "Ketepatan dalam menyelesaikan tugas ",
    "Kemampuan mencapai target perusahaan ",
    "Kemampuan menerima tugas tambahan ",
    "Peningkatan Kemampuan dalam Bekerja ",
]
ASPEK_NT = [
    # "ASPEK NON TEKNIS ",
    "Tertib Administrasi ",
    "Inisiatif ",
    "kerjasama Tim",
    "Ketaatan terhadap instruksi atasan ",
    "Komunikasi",
]
ASPEK_PRIBADI = [
    # "ASPEK KEPRIBADIAN ",
    "Prilaku",
    "Kedisiplinan",
    "Tanggung jawab &amp; Loyalitas",
    "Ketelitian",
    "Kehadiran & tepat waktu ",
]
ASPEK_KPM = [
    # "ASPEK KEPEMIMPINAN ",
    "Koordinasi anggota ",
    "Kontrol anggota ",
    "Evaluasi & pembinaan anggota ",
    "Delegasi tanggung jawab dan wewenang ",
    "Kecepatan & ketepatan pengambilan keputusan ",
]


class ResCompany(models.Model):
    _inherit = "res.company"

    direksi_id = fields.Many2one("hr.employee", string="Direksi")


class NilaiPrestasiKerja(models.Model):
    _name = "prestasi.kerja"
    _inherit = "mail.thread"
    _description = "penilaiaan prestasi kerja"

    name = fields.Char(string="Sequence", default="/",track_visibility="onchange")
    company_id = fields.Many2one("res.company", string="Company")
    employee_id = fields.Many2one("hr.employee", string="Nama", track_visibility="onchange")
    nik = fields.Char(string="NIK", related="employee_id.nik", track_visibility="onchange")
    department_id = fields.Many2one("hr.department",string="Department",related="employee_id.department_id",track_visibility="onchange")
    job_id = fields.Many2one("hr.job",string="Jabatan",related="employee_id.job_id",track_visibility="onchange")
    masa_kerja = fields.Integer(string="Masa Kerja(days)",track_visibility="onchange",compute="_duration",store=True,)
    periode = fields.Char(string="Periode", track_visibility="onchange")
    triwulan = fields.Char(string="Triwulan", track_visibility="onchange")
    tahun = fields.Char(string="Tahun", default=datetime.today().year, track_visibility="onchange")
    prestasi = fields.Text(string="PRESTASI LAIN YANG PERLU DICATAT", track_visibility="onchange")
    indisipliner = fields.Text(string="INDISIPLINER YANG PERLU DICATAT", track_visibility="onchange")
    saran_perbaikan = fields.Text(string='SARAN DAN PERBAIKAN', track_visibility="onchange")
    saran = fields.Text(string="SARAN DAN PERBAIKAN",track_visibility="onchange")
    direksi_id = fields.Many2one("hr.employee",string="Direksi",track_visibility="onchange",related="company_id.direksi_id")
    atasan_id = fields.Many2one("hr.employee", string="Atasan Langsung",track_visibility="onchange",related="employee_id.parent_id")
    atasan1_id = fields.Many2one("hr.employee", string="Atasan Tidak langsung", track_visibility="onchange",related="employee_id.parent_id.parent_id")
    tanggal_direksi = fields.Date(string="Tanggal", track_visibility="onchange")
    tanggal_atasan = fields.Date(string="Tanggal", track_visibility="onchange")
    tanggal_atasan1 = fields.Date(string="Tanggal", track_visibility="onchange")
    direksi_sign = fields.Binary(string="Direksi", related="direksi_id.user_id.digital_signature")
    atasan_sign = fields.Binary(string="Atasan Langsung", related="atasan_id.user_id.digital_signature")
    atasan1_sign = fields.Binary(string="Atasan Tidak Langsung", related="atasan1_id.user_id.digital_signature")
    nilai_atasan = fields.Integer(string="Nilai Atasan Langsung", track_visibility="onchange")
    apek_tp_line = fields.One2many("aspek.tp.line", "prestasi_id", string="ASPEK TEKNIS PEKERJAAN")
    apek_nt_line = fields.One2many("aspek.nt.line", "prestasi_id", string="ASPEK NON TEKNIS")
    apek_pribadi_line = fields.One2many("aspek.pribadi.line", "prestasi_id", string="ASPEK KEPRIBADIAN")
    apek_kpm_line = fields.One2many("aspek.kpm.line", "prestasi_id", string="ASPEK KEPEMIMPINAN")
    total_atasan = fields.Integer(string="Total Nilai Atasan langsung",compute="_amount_all",track_visibility="onchange",)
    total_atasan1 = fields.Integer(string="Total Nilai Atasan Tidak langsung", compute="_amount_all",track_visibility="onchange",)
    keterangan_nilai = fields.Char(string="Keterangan Nilai", compute="_amount_all", track_visibility="onchange")
    nilai_akhir = fields.Integer(string="Total Nilai", compute="_amount_all", track_visibility="onchange")
    state = fields.Selection(
        [("draft", "Draft"), 
        ("submit", "LOD Set Score"),
        ("submit1", "HOD Set Score"),
        ("review", "HR Review"),
        ("approve", "CEO Approve"),
        ("done", "Done")],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        track_visibility="onchange",
    )
    catatan_revisi = fields.Text(string="Catatan Revisi", track_visibility="onchange")
    is_lod = fields.Boolean(string='Saya LOD',compute="_cek_lead")
    is_hod = fields.Boolean(string='Saya HOD',compute="_cek_lead")

    def _cek_lead(self):
        for i in self:
            if i.atasan_id:
                if i.atasan_id.user_id == i.env.user:
                    i.is_lod = True
                else:
                    i.is_lod = False
            if i.atasan1_id:
                if i.atasan1_id.user_id == i.env.user:
                    i.is_hod = True
                else:
                    i.is_hod = False

    @api.multi
    def cetak_ppk(self):
        return self.env.ref("ab_prestasi_kerja.cetak_ppk").report_action(self)

    
    @api.multi
    def unlink(self):
        for i in self:
            if i.state != "draft":
                raise UserError(
                    ("PPK tidak bisa dihapus pada state %s !")
                    % (i.state)
                )
        return super(NilaiPrestasiKerja, self).unlink()

    @api.multi
    def action_submit(self):

        # user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        # partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        partner = self.atasan_id.user_id.partner_id.id
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=[(6, 0, [partner])],
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'prestasi.kerja', 'submit', 'user', self.atasan_id.user_id)
        self.write({"state": "submit"})

    @api.multi
    def action_input(self):
        partner = self.atasan1_id.user_id.partner_id.id
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=[(6, 0, [partner])],
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'prestasi.kerja', 'submit', 'user', self.atasan1_id.user_id)
        self.write({"state": "submit1"})

    @api.multi
    def action_input1(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        # partner = self.atasan_id.user_id.partner_id.id
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "review"})

    # @api.multi
    # def action_review(self):
    #     self.write({"state": "approve"})


    def action_review(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'prestasi.kerja', 'submit', 'group', 'ab_role.group_direktur')
        self.write({'state': 'approve'})    

    @api.multi
    def action_done(self):
        self.write({"state": "done"})

    @api.multi
    def action_cancel(self):
        self.write({"state": "draft"})

   
    @api.depends(
        "apek_tp_line.nilai_atasan",
        "apek_tp_line.nilai_atasan1",
        "apek_nt_line.nilai_atasan",
        "apek_nt_line.nilai_atasan1",
        "apek_pribadi_line.nilai_atasan",
        "apek_pribadi_line.nilai_atasan1",
        "apek_kpm_line.nilai_atasan",
        "apek_kpm_line.nilai_atasan1",
    )
    def _amount_all(self):
        for total in self:
            atasan = atasan1 = 0
            keterangan = ""
            for line in total.apek_tp_line:
                atasan += line.nilai_atasan
                atasan1 += line.nilai_atasan1
            for line in total.apek_nt_line:
                atasan += line.nilai_atasan
                atasan1 += line.nilai_atasan1
            for line in total.apek_pribadi_line:
                atasan += line.nilai_atasan
                atasan1 += line.nilai_atasan1
            for line in total.apek_kpm_line:
                atasan += line.nilai_atasan
                atasan1 += line.nilai_atasan1
            total_atasan = atasan + atasan1
            if total_atasan > 800:
                keterangan = "Nilai melebihi klasifikasi nilai"
            elif total_atasan > 600:
                keterangan = "Nilai A dengan kualias Sangat Baik"
            elif total_atasan > 450:
                keterangan = "Nilai B dengan kualitas Baik"
            elif total_atasan > 250:
                keterangan = "Nilai C dengan kualitas Cukup"
            elif total_atasan > 100:
                keterangan = "Nilai D dengan kualitas Buruk"
            elif total_atasan == 0:
                keterangan = "Belum ada nilai"
            else:
                keterangan = "Nilai tidak masuk klasifikasi"

            total.update(
                {
                    "total_atasan": atasan,
                    "total_atasan1": atasan1,
                    "nilai_akhir": total_atasan,
                    "keterangan_nilai": keterangan,
                }
            )

    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("no.ppk")
        return super(NilaiPrestasiKerja, self).create(vals)

    @api.onchange("employee_id")
    def _duration(self):
        if self.employee_id.tanggal_join:
            for rec in self:
                rec.masa_kerja = (
                    date.today() - rec.employee_id.tanggal_join).days

    @api.model
    def default_get(self, fields):
        get_month = (date.today()).month
        number = "-"
        rec = super(NilaiPrestasiKerja, self).default_get(fields)
        # for s in self:
        if get_month <= 3:
            number = "I"
        elif get_month <= 6:
            number = "II"
        elif get_month <= 9:
            number = "III"
        elif get_month <= 12:
            number = "IV"
        aspek = []
        nilai_nt = []
        nilai_pribadi = []
        nilai_kpm = []
        for i in ASPEK_TP:
            aspek.append((0, 0, {"aspek_penilaian": i}))
        for j in ASPEK_NT:
            nilai_nt.append((0, 0, {"aspek_penilaian": j}))
        for k in ASPEK_PRIBADI:
            nilai_pribadi.append((0, 0, {"aspek_penilaian": k}))
        for l in ASPEK_KPM:
            nilai_kpm.append((0, 0, {"aspek_penilaian": l}))

        rec.update(
            {
                "apek_tp_line": aspek,
                "apek_nt_line": nilai_nt,
                "apek_pribadi_line": nilai_pribadi,
                "apek_kpm_line": nilai_kpm,
                "triwulan": number,
                "company_id": self.env.user.company_id.id,
            }
        )
        return rec


class AspekTP(models.Model):
    _name = "aspek.tp.line"
    _description = "penilaiaan Aspek Teknis pekerjaan"

    sequence = fields.Integer(string=" ")
    aspek_penilaian = fields.Char(string="Aspek Penilaian")
    nilai_atasan = fields.Integer(string="Atasan Langsung", store=True)
    nilai_atasan1 = fields.Integer(string="Atasan Tidak Langsung", compute="_compute_nilai", store=True)
    prestasi_id = fields.Many2one("prestasi.kerja", string="PK")

    @api.onchange('nilai_atasan','nilai_atasan1')
    def onchange_nilai(self):
        for o in self:
            if o.nilai_atasan > 20 or o.nilai_atasan1 >20:
                raise UserError(("Nilai Tidak Boleh Lebih Dari 20!"))
            elif o.nilai_atasan < 0 or o.nilai_atasan1 < 0:
                raise UserError(("Nilai Tidak Boleh Kurang Dari 0!"))
          
class AspekNT(models.Model):
    _name = "aspek.nt.line"
    _description = "Penilaiaan Aspek Non Teknis"

    sequence = fields.Integer(string=" ")
    aspek_penilaian = fields.Char(string="Aspek Penilaian")
    nilai_atasan = fields.Integer(string="Atasan Langsung", store=True)
    nilai_atasan1 = fields.Integer(
        string="Atasan Tidak Langsung", compute="_compute_nilai", store=True
    )
    prestasi_id = fields.Many2one("prestasi.kerja", string="PK")

    @api.onchange('nilai_atasan','nilai_atasan1')
    def onchange_nilai(self):
        for o in self:
            if o.nilai_atasan > 20 or o.nilai_atasan1 >20:
                raise UserError(("Nilai Tidak Boleh Lebih Dari 20!"))
            elif o.nilai_atasan < 0 or o.nilai_atasan1 < 0:
                raise UserError(("Nilai Tidak Boleh Kurang Dari 0!"))

class AspekPribadi(models.Model):
    _name = "aspek.pribadi.line"
    _description = "Penilaiaan Aspek Kepribadian"

    sequence = fields.Integer(string=" ")
    aspek_penilaian = fields.Char(string="Aspek Penilaian")
    nilai_atasan = fields.Integer(string="Atasan Langsung", store=True)
    nilai_atasan1 = fields.Integer(string="Atasan Tidak Langsung", compute="_compute_nilai", store=True)
    prestasi_id = fields.Many2one("prestasi.kerja", string="PK")

    @api.onchange('nilai_atasan','nilai_atasan1')
    def onchange_nilai(self):
        for o in self:
            if o.nilai_atasan > 20 or o.nilai_atasan1 >20:
                raise UserError(("Nilai Tidak Boleh Lebih Dari 20!"))
            elif o.nilai_atasan < 0 or o.nilai_atasan1 < 0:
                raise UserError(("Nilai Tidak Boleh Kurang Dari 0!"))

class AspekKPM(models.Model):
    _name = "aspek.kpm.line"
    _description = "Penilaiaan Aspek Kepemimpinan"

    sequence = fields.Integer(string=" ")
    aspek_penilaian = fields.Char(string="Aspek Penilaian")
    nilai_atasan = fields.Integer(string="Atasan Langsung", store=True)
    nilai_atasan1 = fields.Integer(string="Atasan Tidak Langsung", compute="_compute_nilai", store=True)
    prestasi_id = fields.Many2one("prestasi.kerja", string="PK")

    @api.onchange('nilai_atasan','nilai_atasan1')
    def onchange_nilai(self):
        for o in self:
            if o.nilai_atasan > 20 or o.nilai_atasan1 >20:
                raise UserError(("Nilai Tidak Boleh Lebih Dari 20!"))
            elif o.nilai_atasan < 0 or o.nilai_atasan1 < 0:
                raise UserError(("Nilai Tidak Boleh Kurang Dari 0!"))
                
class Wizard(models.TransientModel):
    _name = 'catatan.wizard.ppk'
    _description = 'Wizard Revisi PPK'

    text_revisi = fields.Text(string='Catatan Revisi')

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            ppk = self.env['prestasi.kerja'].browse(active_id)
            ppk.catatan_revisi = self.text_revisi
            ppk.action_cancel()
