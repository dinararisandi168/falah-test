
from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo.http import content_disposition, request
from odoo.modules.module import get_resource_path


class SummaryLeaveReportXlsx(models.AbstractModel):
    _name = "report.ab_hr_employees.report_summary_leave_xlsx"
    _inherit = "report.report_xlsx.abstract"

    def generate_xlsx_report(self, workbook, data, obj):
        heading_format = workbook.add_format(
            {'align': 'left', 'valign': 'vleft', 'bold': True, 'size': 10})
        sub_heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True,
                                                  'size': 9, 'color': 'white', 'bg_color': '#4472c4', 'border_color': 'white', 'border': 1})

        normal_num_bold_no_currency = workbook.add_format(
            {'bold': True, 'num_format': '#,###0.00'})

        # add sheet
        worksheet = workbook.add_worksheet('Cuti Tahunan')
        # setting width of the column

        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 40)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 25)
        worksheet.set_column('G:G', 40)
        worksheet.set_column('H:H', 10)

        worksheet.freeze_panes(9, 1)
        worksheet.freeze_panes(9, 2)
        worksheet.freeze_panes(9, 3)
        worksheet.freeze_panes(9, 4)
        worksheet.freeze_panes(9, 5)
        worksheet.freeze_panes(9, 6)
        worksheet.freeze_panes(9, 7)
        worksheet.freeze_panes(9, 8)

        row = 1
        image = get_resource_path(
            'ab_hr_employees', 'static/description', 'logo.png')
        worksheet.insert_image(
            row, 0, image,  {'x_scale': 0.2, 'y_scale': 0.2})

        row += 4
        worksheet.merge_range(
            'A6:C6', 'HAK CUTI TAHUNAN KARYAWAN', heading_format)
        worksheet.merge_range(
            'A7:C7', 'KARYAWAN PERMANENT DAN KONTRAK', heading_format)

        worksheet.write('A9', 'No', sub_heading_format)
        worksheet.write('B9', 'NIK', sub_heading_format)
        worksheet.write('C9', 'NAMA', sub_heading_format)
        worksheet.write('D9', 'POSISI', sub_heading_format)
        worksheet.write('E9', 'JATAH CUTI 2020', sub_heading_format)
        worksheet.write('F9', 'CUTI YANG SUDAH DIAMBIL', sub_heading_format)
        worksheet.write(
            'G9', 'POTONG DARI CUTI BERSAMA 24 & 31 DES 20', sub_heading_format)
        worksheet.write('H9', 'SISA', sub_heading_format)

        # spb_obj = self.env['hr.employee'].search([])
        spb_obj = self.env['hr.employee'].search([])
        sum_leave = self.env['hr.leave.report'].search(
            [('type', '=', 'request')])
        sum_leave2 = self.env['hr.leave.report'].search(
            [('type', '=', 'allocation')])

        no = 0

        str_no = []
        NIK = []
        NAMA = []
        POSISI = []
        JATAHCUTI = []
        AMBILCUTI = []
        SISA = []

        for x in spb_obj:

            no += 1
            str_no.append(str(no))
            NIK.append(x.nik or '')
            NAMA.append(x.name or '')
            POSISI.append(x.job_id.name or '')
            SISA.append(x.remaining_leaves or '')

        for y in sum_leave:

            AMBILCUTI.append(str(y.number_of_days) or '0')

        for z in sum_leave2:

            JATAHCUTI.append(z.number_of_days or '')

        # AMBILCUTI = [y.number_of_days if y.number_of_days else 0 for y in spbu_arb]

        worksheet.write_column('A10', str_no)
        worksheet.write_column('B10', NIK)
        worksheet.write_column('C10', NAMA)
        worksheet.write_column('D10', POSISI)
        worksheet.write_column('E10', JATAHCUTI)
        worksheet.write_column('F10', AMBILCUTI)
        worksheet.write_column('H10', SISA)
