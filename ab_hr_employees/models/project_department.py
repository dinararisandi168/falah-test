# -*- coding: utf-8 -*-

from odoo import _, api, fields, models


class HrProjectDepartment(models.Model):
    _name = "hr.project.department"

    name = fields.Char(string="Name")
