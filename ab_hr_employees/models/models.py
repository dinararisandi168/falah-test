# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date



class HrEmployee(models.Model):
    _inherit = "hr.employee"

    nik = fields.Char(string="NIK")
    npwp = fields.Char(string="NPWP")
    alamat_ktp = fields.Char(string="Alamat KTP")
    alamat_tinggal = fields.Char(string="Alamat Tinggal")
    tanggal_join = fields.Date(string="Tanggal Join")
    tanggal_probation = fields.Date(string="Tanggal Probation")
    permanent = fields.Boolean(string="PERMANENT")
    alamat_email = fields.Char(string="Alamat Email")
    alamat_email_pribadi = fields.Char(string="Alamat Email Pribadi")
    no_rek_mandiri = fields.Char(string="Bank Name")
    no_tlp = fields.Char(string="No Telp")
    agama = fields.Selection(
        [
            ("islam", "Islam"),
            ("kristen", "Kristen"),
            ("khatolik", "Khatolik"),
            ("hindu", "Hindu"),
            ("budha", "Budha"),
            ("konghucu", "Konghucu"),
        ],
        string="AGAMA",
    )
    is_project = fields.Boolean(compute='_compute_is_project', string='Employee Project')
    
    def _compute_is_project(self):
        for i in self:
            i.is_project = 'Project' in [i.department_id.name, i.department_id.parent_id.name]

    employeetags_id = fields.Many2one("hr.project.department", string="Department")

    tahun_lulus = fields.Date(string="Tahun Lulus")

    year = fields.Selection(sorted([(x, x) for x in range(1930, date.today().year + 1)], reverse=True), string='Tahun Lulus', default=date.today().year)


    bpjs_tk = fields.Char(string="No BPJS TK")
