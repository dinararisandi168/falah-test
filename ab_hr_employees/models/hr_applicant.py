# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from lxml import etree
from odoo.addons.ab_role.models.models import _get_uid_from_group


class HrApplicant(models.Model):
    _inherit = "hr.applicant"

    @api.model
    def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
        result = super(HrApplicant, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(result["arch"])
        if self.env.context.get("open_applicant", []) and (view_type == "tree" or view_type == "form" or view_type == "kanban"):
            if self.user_has_groups("project.group_project_manager"):
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "False")
                    node_form.set("delete", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("create", "False")
                    tree.set("delete", "false")
                for form in doc.xpath("//kanban"):
                    form.set("create", "false")
                    form.set("delete", "false")
            if self.user_has_groups("ab_role.group_bussines_demand"):
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "false")
                    node_form.set("delete", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("delete", "false")
                    tree.set("create", "false")
                for form in doc.xpath("//kanban"):
                    form.set("create", "false")
                    form.set("delete", "false")
            if self.user_has_groups("ab_role.group_hod_general_affair"):
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "false")
                    node_form.set("delete", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("delete", "false")
                    tree.set("create", "false")
                for form in doc.xpath("//kanban"):
                    form.set("create", "false")
                    form.set("delete", "false")
            if self.user_has_groups("ab_role.group_hod_fat"):
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "false")
                    node_form.set("delete", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("delete", "false")
                    tree.set("create", "false")
                for form in doc.xpath("//kanban"):
                    form.set("create", "false")
                    form.set("delete", "false")

        result["arch"] = etree.tostring(doc, encoding="unicode")
        return result
