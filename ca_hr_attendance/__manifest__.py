# -*- coding: utf-8 -*-
{
    'name': "Attendances by coordinate and picture",

    'summary': """
       Track employee attendance by coordinate and image, connect with mobile apps""",

    'description': """
        Track employee attendance by coordinate and image,  connect with mobile apps
    """,

    'author': "choirularifin1010@gmail.com",
    'website': "https://choirular.wordpress.com/",
    'category': 'Human Resources',
    'version': '0.1',
    'depends': ['base','hr_attendance'],

    'data': [
        'views/ca_hr_attendance_view.xml'
    ],
    'demo': [
        #'demo/demo.xml',
    ],
}