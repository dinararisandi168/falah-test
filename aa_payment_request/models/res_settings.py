from odoo import api, fields, models


class ResConfig(models.TransientModel):
    _inherit = "res.config.settings"
    _description = "Custom Config Attendance"

    account_cash_advance = fields.Many2one(comodel_name='account.account', string='Account Cash Advance',store=True, readonly=False, related="company_id.account_cash_advance")
    
class Company(models.Model):
    _inherit = "res.company"

    account_cash_advance = fields.Many2one(comodel_name='account.account', string='Account Cash Advance')
    
