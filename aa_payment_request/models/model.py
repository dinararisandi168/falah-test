from datetime import datetime
from odoo.tools import float_round
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from odoo.addons.ab_role.models.models import _get_uid_from_group
import time


class PaymentRequest(models.Model):
    _name = "payment.request"
    _inherit = "mail.thread"

    # @api.model
    # def fields_view_get(self, view_id="approval_payment_request_form_view", view_type="form", toolbar=False, submenu=False):
    #     result = super(PaymentRequest, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     if self.env.project_id("True") and (view_type == "form"):
    #         for form in doc.xpath("//tree") 
    #             tree.set("create", "false")
    #         for form in doc.xpath("//form"):
    #             form.set("create", "false")
    #     elif self.env.project_id("False"):
    #         if not self.user_has_groups("ab_role.group_hod_general_affair"):
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "false")
    #             for form in doc.xpath("//form"):
    #                 form.set("create", "false")

        # result["arch"] = etree.tostring(doc, encoding="unicode")
        # return result

    @api.depends('payment_line')
    def _amount_all(self):
        for o in self:
            if o.advance_amount:
                o.amount = o.advance_amount - sum([l.amount for l in o.payment_line])
            else:
                o.amount = sum([l.amount for l in o.payment_line])

            o.balance = o.advance_amount - sum([l.bill for l in o.payment_line])

            if o.amount and o.type!='settle':
                o.payment = o.amount
                
            if len(o.payment_line)==1 and o.payment_line[0].invoice_id:
                o.project_id = o.payment_line[0].invoice_id.project_id.id

    @api.one
    @api.depends('payment_line.amount', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
                 'currency_id','amount_total')
    def _compute_amount_tax(self):
        self.amount_untaxed = 0
        self.amount_tax = 0
        self.amount_total = 0
        round_curr = self.currency_id.round
        if self.payment_type=='bill':
            self.amount_untaxed = sum(line.amount for line in self.payment_line)
        else:
            self.amount_untaxed = sum(line.price_subtotal for line in self.payment_line)
            tax_group = self.get_taxes_values()
            amount_tax = 0
            for line_tax in tax_group:
                amount_tax += tax_group.get(line_tax,{}).get('amount',False)
                
            self.amount_tax = amount_tax
        self.amount_total = self.amount_untaxed + self.amount_tax
        self.balance = self.advance_amount - self.amount_total         

    name = fields.Char("Reference", default="/", readonly=True)
    date = fields.Date("Date", required=True, default=fields.Date.context_today, readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange")
    due_date = fields.Date(string="Due Date", default=fields.Date.context_today)
    user_id = fields.Many2one("res.users", string="User", readonly=True, required=True, default=lambda self: self.env.user, copy=False)
    amount = fields.Monetary("Amount", store=True, compute="_amount_all", track_visibility="onchange")
    balance = fields.Monetary("Balance", store=True )
    currency_id = fields.Many2one("res.currency", string="Currency", required=True, default=lambda self: self.env.user.company_id.currency_id)
    employee_id = fields.Many2one("hr.employee", string="Drafter", readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange", required=True,)
    partner_id = fields.Many2one("res.partner", string="Partner", compute='compute_payment_type', track_visibility="onchange", store=True)
    department_id = fields.Many2one("hr.department", string="Department", related="employee_id.department_id", track_visibility="onchange")
    pembayaran_id = fields.Many2one("payment.request", "Advance", domain=[("type", "=", "aap"), ("state", "in", ["done",'paid'])],
                                    readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange")
    payment_line = fields.One2many("payment.request.line", "payment_id", "Payment Lines", readonly=True,
                                   states={"draft": [("readonly", False)],"submit": [("readonly", False)],"reviewed_ga": [("readonly", False)],"reviewed_hr": [("readonly", False)],"reviewed_pm": [("readonly", False)]})
    description = fields.Text("Description", required=True, readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange")
    purpose = fields.Char(string="Purpose", readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange",)
    project_id = fields.Many2one("project.project", string="Project", readonly=True, states={"draft": [("readonly", False)]})

    reason = fields.Text("Reason", readonly=True, states={"draft": [("readonly", False)]}, track_visibility="onchange", )
    type = fields.Selection(
        [
            ("payment", "Payment Request"),
            ("aap", "Approval Advance Payment"),
            ("settle", "Approval Settlements"),
        ],
        string="Type",
        required=True,
    )
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("reviewed_hr", "Reviewed HR"),
            ("reviewed_ga", "Reviewed GA"),
            ("reviewed_pm", "Reviewed PM"),
            ("reviewed", "Reviewed FAT"),
            ("verified", "Verified"),
            ("confirm", "Confirmed"),
            ("done2", "Approval Direction"),
            ("done", "Approved"),
            ('paid', 'Paid'),
            ('settle', 'Settlement'),
            ('close', 'Closed'),
            ("cancel", "Cancel"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        track_visibility="onchange",
    )
    state_a = fields.Selection(related='state', store=False, track_visibility=False)
    state_b = fields.Selection(related='state', store=False, track_visibility=False)
    state_c = fields.Selection(related='state', store=False, track_visibility=False)
    state_d = fields.Selection(related='state', store=False, track_visibility=False)
    state_e = fields.Selection(related='state', store=False, track_visibility=False)
    state_f = fields.Selection(related='state', store=False, track_visibility=False)
    
    journal_id = fields.Many2one('account.journal',string='Journal', 
                                 track_visibility="onchange", domain=[("type", "in", ['cash','bank'])])
    cara = fields.Selection([("cash", "Cash"), ("transfer", "Transfer")], string="Method", default="cash", required=True, )
    nama_rekening = fields.Char("Rekening Name", readonly=True, states={"draft": [("readonly", False)],"submit": [("readonly", False)]})
    nomor_rekening = fields.Char("Rekening Number", readonly=True, states={"draft": [("readonly", False)],"submit": [("readonly", False)]})
    nama_bank = fields.Many2one('res.bank',string="Bank Name", readonly=True, states={"draft": [("readonly", False)],"submit": [("readonly", False)]})
    medical = fields.Boolean(string="Is Medical")
    advance_type = fields.Selection(
        [
            ("project_based", "Project based"),
            ("routine", "Routine"),
            ("non_routine", "Non Routine"),
            # ("kurang_bayar", "Kurang Bayar"),
        ],
        string="Advance Type",
    )
    payment_type = fields.Selection(
        [
            ("project", "Project based"),
            ("general", "General"),
            ("hrd", "Human Resources"),
            ("medical", "Health Claim"),
            ("overbook", "Over Booking"),
            ("bill","Vendor Bill"),
        ],
        string="Payment Type",
    )
    analytic_account_id = fields.Many2one("account.analytic.account", string="Analytic Account", compute='compute_analytic', readonly=False, store=True)
    payment_plan = fields.Date(string="Payment Plan", default=fields.Date.context_today, track_visibility="onchange", readonly=False)
    attachment_ids = fields.Many2many(comodel_name="ir.attachment", string="Attachment List")
    attachment_lines = fields.One2many("ir.attachment", "payment_id", string="Attachment List")
    payment = fields.Monetary("Payment", store=True, compute="_amount_payment", track_visibility="onchange")
    pinjaman_id = fields.Many2one("hr.cicilan", "Pinjaman", domain="[('state', '=', 'done')]", readonly=True, states={"draft": [("readonly", False)]})
    is_gtlimajt = fields.Boolean(string="Di Atas 5 Juta")
    is_hr = fields.Boolean(string="Request HR",default=False)
    is_bill = fields.Boolean(string="Vendor Bill",default=False)
    date_approved = fields.Datetime(string="Date Approved", track_visibility="onchange", readonly=True)
    statement_id = fields.Many2one('account.bank.statement',string='Bank Statement', readonly=True, store=True, compute='compute_statement_id')
    settlement_id = fields.Many2one('payment.request',string='Settlement', readonly=True, compute='compute_settlement_id')
    advance_amount = fields.Monetary("Amount", digits=dp.get_precision("Product Price"), related='pembayaran_id.amount_total')

    tax_line_ids = fields.One2many('payment.request.tax', 'payment_id', string='Tax Lines', 
        readonly=True, states={'draft': [('readonly', False)]}, copy=True)

    amount_untaxed = fields.Monetary("Amount Untaxed", store=True, compute="_compute_amount_tax", track_visibility="onchange")
    amount_tax = fields.Monetary("Tax Amount", store=True, compute="_compute_amount_tax", track_visibility="onchange")
    amount_total = fields.Monetary("Amount Total", store=True, compute="_compute_amount_tax", track_visibility="onchange")
    total_actual_amount = fields.Monetary('Total Actual Amount')
    can_create_statement = fields.Boolean(compute='_compute_can_create_statement', string='Can CReate Statement')
    can_create_settlement = fields.Boolean(compute='_compute_can_create_settlement', string='can create settlement')
    is_paid = fields.Boolean(string="is_paid")
    
    @api.depends('total_actual_amount','amount_total')
    def _compute_can_create_statement(self):
        for this in self:
            this.can_create_statement = this.total_actual_amount == this.amount_total

    @api.depends('settlement_id')
    def _compute_can_create_settlement(self):
        for i in self:
            i.can_create_settlement = i.settlement_id == i.id

    # payment_plan = fields.Float(string='Payment Plan',readonly=True, states={"draft": [("readonly", False)]})
    # attacment_line = fields.One2many("ir.attachment", "payment_id", string="Attachment List")

    @api.multi
    def write(self, vals):
        for i in self:
            if i.attachment_ids:
                for ai in i.attachment_ids:
                    if ai.res_id == 0 or False:
                        ai.update({'res_id': i.id})
        res = super(PaymentRequest, self).write(vals)
        return res

    def compute_settlement_id(self):
        for i in self:
            i.settlement_id = i.search([('pembayaran_id','=',i.id)])

    @api.onchange('payment_line')
    def _onchange_payment_line(self):
        overtime = []
        for i in self.payment_line:
            if i.overtime_id.id in overtime:
                raise UserError(('Tidak bisa menambahkan line dengan overtime yang sama.'))
            if i.overtime_id.id:
                overtime.append(i.overtime_id.id)

    @api.model
    def create(self, vals):
        res = super(PaymentRequest, self).create(vals)
#        if res.pinjaman_id:
#            res.pinjaman_id.update({'payment_id': self.id,})
        if res.type == "payment":
            res.name = self.env["ir.sequence"].next_by_code("payment.request")
        elif res.type == "aap":
            res.name = self.env["ir.sequence"].next_by_code("approval.advance.payment")
        elif res.type == "settle":
            res.name = self.env["ir.sequence"].next_by_code("approval.settlements")
        
        if res.attachment_ids:
            for ai in res.attachment_ids:
                if ai.res_id == 0 or False:
                    ai.update({'res_id': res.id})
        return res

    # @api.model
    # def create(self, vals):
    #     name = "/"
    #     if vals.get("type") == "payment":
    #         name = self.env["ir.sequence"].next_by_code("payment.request")
    #     elif vals.get("type") == "aap":
    #         name = self.env["ir.sequence"].next_by_code("approval.advance.payment")
    #     elif vals.get("type") == "settle":
    #         name = self.env["ir.sequence"].next_by_code("approval.settlements")
    #     return super(PaymentRequest, self).create(vals)

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        round_curr = self.currency_id.round
        for line in self.payment_line:
            if not line.account_id:
                continue
            price_unit = line.unit_price 
            taxes = line.payment_line_tax_ids.compute_all(price_unit, self.currency_id, line.qty, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = round_curr(val['base'])
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += round_curr(val['base'])
        return tax_grouped

    def _prepare_tax_line_vals(self, line, tax):
        """ Prepare values to create an account.invoice.tax line

        The line parameter is an account.invoice.line, and the
        tax parameter is the output of account.tax.compute_all().
        """
        vals = {
            'invoice_id': self.id,
            'name': tax['name'],
            'tax_id': tax['id'],
            'amount': tax['amount'],
            'base': tax['base'],
            'manual': False,
            'sequence': tax['sequence'],
            'account_analytic_id': tax['analytic'] and self.analytic_account_id.id or False,
            'account_id': self.type in ('out_invoice', 'in_invoice') and (tax['account_id'] or line.account_id.id) or (tax['refund_account_id'] or line.account_id.id),
            'analytic_tag_ids': tax['analytic'] and line.analytic_tag_ids.ids or False,
        }

        # If the taxes generate moves on the same financial account as the invoice line,
        # propagate the analytic account from the invoice line to the tax line.
        # This is necessary in situations were (part of) the taxes cannot be reclaimed,
        # to ensure the tax move is allocated to the proper analytic account.
        if not vals.get('account_analytic_id') and self.analytic_account_id and vals['account_id'] == line.account_id.id:
            vals['account_analytic_id'] = self.analytic_account_id.id
        return vals


    def get_log_payment(self):
        csql = """
select * from (select msg.date + interval '7 hour' as tgl_act,p.name as act_by,coalesce(mm.field_desc,'false') as state,mm.new_value_char,msg.body from mail_message msg
join res_partner p on p.id=msg.author_id left join mail_tracking_value mm on mm.mail_message_id=msg.id and mm.field='state'
where msg.model='payment.request' and msg.res_id=%s and msg.subtype_id=2
order by msg.date desc) as data where state<>'false' or length(body)>0
        """
        self.env.cr.execute(csql,(self.id,))
        res = self.env.cr.fetchall()
        return res

    def close_settlement(self):
        self.write({"state": "close"})
    
    def action_create_settlement(self):
        res = self.search([('pembayaran_id','=',self.id)])
        self.write({"is_paid": True})
        if not res:
            line_ids = []
            for line in self.payment_line:
                line_ids += [(0,0, {
                            "product_id": line.product_id.id,
                            "name": line.name,
                            "category_id": line.category_id.id,
                            "account_id": line.account_id.id,
                            "qty": line.qty,
                            "unit_price": line.unit_price,
                            "state": 'open',
                            "amount": line.amount,
                            'bill': line.unit_price * line.qty,
                                      })]
            res = self.create({
                    'employee_id': self.employee_id.id,
                    'partner_id': self.partner_id.id,
                    'user_id': self.env.user.id,
                    'department_id': self.department_id.id,
                    'pembayaran_id': self.id,
                    'type': 'settle',
                    'amount': self.amount,
                    'advance_amount': self.amount,
                    'project_id': self.project_id.id,
                    'journal_id': self.journal_id.id,
                    'analytic_account_id': self.analytic_account_id.id,
                    'account_id': self.journal_id.default_debit_account_id.id,
                    'description': self.description,
                    'payment_plan': self.payment_plan,
                    'payment_line': line_ids,
                    })

        return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.approval_settlements_form_view").id,
                'target': 'current',
                'res_id': res.id,
                }


    def action_create_statement_multi(self,records):
        id_payment = []
        line = []
        journal = records.mapped('journal_id.id')
        project = records.filtered(lambda x: x.payment_type == 'project').mapped('project_id.id')
        if not journal.count(journal[0]) == len(journal):
            raise UserError(('Journal payment request tidak sama.'))
        if project:
            if not project.count(project[0]) == len(project):
                raise UserError(('Tipe payment request tidak sama.'))
        if len(records) < 2:
            raise UserError(('Minimal 2 dokumen payment request untuk melakukan merger.'))
        for i in records:
            budget_id = False
            if i.project_id:
                budget_id = self.env['account.budget.post'].search([('name','=ilike','Project')],order='id',limit=1).id
            else:
                budget_id = self.env['account.budget.post'].search([('name','=ilike','General')],order='id',limit=1).id
            id_payment.append(i.id)
            if i.state != 'done':
                raise UserError(('Harus payment request yang berstatus Approved untuk membuat statement.'))
                       
            line += i.action_create_statement(False, multi=True)
        
        header =  "Multiple Payment Request %s" % (datetime.now().strftime('%d/%m/%Y'))
        obj_bank = self.env['account.bank.statement']
        bs = obj_bank.create({
            'name': header,
            'journal_id': journal[0],
            'general_budget_id': budget_id,
            'multiple': True,
            'accounting_date': datetime.now(),
            'line_ids': line
        })
        pr = self.env['payment.request'].search([('id', 'in', id_payment)])
        for p in pr:
            p.statement_id = bs.id
        
        return {
            'name': 'Created Bank/Cash Statement',
            'type': 'ir.actions.act_window',
            'res_model': 'account.bank.statement',
            'view_mode': 'form',
            'view_type': 'form',
            'domain': [('id', '=', bs.id)],
        }

        
    def action_create_statement(self, context, multi=False):
        if not self.partner_id:
            if self.payment_type=='medical':
                self.partner_id=self.employee_id.user_id.partner_id.id
            elif self.payment_type=='overbook':
                self.partner_id = self.env.user.company_id.partner_id.id
        line_ids = []
        
        account_id = (
                self.env["res.company"]
                ._company_default_get("account.account")
                .account_cash_advance
                )
        if not account_id:
            action = self.env.ref("account.action_account_config")
            msg = "Cannot find a cash advance accounts for this company, You should configure it. \nPlease go to Account Configuration."
            raise RedirectWarning(msg, action.id, "Go to the configuration panel")

        if self.type=='settle':
            # for line in self.payment_line:
                line_ids += [(0,0, {
                    "date": self.date,
                    "name": 'Settlement - ' + self.description,
                    # "amount": self.balance,
                    "amount": self.amount_total,
                    "partner_id": self.partner_id.id,
                    "analytic_account_id": self.analytic_account_id.id,
                    "account_id": account_id.id,
                    # 'payment_request_line': line.id,
                                      })]
        elif self.type=='aap':
            line_ids = [(0,0, {
                    "date": self.date,
                    "name": 'Advance Payment - ' + self.description,
                    "amount": -self.amount_total,
                    "partner_id": self.partner_id.id,
                    "analytic_account_id": self.analytic_account_id.id,
                    "account_id": account_id.id,
                                      })]

        elif self.type=='payment':
            convert_amount = 0.0
            if self.is_bill:
                for line in self.payment_line:
                    # convert_amount = self.currency_id._convert(line.amount, self.env.user.company_id.currency_id, self.env.user.company_id, self.date) - line.amount
                    convert_amount = self.currency_id._convert(line.amount, self.env.user.company_id.currency_id, self.env.user.company_id, self.date)
                    line_ids += [(0,0, {
                        "date": self.date,
                        "name": 'Bill Payment - ' + self.description,
                        "amount": -line.amount if self.currency_id.id == self.env.user.company_id.currency_id else -convert_amount,
                        "partner_id": self.partner_id.id,
                        "analytic_account_id": self.analytic_account_id.id,
                        "account_id": line.account_id.id,
                        # "pembayaran_line_id": self.id,
                        'payment_request_line': line.id,
                                      })]
            else:
                # Convert amount from field currency_id to currency_id in res.company
                # convert_amount = self.currency_id._convert(self.amount_total, self.env.user.company_id.currency_id, self.env.user.company_id, self.date) - self.amount_total
                convert_amount = self.currency_id._convert(self.amount_total, self.env.user.company_id.currency_id, self.env.user.company_id, self.date)
                line_ids = [(0,0, {
                    "date": self.date,
                    "name": 'Payment Request - ' + self.description,
                    "amount": -self.amount_total if self.currency_id.id == self.env.user.company_id.currency_id else -convert_amount,
                    "partner_id": self.partner_id.id,
                #    "pembayaran_line_id": self.id,
                    "ref": self.name,
                    "analytic_account_id": self.analytic_account_id.id,
                    "account_id": self.journal_id.default_credit_account_id.id or False,
#                    'payment_request_line': line.id,
                                      })]
            
        budget_id = False
        if self.project_id:
            budget_id = self.env['account.budget.post'].search([('name','=ilike','Project')],order='id',limit=1).id
        else:
            budget_id = self.env['account.budget.post'].search([('name','=ilike','General')],order='id',limit=1).id
        
        bank_statement = self.env['account.bank.statement'].search([('payment_request_id','=',self.id)])
        if not multi:
            if bank_statement:
                return {
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'account.bank.statement',
                    'view_id': self.env.ref("account.view_bank_statement_form").id,
                    'target': 'current',
                    'res_id': bank_statement.id,
                    }
            else:
                return {
                    'name': ('Create Statement'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'account.bank.statement',
                    'view_id': self.env.ref("account.view_bank_statement_form").id,
                    'type': 'ir.actions.act_window',
                    'context': {'default_name': self.name,
                                'default_journal_id': self.journal_id.id,
                                'default_general_budget_id': budget_id,
                                'default_line_ids': line_ids,
                                'default_payment_request_id': self.id,
                                'default_accounting_date': datetime.now(),
                                },
                    'target':'current'
                    }
        else:
            return line_ids

    @api.one
    @api.depends('project_id')
    def compute_analytic(self):
        if self.project_id:
            self.analytic_account_id = self.project_id.analytic_account_id.id
        else:
            self.analytic_account_id = False


    @api.onchange('cara','employee_id')
    def _change_cara(self):
        if self.employee_id.user_id.partner_id:
            if self.payment_type=='overbook':
                self.partner_id = self.env.user.company_id.partner_id.id
            elif self.payment_type=='bill' and len(self.payment_line)>0 and self.is_bill:
                self.partner_id=self.payment_line[0].invoice_id.partner_id.id
            else:
                self.partner_id=self.employee_id.user_id.partner_id.id
        else:
            self.partner_id = False

        if self.cara and self.cara=='cash':
            journal_id = self.env['account.journal'].search([('type','=','cash')],order='id',limit=1).id
            t_domain = {'domain': {'journal_id': [('type', '=', 'cash')]}}
        elif self.cara and self.cara=='transfer':
            journal_id = self.env['account.journal'].search([('type','=','bank')],order='id',limit=1).id
            t_domain = {'domain': {'journal_id': [('type', '=', 'bank')]}}
            if self.partner_id:
                rek_bank = self.env['res.partner.bank'].search([('partner_id','=',self.partner_id.id)],order='id',limit=1)
                if rek_bank:
                    self.nomor_rekening = rek_bank.acc_number
                    self.nama_bank = rek_bank.bank_id.id
                else:
                    self.nomor_rekening = False
                    self.nama_bank = False
                self.nama_rekening = self.partner_id.name
        else:
            journal_id = False
            t_domain = {'domain': {'journal_id': [('type', 'in', ['cash','bank'])]}}
        self.journal_id = journal_id


        return t_domain

    @api.onchange('payment_type')
    def compute_payment_type(self):
        for rec in self:
            if rec.payment_type=='medical':
                rec.partner_id = rec.employee_id.user_id.partner_id.id
            elif rec.payment_type=='overbook':
                rec.partner_id = rec.env.user.company_id.partner_id.id

    @api.onchange('payment_type')
    def _change_payment_type(self):
        t_domain = {'domain': {'journal_id': [('type', 'in', ['cash','bank'])]}}
        if self.payment_type!='project':
            self.project_id=False
        elif self.payment_type=='bill':
            self.payment_line = False
        elif self.payment_type=='overbook':
            t_domain = {'domain': {'journal_id': [('type', '=', 'bank')]}}
            self.partner_id = self.env.user.company_id.partner_id.id
        return t_domain

    
    @api.onchange('advance_type')
    def _change_advance_type(self):
        if self.advance_type!='project_based':
            self.project_id=False

    @api.onchange("pinjaman_id")
    def _change_line(self):
        if self:
            ress = []
            payment = self.pinjaman_id
            if payment:
                for x in payment:
                    isi = {
                        "amount": x.value,
                        "name": x.keperluan,
                        "date": x.date,
                        "state": "open"
                    }
                ress.append(isi)

            self.update({"payment_line": ress})

    @api.onchange("pembayaran_id")
    def _change_line_pembayaran(self):
        ress = []
        payment = self.pembayaran_id
        if payment:
            for x in payment:
                isi = {
                    "amount": x.amount,
                    "name": False,
                    "date": False,
                    "state": "open"
                }
            ress.append(isi)
            self.update({"payment_line": ress})

    @api.onchange('payment_line')
    def onchange_payment_line(self):
        if self.type=='payment':
            amount_bill = sum([x.bill for x in self.payment_line])
            amount_p = self.pembayaran_id.amount
            amount = amount_p - amount_bill
            if all(x.amount == 0 for x in self.payment_line):
                if amount_bill > 0 and amount_p > 0 and amount != 0:
                    isi = {
                        "amount": amount,
                        "name": False,
                        "date": False,
                        "state": "open"
                    }
                    self.update({"payment_line": [isi]})
            self._compute_amount_tax()

    @api.onchange("amount")
    def _change_button(self):
        if self:
            if self.amount > 5000000:
                self.update({"is_gtlimajt": True})
            else:
                self.update({"is_gtlimajt": False})

    @api.multi
    def name_get(self):
        res = []
        for f in self:
            res.append((f.id, "%s - %s - %s" % (f.name, f.amount, f.description)))
        return res
    
    @api.multi
    def get_formview_id(self, access_uid=None):
        """ Update form view id of action to open the invoice """
        if self.type in ('payment'):
            return self.env.ref('aa_payment_request.submit_payment_request_form_view').id
        elif self.type in ('aap'):
            return self.env.ref('aa_payment_request.approval_advance_payment_form_view').id
        elif self.type in ('settle'):
            return self.env.ref('aa_payment_request.approval_settlements_form_view').id

    @api.multi
    def unlink(self):
        for o in self:
            if o.state != "draft":
                raise UserError(("Data tidak bisa dihapus pada state %s ! Data hanya bisa dihapus pada state Draft.") % (o.state))
        return super(PaymentRequest, self).unlink()

    @api.multi
    def payment_draft(self):
        for o in self:
            return o.write({"state": "draft"})

    @api.multi
    def payment_open(self):
        for o in self:
            user_ids = _get_uid_from_group(
                o, o.env.ref("account.group_account_invoice").id
            )
            partner_ids = [o.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            o.message_post(
                body="Waiting to Confirm",
                subject=o.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            o.write({"state": "confirm"})

    @api.multi
    def payment_done(self):
        for o in self:
            # o.write({"state": "done"})
            user_ids = _get_uid_from_group(o, o.env.ref("account.group_account_invoice").id)
            partner_ids = [o.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            o.message_post(
                        body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to create statement. \nThanks",
                        subject= self.name,
                        partner_ids=partner_ids,
                        message_type="notification",
                        subtype="mail.mt_comment",
                    )
            o.write({"state": "done"})
            

            dict_type = {
                'payment': 'Payment Request',
                'aap': 'Cash Advance',
                'settle': 'Settlement'
            }
            form_name = dict_type[self.type]
            self.update({"date_approved": fields.Datetime.now()})

            if o.type == "payment":
                if o.amount > 30000000 and o.state == "confirm":
                    o.message_post(
                        body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                        subject="%s - %s - [Need Verify or Approve]" % (form_name, o.name),
                        partner_ids=partner_ids,
                        message_type="notification",
                        subtype="mail.mt_comment",
                    )
                    o.write({"state": "done2"})
            if o.type == "aap":
                if o.amount > 5000000 and o.state == "confirm":
                    o.message_post(
                        body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                        subject="%s - %s - [Need Verify or Approve]" % (form_name, o.name),
                        partner_ids=partner_ids,
                        message_type="notification",
                        subtype="mail.mt_comment",
                    )
                    o.write({"state": "done2"})
            if o.type == "settle":
                if o.amount > 5000000 and o.state == "confirm":
                    o.message_post(
                        body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                        subject="%s - %s - [Need Verify or Approve]" % (form_name, o.name),
                        partner_ids=partner_ids,
                        message_type="notification",
                        subtype="mail.mt_comment",
                    )
                    o.write({"state": "done2"})
#                invoice_id = o.create_vendor_bill()
#                o.write({"state": "done"})
#                action = {
#                    "name": "Invoices",
#                    "type": "ir.actions.act_window",
#                    "res_model": "account.invoice",
#                    "res_id": invoice_id.id,
#                    "view_mode": "form",
#                    "views": [
#                        (self.env.ref("account.invoice_supplier_form").id, "form")
#                    ],
#                    "target": "current",
#                }
#                return action

            # res = super(PaymentRequest, self).payment_open()
        if self.is_medical:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
            partner = [(6, 0, [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids])]
            form_name = 'Health Claims'

            if partner[0][2][0]:
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                    subject=form_name + ' - ' + self.name + ' - [Need Review]',
                    partner_ids=partner,
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
            self.write({"state": "done"})
    
    def payment_review_hr(self):
        if not self.project_id:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [
                self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
            ]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to verify or approve.\n Thanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "reviewed_hr"})

    def payment_review(self):
        if not self.project_id:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [
                self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
            ]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to verify or approve.\n Thanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "reviewed_ga"})
        else:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [
                self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
            ]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to verify or approve.\n Thanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "reviewed_pm"})

    def payment_review_ga(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "reviewed_ga"})

    def payment_review_pm(self):
        # user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        # partner_ids = [
        #     self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        # ]
        # self.message_post(
        #     body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
        #     subject=self.name,
        #     partner_ids=partner_ids,
        #     message_type="notification",
        #     subtype="mail.mt_comment",
        # )
        
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'ab_role.group_hod_fat')
        self.write({"state": "reviewed_pm"})

    def cek_coa_product(self):
        for line in self.payment_line:
            if not line.account_id:
                raise UserError(_('Untuk melanjutkan Payment Request Submission, Harap menghubungi Team Accounting untuk melengkapi COA pada Product %s') % (line.product_id.name))                
            # not line.product_id.property_account_income_id or 
        
    def payment_review_fat(self):
        self.cek_coa_product()
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'ab_role.group_direktur')
        self.write({"state": "reviewed"})

        if self.balance==0 and self.type=='settle':
            self.genetate_journal_settlement()
        elif self.balance>0 and self.type=='settle':
            self.write({"state": "done"})

        ################################### Penambahan Journal Entries tanpa VB untuk IMPAS #####################

    def genetate_journal_settlement(self):
        move_line=[]
        adv_account_id = (
            self.env["res.company"]
            ._company_default_get("account.account")
            .account_cash_advance
        )
        if not adv_account_id:
            action = self.env.ref("account.action_account_config")
            msg = "Cannot find a cash advance accounts for this company, You should configure it. \nPlease go to Account Configuration."
            raise RedirectWarning(msg, action.id, "Go to the configuration panel")

        if self.type=='settle':
            for line in self.pembayaran_id.statement_id.move_line_ids:
                if line.account_id.id==adv_account_id.id:
                    move_line += [(0, 0, self._prepare_move_line(line,self.analytic_account_id.id,self.advance_amount))]
            for line in self.payment_line:
                move_line += [(0, 0, self._prepare_move_line(line,self.analytic_account_id.id))]

            journal_id = self.env['account.journal'].search([('name','=','Miscellaneous Operations')]).id
            
            move_vals = {
                'ref': self.name,
                'line_ids': move_line,
                'journal_id': journal_id,
                'date': time.strftime("%Y-%m-%d %H:%M:%S"),
                'narration': 'Journal Settlement ' + self.name,
                }
            move = self.env['account.move'].create(move_vals)
            move.post()
            
            self.balance = 0
            self.update({'state': 'close',})
            self.pembayaran_id.update({'state': 'close',})
            
    def _prepare_move_line(self,line,analytic_account_id,amount=False):
        if amount:
            bill = 0
            amount = amount 
            qty = 1
            account_id = line.account_id.id
        else:
            bill = line.bill
            amount = 0
            qty = line.qty
            account_id = line.product_id.property_account_expense_id.id
        return {
            'date_maturity': self.date, 
            'partner_id': self.partner_id.id or False,
            'name': line.name, 
            'debit': bill, 
            'credit': amount, 
            'debit_cash_basis': bill, 
            'credit_cash_basis': amount, 
            'balance_cash_basis': bill - amount, 
            'account_id': account_id, 
            'quantity': qty, 
            'product_id': line.product_id.id, 
            'analytic_account_id': analytic_account_id,
            }

        ################################### Penambahan Journal Entries tanpa VB #####################

    @api.multi
    def payment_submit_hr(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "submit"})

    @api.multi
    def payment_submit(self):
        if len(self.payment_line)==0:
            raise UserError(_('Belum ada laporan Pertanggungjawaban'))
        for line in self.payment_line:
            if not line.product_id and not self.is_medical and not line.invoice_id and self.payment_type not in ('overbook','bill'):
                raise UserError(_('Belum memilih Product'))
            elif not line.product_id and self.payment_type in ('medical'):
                raise UserError(_('Product wajib diisi dengan Reimbusement Kesehatan Karyawan'))
            
        if self.project_id:
            user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'project.group_project_manager')
            self.write({"state": "submit"})

        elif self.payment_type == 'general':
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_general_affair").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "submit"})
        
        elif self.payment_type in ('bill', 'overbook'):
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "submit"})

        elif self.payment_type in ('hrd','medical'):
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "submit"})

    @api.multi
    def advance_payment_submit(self):
        if self.partner_id and self.cara=='transfer':
            rek_bank = self.env['res.partner.bank'].search([('partner_id','=',self.partner_id.id)],order='id',limit=1)
            if not rek_bank:
                self.env['res.partner.bank'].create({'partner_id': self.partner_id.id,
                                                     'acc_number': self.nomor_rekening,
                                                     'acc_type': 'bank',
                                                     'acc_holder_name': self.nama_rekening,
                                                     'bank_id': self.nama_bank.id,
                                                    })
        if self.project_id:
            user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'project.group_project_manager')
        else:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
        self.write({"state": "submit"})

    @api.multi
    def settlement_submit(self):
        for o in self:
            form_name = ''
            if o.is_medical:
                user_ids = _get_uid_from_group(o, o.env.ref("ab_role.group_hod_human_resource").id)
                partner = [(6, 0, [o.env["res.users"].browse(uid).partner_id.id for uid in user_ids])]
                form_name = 'Health Claims'
            else:
                fat = _get_uid_from_group(o, o.env.ref("project.group_project_manager").id)
                partner = [(6, 0, [o.env["res.users"].browse(uid).partner_id.id for uid in fat])]
                form_name = 'Payment Request'
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to review. Thanks",
                    subject=self.name,
                    partner_ids=[o.employee_id.parent_id.user_id.partner_id.id],
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
                self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'project.group_project_manager')

            if partner[0][2][0]:
                o.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                    subject=form_name + ' - ' + o.name + ' - [Need Review]',
                    partner_ids=partner,
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
            o.write({"state": "submit"})

    @api.constrains("pembayaran_id", "amount")
    def _check_pembayaran_id(self):
        if self.pembayaran_id:

            if self.pembayaran_id.amount < self.amount and self.type == "IN":
                raise UserError(
                    "Advance amount harus lebih kecil nilainya atau sama dengan"
                )

            if self.pembayaran_id.amount < self.amount and self.type == "settle":
                raise UserError(
                    "Advance amount harus lebih kecil nilainya atau sama dengan"
                )

    def create_vendor_bill(self):
        for payment in self:

            currency = self.env.user.company_id.currency_id

            journal_domain = [
                ("type", "=", "purchase"),
                ("currency_id", "=", currency.id),
            ]

            journal_id = self.env["account.journal"].search(journal_domain, limit=1)
            invoice_id = payment._prepare_invoice(journal_id)
            invoice_id = self.env["account.invoice"].create(invoice_id)
            invoice_lines = payment._prepare_invoice_lines(journal_id)
            for x in invoice_lines:
                invoice_id.write({"invoice_line_ids": [(0, 0, x)]})
            return invoice_id

    def _prepare_invoice(self, journal_id):

        account_id = (
            self.env["res.company"]
            ._company_default_get("account.account")
            .account_cash_advance
        )
        if not account_id:
            action = self.env.ref("account.action_account_config")
            msg = "Cannot find a cash advance accounts for this company, You should configure it. \nPlease go to Account Configuration."
            raise RedirectWarning(msg, action.id, "Go to the configuration panel")

        data = {
            "type": "in_invoice",
            "partner_id": self.partner_id.id,
            "journal_id": journal_id.id,
            "account_id": account_id.id,
        }
        return data

    def _prepare_invoice_lines(self, journal):
        lines = []
        account_id = journal.default_debit_account_id.id
        for line in self.payment_line:
            if line.bill > 0:
                data = {}
                data["name"] = line.name
                data["price_unit"] = line.bill
                data["account_id"] = account_id
                lines.append(data)
        return lines

    @api.multi
    def cetak_payment_request(self):
        return self.env.ref("aa_payment_request.cetak_payment_request").report_action(self)

    @api.multi
    def cetak_advance_payment(self):
        return self.env.ref("aa_payment_request.cetak_advance_payment").report_action(self)

    @api.multi
    def cetak_settlement(self):
        return self.env.ref("aa_payment_request.cetak_settlement").report_action(self)

class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    payment_id = fields.Many2one("payment.request")
#    vendorbill_id = fields.Many2one("account.invoice")
    is_payment = fields.Boolean(string="payment")
    is_medical = fields.Boolean()

    # type = fields.Selection(
    #     selection_add=[
    #         ("payment", "Payment Request"),
    #         ("aap", "Approval Advance Payment"),
    #         ("settle", "Approval Settlements"),
    #     ]
    # )

    type = fields.Selection(
        [
            ("url", "URL"),
            ("in_invoice", "Vendor Bill"),
            ("binary", "File"),
            ("payment", "Payment Request"),
            ("aap", "Approval Advance Payment"),
            ("settle", "Approval Settlements"),
        ],
        string="Type",
        required=True,
        change_default=True,
        help="You can either upload a file from your computer or copy/paste an internet link to your file.",
    )

class PaymentRequestTax(models.Model):
    _name = "payment.request.tax"
    _description = "Invoice Tax"
    _order = 'sequence'

    @api.depends('payment_id.payment_line')
    def _compute_base_amount(self):
        tax_grouped = {}
        for payment in self.mapped('payment_id'):
            tax_grouped[payment.id] = payment.get_taxes_values()
        for tax in self:
            tax.base = 0.0
            if tax.tax_id:
                key = tax.tax_id.get_grouping_key({
                    'tax_id': tax.tax_id.id,
                    'account_id': tax.account_id.id,
                    'account_analytic_id': tax.account_analytic_id.id,
                    'analytic_tag_ids': tax.analytic_tag_ids.ids or False,
                })
                if tax.payment_id and key in tax_grouped[tax.payment_id.id]:
                    tax.base = tax_grouped[tax.payment_id.id][key]['base']
                else:
                    _logger.warning('Tax Base Amount not computable probably due to a change in an underlying tax (%s).', tax.tax_id.name)

    payment_id = fields.Many2one('payment.request', string='Payment Request', ondelete='cascade', index=True)
    name = fields.Char(string='Tax Description', required=True)
    tax_id = fields.Many2one('account.tax', string='Tax', ondelete='restrict')
    account_id = fields.Many2one('account.account', string='Tax Account', required=True, domain=[('deprecated', '=', False)])
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account')
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')
    amount = fields.Monetary('Tax Amount')
    amount_rounding = fields.Monetary('Amount Delta')
    amount_total = fields.Monetary(string="Amount Total", compute='_compute_amount_total')
    manual = fields.Boolean(default=True)
    sequence = fields.Integer(help="Gives the sequence order when displaying a list of invoice tax.")
    company_id = fields.Many2one('res.company', string='Company', related='account_id.company_id', store=True, readonly=True)
    currency_id = fields.Many2one('res.currency', related='payment_id.currency_id', store=True, readonly=True)
    base = fields.Monetary(string='Base', compute='_compute_base_amount', store=True)

    @api.depends('amount', 'amount_rounding')
    def _compute_amount_total(self):
        for tax_line in self:
            tax_line.amount_total = tax_line.amount + tax_line.amount_rounding



class PaymentRequestLine(models.Model):
    _name = "payment.request.line"

    @api.one
    @api.depends('unit_price', 'payment_line_tax_ids', 'qty','invoice_id',
        'product_id', 'payment_id.partner_id', 'payment_id.currency_id', 'payment_id.date')
    def _compute_price(self):
        currency = self.payment_id and self.payment_id.currency_id or None
        price = self.unit_price 
        taxes = False
        if self.payment_line_tax_ids:
            taxes = self.payment_line_tax_ids.compute_all(price, currency, self.qty, product=self.product_id, partner=self.payment_id.partner_id)
        self.price_subtotal = taxes['total_excluded'] if taxes else self.qty * price

        if self.product_id.product_tmpl_id.property_account_expense_id:
            self.account_id = self.product_id.product_tmpl_id.property_account_expense_id.id
            
        if self.type=='aap':
            self.advance_amount = float(self.qty) * self.unit_price
            self.amount = self.advance_amount
        elif self.type=='settle':
            self.advance_amount = float(self.qty) * self.unit_price
            self.bill = self.advance_amount
            self.amount = self.advance_amount
        elif self.type=='payment':
            if self.invoice_id:
                self.category_id = False
                self.product_id = False
#                self.account_id = False
#                self.unit_price = False
                self.payment_line_tax_ids = False
                self.amount = self.qty * self.unit_price
                self.payment_id.partner_id = self.invoice_id.partner_id.id
            else:
                self.invoice_id = False
                self.amount = taxes['total_excluded'] if taxes else self.price_subtotal
                
        if self.type=='aap' or self.type=='settle':
            categ_id = self.env['category.payment'].search([('name','=ilike','Cash Advance')],order='id',limit=1)
            self.category_id = categ_id.id

            


    payment_id = fields.Many2one("payment.request", "STO Reference", required=True, ondelete="cascade")
    name = fields.Char("Description", required=True)
    invoice_id = fields.Many2one("account.invoice", "Vendor Bill",
                                 domain=[("state", "=", "open"), ("type", "=", "in_invoice")])
    category_id = fields.Many2one(comodel_name="category.payment", string="Category")
    date = fields.Date(string="Date")
    amount = fields.Monetary("Amount", digits=dp.get_precision("Product Price"))
    persen = fields.Float(string="Persentase %")
    is_bill = fields.Boolean(string="Vendor Bill", related='payment_id.is_bill')
    bill = fields.Float(string="Bill Amount", digits=dp.get_precision("Product Price"), store=True)
    product_id = fields.Many2one('product.product',string='Product',domain=[('purchase_ok', '=', True)])
    product_category = fields.Many2one('product.category',string='Category',store=True)
    qty = fields.Integer(string='Qty',default=1)
    unit_price = fields.Float(string='Unit Price',default=0.0)
    price_subtotal = fields.Monetary(string='Amount (without Taxes)',
        store=True, readonly=True, compute='_compute_price', help="Total amount without taxes")
#    tax_id = fields.Many2one('account.tax',string='Tax',domain=[('type_tax_use', '=', 'purchase')])
    payment_line_tax_ids = fields.Many2many('account.tax',
        'account_payment_line_tax', 'payment_line_id', 'tax_id',
        string='Taxes', domain=[('type_tax_use','!=','none'), '|', ('active', '=', False), ('active', '=', True)], store=True)
    advance_amount = fields.Monetary("Amount", digits=dp.get_precision("Product Price"),compute='_compute_price')
    account_id = fields.Many2one('account.account', string='Account', domain=[('deprecated', '=', False)],
        store=True, help="The expense account related to the selected product.")
    type = fields.Selection(
        [
            ("payment", "Payment Request"),
            ("aap", "Approval Advance Payment"),
            ("settle", "Approval Settlements"),
        ],
        string="Type",
        store=True,
        related="payment_id.type",
    )
    currency_id = fields.Many2one(
        related="payment_id.currency_id",
        depends=["payment_id"],
        store=True,
        string="Currency",
    )

    state = fields.Selection(
        [("open", "Open"), ("paid", "Paid")],
        string="Status",
        readonly=True,
        default="open",
    )
    analytic_account_id = fields.Many2one("account.analytic.account", string="Analytic Account")
    overtime_id = fields.Many2one("hr.overtime", string="Overtime", domain=[('state', 'in', ['done', 'paid']), ('payment_request_line', '=', False)])
    payment_type = fields.Selection(
        [
            ("project", "Project based"),
            ("general", "General"),
            ("hrd", "Human Resources"),
            ("medical", "Health Claim"),
            ("overbook", "Over Booking"),
            ("bill","Vendor Bill"),
        ],
        store=True,
        related="payment_id.payment_type",
        string="Payment Type",
    )
    

    @api.onchange("product_id")
    def onchange_product_category(self):
        if not self.product_category:
            self.product_category = self.product_id.categ_id.id

        if not self.product_id.default_expense_account_id:
            self.account_id = self.product_id.default_expense_account_id.id

    @api.onchange("product_category")
    def onchange_product_category(self):
        t_domain = False
        if self.product_category:
            products = self.env['product.product'].search([('categ_id','child_of',self.product_category.id)])
            t_domain = {'domain': {'product_id': [('id', 'in', products.ids)]}}
            return t_domain
            

    @api.onchange("persen")
    def _persen(self):
        for i in self:
            i.amount = i.persen / 100 * i.amount

        
    @api.onchange('bill')
    def onchange_bill(self):
        if self.bill > 0:
            self.amount = 0
            self.state = False

        if self.payment_id.type=='payment':
            if self.invoice_id:
                self.unit_price = self.invoice_id.amount_total
                self.amount = self.invoice_id.amount_total
                self.payment_id.partner_id = self.invoice_id.partner_id.id
            else:
                self.invoice_id = False
                self.amount = taxes['total_included'] if taxes else self.price_subtotal
                

    # @api.model
    # def default_get(self, default_fields):
    #     res = super(PaymentRequestLine, self).default_get(default_fields)
    #     res_ids = self._context.get('payment_line')

    #     # res.update({'urutan': hasil+1})
    #     return res

    @api.onchange("invoice_id")
    def onchange_invocie_id(self):
        if self.invoice_id:
            self.amount = self.invoice_id.amount_total
            self.name = "Pembayaran Vendor Bill"
            self.account_id = self.invoice_id.account_id.id
            if self.invoice_id.project_id:
                self.project_id = self.payment_id.project_id.id
                
    @api.onchange("overtime_id")
    def onchange_overtime_id(self):
        if self.overtime_id:
            self.unit_price = self.overtime_id.lembur

    @api.multi
    def name_get(self):
        res = []
        for field in self:
            res.append((field.id, "[%s] %s" % (field.payment_id.name, field.name)))
        return res

    @api.model
    def name_search(self, name="", args=None, operator="ilike", limit=100):
        if operator not in ("ilike", "like", "=", "=like", "=ilike"):
            return super(PaymentRequestLine, self).name_search(
                name, args, operator, limit
            )
        args = args or []
        domain = ["|", ("payment_id.name", operator, name), ("name", operator, name)]
        recs = self.search(domain + args, limit=limit)
        return recs.name_get()


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    pembayaran_id = fields.Many2one("payment.request", "Payment Request", readonly=True, copy=False)
    project_id = fields.Many2one("project.project", string="Project")
#    attachment_lines = fields.One2many("ir.attachment", "vendorbill_id", string="Attachment List")
    # number = fields.Char(related='move_id.name', store=True, readonly=True, copy=False)
    attachment_ids = fields.Many2many(comodel_name="ir.attachment", string="Attachment List")
    description = fields.Text(string="Description")
    payment_request_line = fields.One2many('payment.request.line','invoice_id',)
    payment_count = fields.Integer(string='Jumlah Simpanan', compute='_compute_payment_count', readonly=True)
    payment_total = fields.Monetary("Payment Total", compute='_compute_payment_count')
    statement_id = fields.Many2one('account.bank.statement',string='Bank Statement', readonly=True, store=True, copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('review_fat', 'Reviewed FAT'),
        ('review_ga', 'Reviewed GA'),
        ('approve', 'Reviewed PM'),
        ('review_fin', 'Reviewed FIN'),
        ('open', 'Open'),
        ('in_payment', 'In Payment'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Open' status is used when user creates invoice, an invoice number is generated. It stays in the open status till the user pays the invoice.\n"
             " * The 'In Payment' status is used when payments have been registered for the entirety of the invoice in a journal configured to post entries at bank reconciliation only, and some of them haven't been reconciled with a bank statement line yet.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")

    def get_log_payment(self):
        csql = """
select * from (select msg.date + interval '7 hour' as tgl_act,p.name as act_by,coalesce(mm.field_desc,'false') as state,mm.new_value_char,msg.body from mail_message msg
join res_partner p on p.id=msg.author_id left join mail_tracking_value mm on mm.mail_message_id=msg.id and mm.field='state'
where msg.model='account.invoice' and msg.res_id=%s and msg.subtype_id=2
order by msg.date desc) as data where state<>'false' or length(body)>0
        """
        self.env.cr.execute(csql,(self.id,))
        res = self.env.cr.fetchall()
        return res
    
    def write(self, vals):
        for i in self:
            if i.attachment_ids:
                for ai in i.attachment_ids:
                    if ai.res_id == 0 or False:
                        ai.update({'res_id': i.id})
        res = super(AccountInvoice, self).write(vals)
        return res
    
    # def create(self, vals):
    #     res = super(AccountInvoice, self).create(vals)
    #     if res.attachment_ids or vals["attachment_ids"]:
    #         for ai in res.attachment_ids:
    #             if ai.res_id == 0 or False:
    #                 ai.update({'res_id': res.id})
    #     return res

    @api.multi
    @api.depends('payment_request_line')
    def _compute_payment_count(self):
        for rec in self:
            rec.payment_count = len(rec.mapped('payment_request_line'))
            rec.payment_total = rec.amount_total - sum(rec.mapped('payment_request_line.amount'))
            
    @api.multi
    def action_view_payment(self):
        action = self.env.ref(
            'aa_payment_request.submit_payment_request_action').read()[0]
        lines = self.mapped('payment_request_line.payment_id')
        if len(lines) > 1:
            action['domain'] = [('id', 'in', lines.ids)]
        elif lines:
            action['views'] = [(self.env.ref(
                'aa_payment_request.submit_payment_request_form_view').id, 'form')]
            action['res_id'] = lines.ids[0]
        return action

    @api.multi
    def action_draft(self):
        # go from canceled state to draft state
        self.write({'state': 'draft', 'date': False})
        # Delete former printed invoice
        try:
            report_invoice = self.env['ir.actions.report']._get_report_from_name('account.report_invoice')
        except IndexError:
            report_invoice = False
        if report_invoice and report_invoice.attachment:
            for invoice in self:
                with invoice.env.do_in_draft():
                    invoice.number, invoice.state = invoice.move_name, 'open'
                    attachment = self.env.ref('account.account_invoices').retrieve_attachment(invoice)
                if attachment:
                    attachment.unlink()
        return True

    @api.model
    def invoice_validate(self):
        res = super(AccountInvoice, self).invoice_validate()
        if self.type == 'in_invoice':
            self.write({'state': 'submit'})
            user_ids = _get_uid_from_group(
                self, self.env.ref("ab_role.group_hod_general_affair").id
            )
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                subject= self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
        else:
            self.write({'state': 'submit'})
            user_ids = _get_uid_from_group(
                self, self.env.ref("ab_role.group_hod_fat").id
            )
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                subject= self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
        return res

    def invoice_submit(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review or Approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
            )
        self.write({"state": "submit"})
        return super(AccountInvoice, self).action_move_create()
        
    #   return super(AccountInvoice, self).action_move_create()
    #   return super(AccountInvoice, self).action_invoice_open()

    def review(self):
        if self.type == 'in_invoice':
            # self._send_message('project.group_project_manager', 'Vendor Bills - %s - [Need Review]' % (
            #     self.number), '[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to verify or approve. \nThanks')
            self.env['send.email.notification'].send(self.id, 'account.invoice', 'review', 'group', 'project.group_project_manager')
            return self.write({'state': 'review_ga'})
        else:
            # self._send_message('ab_role.group_direktur', 'Customer Invoice - %s - [Need Approve]' % (
            #     self.number), '[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to verify or approve. \nThanks')
            self.env['send.email.notification'].send(self.id, 'account.invoice', 'review', 'group', 'ab_role.group_direktur')
            return self.write({'state': 'review_fat'})

    def approve_action(self):
        if self.type == 'in_invoice':
            self._send_message('account.group_account_invoice', 'Vendor Bills - %s - [Need Process]' % (self.number),
                               '[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to process. \nThanks')
        return self.write({'state': 'approve'})

    def review_fin(self):
        if self.type == 'in_invoice':
            self._send_message('ab_role.group_direktur', 'Vendor Bills - %s - [Need Process]' % (self.number),
                               '[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to process. \nThanks')
        return self.write({'state': 'review_fin'})

    def action_to_open(self):
        self.write({'state': 'open'})

    def action_to_open_pur(self):
        user_ids = _get_uid_from_group(self, self.env.ref("purchase.group_purchase_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to create Payment Request.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
            )
        self.write({'state': 'open'})
    
    def action_to_open_hr(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to create Payment Request.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
            )
        self.write({'state': 'open'})

    def _send_message(self, group_user, subject, message_body):
        user_ids = _get_uid_from_group(self, self.env.ref(group_user).id)
        partner_ids = [(4, self.env['res.users'].browse(uid).partner_id.id) for uid in user_ids] if user_ids else []

        if partner_ids:
            self.message_post(subject=subject, body=message_body, partner_ids=partner_ids, message_type="notification", subtype="mail.mt_comment")

    @api.multi
    def cetak_vendor_bill(self):
        return self.env.ref("aa_payment_request.cetak_vendor_bill").report_action(self)
    
    def action_create_payment_vb(self):
#        Create Payment Request
        line_ids = []
        line_ids += [(0,0, {
                            "invoice_id": self.id,
                            "name": 'Pembayaran Vendor Bill ' + self.number,
                            "qty": 1,
                            "unit_price": self.residual,
                            "state": 'open',
                            "amount": self.residual,
                            "account_id": self.account_id.id,
                                      })]
        
        payment = False #self.env['payment.request.line'].search([('invoice_id','=',self.id)],limit=1)
        employee = self.env['hr.employee'].search([('user_id','=',self.create_uid.id)],limit=1)
        payment_type = 'bill'
            

        if payment:
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'target': 'current',
                'res_id': payment.payment_id.id,
                }
        else:
            return {
                'name': ('Create Payment Request'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'type': 'ir.actions.act_window',
                'context': {'default_name': self.number,
                            'default_journal_id': self.journal_id.id,
                            'default_payment_line': line_ids,
                            'default_date': datetime.now(),
                            'default_employee_id': employee.id or False,
                            'default_partner_id': self.partner_id.id,
                            'default_payment_type': payment_type,
                            'default_is_bill': True,
                            'default_currency_id': self.currency_id.id,
                            'default_type': 'payment',
                            'default_description': 'Pembayaran Vendor Bill ' + self.number,
                            'default_reference': self.number,
                            },
                'target':'current'
                }

    def action_create_payment(self):
#        Create Payment Request
        line_ids = []
        line_ids += [(0,0, {
                            "invoice_id": self.id,
                            "name": 'Pembayaran Vendor Bill ' + self.number,
                            "qty": 1,
                            "unit_price": self.residual,
                            "state": 'open',
                            "amount": self.residual,
                            "account_id": self.account_id.id,
                                      })]
        
        payment = False #self.env['payment.request.line'].search([('invoice_id','=',self.id)],limit=1)
        employee = self.env['hr.employee'].search([('user_id','=',self.create_uid.id)],limit=1)

        if self.project_id:
            payment_type = 'project'
        elif self.payment_request_line.invoice_id:
            payment_type = 'bill'
        else:
            payment_type = 'general'
            

        if payment:
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'target': 'current',
                'res_id': payment.payment_id.id,
                }
        else:
            return {
                'name': ('Create Payment Request'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'type': 'ir.actions.act_window',
                'context': {'default_name': self.number,
                            'default_journal_id': self.journal_id.id,
                            'default_payment_line': line_ids,
                            'default_date': datetime.now(),
                            'default_employee_id': employee.id or False,
                            'default_partner_id': self.partner_id.id,
                            'default_payment_type': payment_type,
                            'default_is_bill': True,
                            'default_type': 'payment',
                            'default_description': 'Pembayaran Vendor Bill ' + self.number,
                            'default_reference': self.number,
                            },
                'target':'current'
                }

    def action_create_invoice_statement(self):
#        raise UserError(("Create Invoice Receive Statement %s !") % (self.name))
        line_ids = []
        for line in self.invoice_line_ids:
            line_ids += [(0,0, {
                        "date": datetime.now(),
                        "name": 'Invoice ' + self.number + ' - ' + self.partner_id.name,
                        "amount": self.residual,
                        "partner_id": self.partner_id.id,
                        "account_id": self.account_id.id,
                        "ref": self.reference,
                                      })]
        if self.statement_id:
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'account.bank.statement',
                'view_id': self.env.ref("account.view_bank_statement_form").id,
                'target': 'current',
                'res_id': self.statement_id.id,
                }
        else:
            return {
                'name': ('Create Invoice Statement'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.bank.statement',
                'view_id': self.env.ref("account.view_bank_statement_form").id,
                'type': 'ir.actions.act_window',
                'context': {'default_name': 'Invoice ' + self.number + ' - ' + self.partner_id.name,
                            'default_journal_id': self.journal_id.id,
                            'default_line_ids': line_ids,
                            'default_accounting_date': datetime.now(),
                            },
                'target':'current'
                }

class AccountBankStatement(models.Model):
    _inherit = "account.bank.statement"

    general_budget_id = fields.Many2one(
        "account.budget.post", string="Budgetary Position"
    )
    no_fp = fields.Char(string='No. FP')
    excess_account = fields.Many2one('account.account', string='')
    payment_request_id = fields.Many2one('payment.request',string='Payment Request')
    multiple = fields.Boolean(string='Multiple')

    @api.multi
    def write(self, vals):
        for rec in self:
            if rec.payment_request_id:
                    rec.payment_request_id.update({'statement_id': rec.id, 'state': 'paid'})
        res = super(AccountBankStatement, self).write(vals)
        return res

    @api.onchange("balance_end")
    def onchange_balance_end(self):
        if self.balance_end:
            return {"value": {"balance_end_real": self.balance_end}}

    @api.multi
    def check_confirm_bank(self):
        for l in self.line_ids:
            # l.pembayaran_line_id.state = 'paid'
            if l.pembayaran_line_id.invoice_id:
                l.pembayaran_line_id.invoice_id.pembayaran_id = (
                    l.pembayaran_line_id.payment_id.id
                )

#        super(AccountBankStatement, self).check_confirm_bank()
        if self.journal_type == 'cash' and not self.currency_id.is_zero(self.difference):
            action_rec = self.env['ir.model.data'].xmlid_to_object('account.action_view_account_bnk_stmt_check')
            if action_rec:
                action = action_rec.read([])[0]
                return action
        if self.payment_request_id:
            return self.button_confirm_bank_falah()
        elif self.multiple:
            return self.button_confirm_multiple()
        else:
            return self.button_confirm_bank()

    def button_confirm_bank(self):
        res = super(AccountBankStatement, self).button_confirm_bank()
        if self.invoice_id:
            inv = self.invoice_id
            payment_line = self.env['account.move.line'].search([('statement_id','=',self.id),('account_id','!=',self.account_id.id)])
            customer_invoice = inv.move_id.line_ids.filtered(lambda r: r.account_id.id==self.line_ids[0].account_id.id)
            (customer_invoice + payment_line).reconcile()
        return res

    
    @api.multi
    def button_confirm_multiple(self):
        self.balance_end_real = self.balance_end
        move_line = pajak_line = []
        pajak = tax_incl = tax_excl = 0.0
        self._balance_check()
        statements = self.filtered(lambda r: r.state == 'open')
        for res in self.line_ids:
            move_line = []
            credit_amount = 0
            tax_incl = 0
            payment_request = self.env['payment.request'].search([('name', '=', res.ref)],limit=1)
            tax_group = payment_request.get_taxes_values()
            for line in payment_request.payment_line:
                move_line += [(0, 0, self._prepare_move_line_st(line,payment_request.analytic_account_id.id,line.amount,0,res.id))]
            if len(tax_group)>0:
                for line_tax in tax_group:
                    tax_name = tax_group.get(line_tax,{}).get('name',False)
                    tax_id = tax_group.get(line_tax,{}).get('tax_id',False)
                    tax_account = self.env['account.tax'].search([('id','=',int(tax_id))])
                    tax_amount = tax_group.get(line_tax,{}).get('amount',False)
                    if tax_account.price_include:
                        debit = tax_amount
                        credit = 0
                        tax_incl += tax_amount
                    elif tax_amount<0:
                        debit = 0
                        credit = -tax_amount
                        tax_incl += tax_amount
                    else:
                        debit = tax_amount
                        credit = 0
                        tax_incl += tax_amount
                    move_line += [(0, 0, {
                            'date_maturity': self.date, 
                            'partner_id': payment_request.partner_id.id or False,
                            'name': tax_name, 
                            'debit': debit, 
                            'credit': credit, 
                            'debit_cash_basis': debit, 
                            'credit_cash_basis': credit, 
                            'balance_cash_basis': debit - credit, 
                            'account_id': tax_account.account_id.id, 
                            'quantity': 1, 
                            'analytic_account_id': payment_request.analytic_account_id.id,
                            'statement_id': self.id,
                            })]
                    
        
            credit_amount = payment_request.amount + tax_incl #self.line_ids.amount 
            move_line += [(0, 0, {
                    'date_maturity': self.date, 
                    'partner_id': payment_request.partner_id.id or False,
                    'name': res.name, #self.account_id.name, 
                    'debit': 0, 
                    'credit': credit_amount, 
                    'debit_cash_basis': 0, 
                    'credit_cash_basis': credit_amount, 
                    'balance_cash_basis': -credit_amount, 
                    'account_id': payment_request.journal_id.default_credit_account_id.id, 
                    'quantity': 1, 
                    'analytic_account_id': payment_request.analytic_account_id.id,
                    'statement_id': self.id,
                    })]
            if payment_request.payment_type=='bill':
                journal_id = payment_request.journal_id.id
            else:
                journal_id = self.env['account.journal'].search([('name','=','Miscellaneous Operations')]).id
            
            move_vals = {
                'ref': payment_request.name,
                'line_ids': move_line,
                'journal_id': journal_id,
                'date': self.date,
                'narration': 'Payment Request ' + payment_request.name,
                }
            move = self.env['account.move'].create(move_vals)
            move.post()
            payment_request.state = 'paid'
            if payment_request.is_bill:
                inv = False
                for line in payment_request.payment_line:
                    inv = line.invoice_id
                payment_line = self.env['account.move.line'].search([('move_id','=',move.id),('account_id','!=',self.account_id.id)])
                line_to_reconcile = self.env['account.move.line']
                line_to_reconcile += inv.move_id.line_ids.filtered(lambda r: r.account_id.id==res.account_id.id)
                (line_to_reconcile + payment_line).reconcile(False, False)
        statements.write({'state': 'confirm', 'date_done': time.strftime("%Y-%m-%d %H:%M:%S")})
        
    @api.multi
    def button_confirm_bank_falah(self):
        self.balance_end_real = self.balance_end
        move_line = pajak_line = []
        pajak = tax_incl = tax_excl = 0.0
        # amount_base = self.payment_request_id.payment_line("amount")
        self._balance_check()
        statements = self.filtered(lambda r: r.state == 'open')
        self.payment_request_id.payment_line.write({'state': 'paid'}) 
        if self.payment_request_id.type=='payment':
            tax_group = self.payment_request_id.get_taxes_values()
            for line in self.payment_request_id.payment_line:
                payment_request_amount = self.payment_request_id.currency_id._convert(line.amount, self.env.user.company_id.currency_id, self.env.user.company_id, self.date)
                move_line += [(0, 0, self._prepare_move_line_st(line,self.payment_request_id.analytic_account_id.id,payment_request_amount,0,self.line_ids[0].id))]
                # move_line += [(0, 0, self._prepare_move_line_st(line,self.payment_request_id.analytic_account_id.id,line.amount,0,self.line_ids[0].id))]

            if len(tax_group)>0:
                for line_tax in tax_group:
                    tax_name = tax_group.get(line_tax,{}).get('name',False)
                    tax_id = tax_group.get(line_tax,{}).get('tax_id',False)
                    tax_account = self.env['account.tax'].search([('id','=',int(tax_id))])
                    tax_amount = tax_group.get(line_tax,{}).get('amount',False)
                    if tax_account.price_include:
                        debit = tax_amount
                        credit = 0
                        tax_incl += tax_amount
                    elif tax_amount<0:
                        debit = 0
                        credit = -tax_amount
                        tax_incl += tax_amount
                    else:
                        debit = tax_amount
                        credit = 0
                        tax_incl += tax_amount
                    move_line += [(0, 0, {
                            'date_maturity': self.date, 
                            'partner_id': self.payment_request_id.partner_id.id or False,
                            'name': tax_name, 
                            'debit': debit, 
                            'credit': credit, 
                            'debit_cash_basis': debit, 
                            'credit_cash_basis': credit, 
                            'balance_cash_basis': debit - credit, 
                            'account_id': tax_account.account_id.id, 
                            'quantity': 1, 
                            'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                            'statement_id': self.id,
                            })]
            payment_request_amount = self.payment_request_id.currency_id._convert(self.payment_request_id.amount, self.env.user.company_id.currency_id, self.env.user.company_id, self.date)
            credit_amount = payment_request_amount + tax_incl #self.line_ids.amount 
            
            move_line += [(0, 0, {
                    'date_maturity': self.date, 
                    'partner_id': self.payment_request_id.partner_id.id or False,
                    'name': self.line_ids.name, #self.account_id.name, 
                    'debit': 0, 
                    'credit': credit_amount, 
                    'debit_cash_basis': 0, 
                    'credit_cash_basis': credit_amount, 
                    'balance_cash_basis': -credit_amount, 
                    'account_id': self.payment_request_id.journal_id.default_credit_account_id.id, 
                    'quantity': 1, 
                    'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                    'statement_id': self.id,
                    })]
            
            if self.payment_request_id.payment_type=='bill':
                journal_id = self.payment_request_id.journal_id.id
            else:
                journal_id = self.env['account.journal'].search([('name','=','Miscellaneous Operations')]).id
            
            move_vals = {
                'ref': self.payment_request_id.name,
                'line_ids': move_line,
                'journal_id': journal_id,
                'date': self.date,
                'narration': 'Payment Request ' + self.payment_request_id.name,
                }
            
# ============================================================================================================================
# ============================================================================================================================
# ============================================================================================================================
        elif self.payment_request_id.type=='aap':
            journal_id = self.payment_request_id.journal_id.id
            move_line += [(0, 0, {
                    'date_maturity': self.date, 
                    'partner_id': self.payment_request_id.partner_id.id or False,
                    'name':  self.line_ids.name, #self.account_id.name,
                    'debit': self.payment_request_id.amount, 
                    'credit': 0, 
                    'debit_cash_basis': self.payment_request_id.advance_amount, 
                    'credit_cash_basis': 0, 
                    'balance_cash_basis': self.payment_request_id.advance_amount, 
                    'account_id': self.line_ids[0].account_id.id, 
                    'quantity': 1, 
                    'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                    'statement_id': self.id,
                    'statement_line_id': self.line_ids[0].id,
                    })]
            move_line += [(0, 0, {
                    'date_maturity': self.date, 
                    'partner_id': self.payment_request_id.partner_id.id or False,
                    'name': self.line_ids.name, 
                    'debit': 0, 
                    'credit': self.payment_request_id.amount, 
                    'debit_cash_basis': 0, 
                    'credit_cash_basis': self.payment_request_id.advance_amount, 
                    'balance_cash_basis': -self.payment_request_id.advance_amount, 
                    'account_id': self.payment_request_id.journal_id.default_credit_account_id.id, 
                    'quantity': 1, 
                    'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                    'statement_id': self.id,
                    'statement_line_id': self.line_ids[0].id,
                    })]
            
            move_vals = {
                'ref': self.payment_request_id.name,
                'line_ids': move_line,
                'journal_id': journal_id,
                'date': self.date,
                'narration': 'Payment Request ' + self.payment_request_id.name,
                }

        elif self.payment_request_id.type=='settle':
            account_id = (
                    self.env["res.company"]
                        ._company_default_get("account.account")
                        .account_cash_advance
                        )
            if not account_id:
                action = self.env.ref("account.action_account_config")
                msg = "Cannot find a cash advance accounts for this company, You should configure it. \nPlease go to Account Configuration."
                raise RedirectWarning(msg, action.id, "Go to the configuration panel")
            journal_id = self.journal_id.id
            tax_group = self.payment_request_id.get_taxes_values()
            for line in self.payment_request_id.payment_line:
                move_line += [(0, 0, self._prepare_move_line_st(line,self.payment_request_id.analytic_account_id.id,line.amount,0,self.line_ids[0].id))]
                if len(tax_group)==0:
                    for tax_line in line.payment_line_tax_ids:
                        tax_account = self.env['account.tax'].search([('id','=',tax_line.id)])
                        if tax_account.price_include:
                            debit = line.amount * (tax_account.amount/100)
                            credit = 0
                        elif tax_account.amount<0:
                            debit = 0
                            credit = -line.amount * (tax_account.amount/100)
                        else:
                            debit = line.amount * (tax_account.amount/100)
                            credit = 0
                        move_line += [(0, 0, self._prepare_move_line_st(line,self.payment_request_id.analytic_account_id.id,debit,credit))]
                        

            if len(tax_group)>0:
                for line_tax in tax_group:
                    tax_name = tax_group.get(line_tax,{}).get('name',False)
                    tax_id = tax_group.get(line_tax,{}).get('tax_id',False)
                    tax_account = self.env['account.tax'].search([('id','=',int(tax_id))])
                    tax_amount = tax_group.get(line_tax,{}).get('amount',False)

                    if tax_account.price_include:
                        debit = tax_amount
                        credit = 0
                    elif tax_amount<0:
                        debit = 0
                        credit = -tax_amount
                    else:
                        debit = tax_amount
                        credit = 0
                        tax_incl += tax_amount

                    move_line += [(0, 0, {
                            'date_maturity': self.date, 
                            'partner_id': self.payment_request_id.partner_id.id or False,
                            'name': tax_name, 
                            'debit': debit, 
                            'credit': credit, 
                            'debit_cash_basis': debit, 
                            'credit_cash_basis': credit, 
                            'balance_cash_basis': debit - credit, 
                            'account_id': tax_account.account_id.id, 
                            'quantity': 1, 
                            'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                            'statement_id': self.id,
                            })]
            amount_sett = sum(self.payment_request_id.payment_line.mapped('amount'))
            if self.payment_request_id.balance<0:                    
                account_id = (
                    self.env["res.company"]
                        ._company_default_get("account.account")
                        .account_cash_advance
                        )
                if not account_id:
                    action = self.env.ref("account.action_account_config")
                    msg = "Cannot find a cash advance accounts for this company, You should configure it. \nPlease go to Account Configuration."
                    raise RedirectWarning(msg, action.id, "Go to the configuration panel")

                name = 'Kekurangan Cash Advance '
                self.env['payment.request.line'].create({'payment_id': self.payment_request_id.id,
                                                     'name': name + self.payment_request_id.name,
                                                     'qty': 1,
                                                     'amount': self.payment_request_id.balance,
                                                     'bill': self.payment_request_id.balance,
                                                     'unit_price': self.payment_request_id.balance,
                                                     'account_id': self.payment_request_id.journal_id.default_credit_account_id.id,
                                                     'state': 'paid',
                                                    })
                move_line += [(0, 0, {
                            'date_maturity': self.date, 
                            'partner_id': self.payment_request_id.partner_id.id or False,
                            'name': name, 
                            'debit': 0, 
                            'credit': -self.payment_request_id.balance, 
                            'debit_cash_basis': 0, 
                            'credit_cash_basis': - self.payment_request_id.balance, 
                            'balance_cash_basis': - self.payment_request_id.balance, 
                            'account_id': self.payment_request_id.journal_id.default_credit_account_id.id, 
                            'quantity': 1, 
                            'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                            'statement_id': self.id,
                            })]

            credit_amount = sum(self.line_ids.mapped('amount')) #self.payment_request_id.advance_amount #
            if abs(credit_amount) > amount_sett:
                diff = abs(credit_amount) - amount_sett
                if not self.excess_account:
                    raise UserError("Amount of bank statement it's greater than Amount of settlement.\nPlease fill field 'Excess Account' to deal with it or set amount of bank statement equal to amount of settlement.")
                else:
                    move_line += [(0, 0, {
                        'date_maturity': self.date, 
                        'partner_id': self.payment_request_id.partner_id.id or False,
                        'name':   'Excess',
                        'debit': diff, 
                        'credit': 0, 
                        'debit_cash_basis': diff, 
                        'credit_cash_basis': 0, 
                        'account_id': self.excess_account.id, 
                        'balance_cash_basis': diff, 
                        'quantity': 1, 
                        'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                        'statement_id': self.id,
                        })]
            elif abs(credit_amount) < amount_sett:
                raise UserError("Amount of bank statement can't be less than Amount of settlement.")
            
            move_line += [(0, 0, {
                    'date_maturity': self.date, 
                    'partner_id': self.payment_request_id.partner_id.id or False,
                    'name':   ' '.join([str(elem) for elem in self.line_ids.mapped('name')]), #self.account_id.name,
                    'debit': 0, 
                    'credit': abs(credit_amount), 
                    'debit_cash_basis': 0, 
                    'credit_cash_basis': abs(credit_amount), 
                    'account_id': account_id.id, 
                    'balance_cash_basis': credit_amount, 
                    'quantity': 1, 
                    'analytic_account_id': self.payment_request_id.analytic_account_id.id,
                    'statement_id': self.id,
                    })]
            
            move_vals = {
                'ref': self.payment_request_id.name,
                'line_ids': move_line,
                'journal_id': journal_id,
                'date': self.date,
                'narration': 'Payment Request ' + self.payment_request_id.name,
                }
            self.payment_request_id.update({'balance': 0,'state': 'close'})
            self.payment_request_id.pembayaran_id.update({'state': 'close',})

        move = self.env['account.move'].create(move_vals)
        move.post()

        if self.payment_request_id.is_bill:
            inv = False
            for line in self.payment_request_id.payment_line:
                inv = line.invoice_id
            payment_line = self.env['account.move.line'].search([('move_id','=',move.id),('account_id','!=',self.account_id.id)])
            line_to_reconcile = self.env['account.move.line']
            line_to_reconcile += inv.move_id.line_ids.filtered(lambda r: r.account_id.id==self.line_ids[0].account_id.id)
            (line_to_reconcile + payment_line).reconcile(False, False)

        statements.write({'state': 'confirm', 'date_done': time.strftime("%Y-%m-%d %H:%M:%S")})
    

    def _prepare_move_line(self,line,analytic_account_id,amount=False):
        if amount:
            bill = 0
            amount = amount + line.debit - line.credit
            qty = line.quantity
            account_id = line.account_id.id
        else:
            bill = line.bill
            amount = 0
            qty = line.qty
            account_id = line.product_id.property_account_expense_id.id
        return {
            'date_maturity': self.date, 
            'partner_id': self.payment_request_id.partner_id.id or False,
            'name': line.name, 
            'debit': bill, 
            'credit': amount, 
            'debit_cash_basis': bill, 
            'credit_cash_basis': amount, 
            'balance_cash_basis': bill - amount, 
            'account_id': account_id, 
            'quantity': qty, 
            'product_id': line.product_id.id, 
            'analytic_account_id': analytic_account_id,
            }

    def _prepare_move_line_st(self,line,analytic_account_id,debit=0.0,credit=0.0,st_line=False):
        return {
            'date_maturity': self.date, 
            'partner_id': self.payment_request_id.partner_id.id or False,
            'name': line.name, 
            'debit': debit, 
            'credit': credit, 
            'debit_cash_basis': debit, 
            'credit_cash_basis': credit, 
            'balance_cash_basis': debit-credit, 
            'account_id': line.account_id.id, 
            'quantity': 1, 
#            'product_id': line.product_id.id, 
            'analytic_account_id': analytic_account_id,
            'statement_id': self.id,
            'statement_line_id': st_line,
            }


class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    invoice_id = fields.Many2one('account.invoice', string='Invoice', domain=['&', ('type', '=', 'in_invoice'), ('state', '=', 'open')])
    payment_request_line = fields.Many2one('payment.request.line',string='Payment Line',store=True)

    @api.onchange('invoice_id')
    def _onchange_invoice_id(self):
        res = []
        if self.invoice_id:
            self.amount = self.invoice_id.amount_total
            self.name = self.invoice_id.number

    pembayaran_line_id = fields.Many2one("payment.request.line", "Payment Request")

    @api.onchange("pembayaran_line_id")
    def onchange_pembayaran_line_id(self):
        n = 1
        payment_line_id = self.pembayaran_line_id
        if payment_line_id:
            n = -1
            if payment_line_id.type == "settle":
                n = 1

            account_id = (
                self.env["res.company"]
                ._company_default_get("account.account")
                .account_cash_advance.id
            )

            return {
                "value": {
                    "date": payment_line_id.payment_id.date,
                    "name": payment_line_id.name,
                    "ref": payment_line_id.invoice_id.number or False,
                    "amount": payment_line_id.amount * n,
                    "partner_id": payment_line_id.invoice_id.partner_id.id
                    or payment_line_id.payment_id.partner_id.id
                    or False,
                    "analytic_account_id": payment_line_id.payment_id.analytic_account_id.id,
                    "account_id": account_id
                    if payment_line_id.type == "aap"
                    else False,
                }
            }

    def fast_counterpart_creation(self):
        res = super(AccountBankStatementLine, self).fast_counterpart_creation()
        for x in self:
            if x.pembayaran_line_id:
                x.pembayaran_line_id.state = "paid"
        return res

    def process_reconciliation(
        self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None
    ):
        res = super(AccountBankStatementLine, self).process_reconciliation(
            counterpart_aml_dicts=counterpart_aml_dicts,
            payment_aml_rec=payment_aml_rec,
            new_aml_dicts=new_aml_dicts,
        )
        for x in self:
            if x.pembayaran_line_id:
                x.pembayaran_line_id.state = "paid"
        return res


class CategoryPayment(models.Model):
    _name = "category.payment"
    _description = "Category Payment"

    name = fields.Char(string="Name")

class HrOvertime(models.Model):
    _inherit = 'hr.overtime'

    payment_id = fields.Many2one('payment.request', string='Payment Request', related='payment_request_line.payment_id')
    payment_request_line = fields.One2many('payment.request.line', 'overtime_id', string='Payment Request')

    @api.multi
    def name_get(self):
        res = []
        for field in self:
            res.append((field.id, '%s - %s' % (field.name, field.project_id or "None")))
        return res 