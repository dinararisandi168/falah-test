# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError
from odoo.addons.ab_role.models.models import _get_uid_from_group


# import logging
# _logger = logging.getLogger(__name__)
import time


class HrCicilan(models.Model):
    _name = "hr.cicilan"
    _inherit = "mail.thread"
    _description = "Cicilan"

    name = fields.Char(string="Reference", required=True, readonly=True, default="/")
    date = fields.Date(string="Date", readonly=True, track_visibility="onchange", default=fields.Date.today())
    catatan_revisi = fields.Text(string="Catatan Cancel Pinjaman", track_visibility="onchange")
    user_id = fields.Many2one("res.users", "Created By", required=True, readonly=True, select=True, default=lambda self: self.env.user,)
    employee_id = fields.Many2one("hr.employee", "Employee", required=True, readonly=True, select=True, states={"draft": [("readonly", False)]}, track_visibility="onchange")
    value = fields.Float(string="Amount", required=True, readonly=True, digits=0, states={"draft": [("readonly", False)]}, oldname="purchase_value")
    angsuran = fields.Integer("Angsuran (Bulan)", required=True, readonly=True, states={"draft": [("readonly", False)]},)
    pinjaman = fields.Char(string="Pinjaman", compute="_pinjaman_per")
    keperluan = fields.Char(string="Keperluan", states={"draft": [("readonly", False)]})
    komitmen = fields.Char(string="Komitmen", states={"draft": [("readonly", False)]})
    product_pinjaman = fields.Many2one('product.product', String="Product", readonly=True, store=True, default=lambda self: self.env['product.product'].search([('name', 'ilike', 'Pinjaman Karyawan')], limit=1))    

    def action_create_payment(self):
#        Create Payment Request
        line_ids = []
        line_ids += [(0,0, {
                            "product_id": self.product_pinjaman.id,
                            "name": 'Pinjaman Karyawan ' + self.name,
                            "qty": 1,
                            "unit_price": self.value,
                            "state": 'open',
                            "amount": self.value,
                                      })]
        
        payment = self.env['payment.request'].search([('pinjaman_id','=',self.id)],limit=1)
        if payment:
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'target': 'current',
                'res_id': payment.id,
                }
        else:
            return {
                'name': ('Create Payment Request'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'payment.request',
                'view_id': self.env.ref("aa_payment_request.submit_payment_request_form_view").id,
                'type': 'ir.actions.act_window',
                'context': {'default_name': self.name,
                            'default_payment_line': line_ids,
                            'default_date': datetime.now(),
                            'default_employee_id': self.employee_id.id or False,
                            'default_partner_id': self.employee_id.address_id.id,
                            'default_payment_type': 'hrd',
                            'default_type': 'payment',
                            'default_description': 'Pinjaman Karyawan ' + self.name,
                            'default_reference': self.name,
                            'default_pinjaman_id': self.id,
                            },
                'target':'current'
                }

    @api.onchange('value')
    def _onchange_value(self):
        if self.value and self.employee_id:
            gaji = self.env['hr.contract'].search(
                [('employee_id', '=', self.employee_id.id), ('state', '=', 'open')], limit=1, order="id desc").wage
            if self.value >= gaji:
                self.update({'value': gaji})
                return{
                    'warning':
                    {
                        'title': 'Something Wrong',
                        'message': 'Pinjaman tidak boleh lebih besar dari gaji'
                    }
                }

    def _pinjaman_per(self):
        if self.cicilan_line and self.angsuran:
            nilai = 0
            for x in self.cicilan_line:
                if x.payslip_id:
                    nilai += 1
            hasil = "%s/%s" % (nilai, self.angsuran)
            self.pinjaman = hasil

    value_residual = fields.Float(
        compute="_amount_residual",
        store=True,
        method=True,
        digits=0,
        string="Remaining Installment",
    )
    lunas = fields.Boolean("Paid Off", readonly=True,
                           track_visibility="onchange")
    cicilan_line = fields.One2many(
        "hr.cicilan.line", "cicilan_id", string="Installment Lines", readonly=True
    )
    tipe = fields.Many2one(
        "jenis.cicilan",
        "Type",
        readonly=True,
        states={"draft": [("readonly", False)]},
    )
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("verify", "Verified"),
            ("done", "Done"),
            ("cancel", "Cancel"),
        ],
        "State",
        readonly=True,
        select=True,
        track_visibility="onchange",
        default="draft",
    )

    # @api.onchange('value')
    # def _onchange_value(self):
    #     if self.value:
    #         gaji = self.env['hr.contract'].search(
    #             [('employee_id', '=', self.employee_id, 'state', '=', 'open')]).wage
    #         if self.value >= gaji:
    #             return {
    #                 self.value = gaji
    #                 'warning': {
    #                     'title': 'Something Wrong',
    #                     'message': 'Pinjaman tidak boleh lebih besar dari gaji'
    #                 }
    #             }

    # @api.onchange('value')
    # def _onchange_value(self):
    #     if self.value:
    #         gaji = self.env['hr.contract'].search(
    #             [('employee_id', '=', self.employee_id.id, 'state', '=', 'open')], limit=1, order="id desc")
    #         if self.value >= gaji.wage:
    #             return{
    #                 'warning':
    #                 {
    #                     'title': 'Something Wrong',
    #                     'message': 'Pinjaman tidak boleh lebih besar dari gaji'
    #                 }
    #             }

    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].get("hr.cicilan")
        return super(HrCicilan, self).create(vals)

    def hitung_cicilan(self):
        angsuran = self.angsuran
        start = time.time()
        if not self.cicilan_line:
            cicilan = 0
            cline = self.env["hr.cicilan.line"]
            if self.value and angsuran:
                cicilan = self.value / angsuran
            no = 1
            terbayar = cicilan
            sisa = self.value - cicilan

            if self.cicilan_line:
                for i in self.cicilan_line:
                    i.unlink()

            # query = "select input_cicilan_line(%s,%s,%s,%s,%s,%s)" % (angsuran,int(terbayar),int(cicilan),int(sisa),self.id,self.env.uid)
            # self.env.cr.execute(query)
            for line in range(angsuran):
                cline.create(
                    {
                        "angsuran_ke": no,
                        "cicilan_terbayar": terbayar,
                        "cicilan_value": cicilan,
                        "sisa_cicilan": sisa,
                        "cicilan_id": self.id,
                    }
                )
                no += 1
                terbayar += cicilan
                sisa -= cicilan
            # 0.00680590073268 orm
            # 0.0017166852951 query
            # end = time.time()
            # duration=(end-start)/60
            # _logger.info("Total time: %s minutes" , duration)
            return {self.id: self.id}
        else:
            return True

    def _get_users(self, group_user):
        res = []
        temp_group = []

        for group in group_user:
            users = self.env['ir.model.data'].xmlid_to_object(group).users
            if users:
                for i in users:
                    temp_group.append((4, i.partner_id.id))
        for i in temp_group:
            if not i in res:
                res.append(i)
        return res

    def cicilan_draft(self):
        self.write({"state": "draft"})
        return True

    def cicilan_verify(self):
        partner_ids = self._get_users(['ab_role.group_direktur'])
        # partner_ids = [o.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        for i in self:
            i.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                subject="Form Pinjaman - %s - [Need Verify or Approve]" % (i.name),
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.env['send.email.notification'].send(self.id, 'hr.cicilan', 'verify', 'group', 'ab_role.group_direktur')
            i.write({"state": "verify"})
        return True

    def cicilan_approve(self):
        self.write({"state": "done"})
        return True

    def cicilan_cancel(self):
        if self.cicilan_line:
            for i in self.cicilan_line:
                i.unlink()
        self.write({"state": "cancel", "lunas": False})
        for o in self:
            if o.employee_id.user_id:
                if o.employee_id.user_id.partner_id:
                    partner = o.employee_id.user_id.partner_id.id
                    if partner:
                        o.message_post(
                            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that inform your request has been canceled. \nThanks",
                            subject="Form Pinjaman - %s - [Has Been Cancelled]" % (o.name),
                            partner_ids=[(6, 0, [partner])],
                            message_type="notification",
                            subtype="mail.mt_comment",
                        )
            o.write({"state": "cancel"})

    def cicilan_confirm(self):
        partner_ids = self._get_users(['ab_role.group_hod_fat'])
        cicilan = self.env["hr.cicilan"]
        filter_cicilan = cicilan.search(
            [
                ("employee_id", "=", self.employee_id.id),
                ("state", "=", "done"),
                ("tipe.name", "=", self.tipe.name),
                ("lunas", "=", False),
            ]
        )
        if self.tipe.max_angsur > 0:
            if self.angsuran > self.tipe.max_angsur:
                raise UserError(_("Maksimal %s bulan !") % (self.tipe.max_angsur))
            elif self.angsuran > 6:
                raise UserError(_("Maksimal 6 bulan !"))

        if self.tipe.max_pinjaman > 0:
            if self.value > self.tipe.max_pinjaman:
                raise UserError(
                    _("Maksimal cicilan hanya Rp. %s !") % (
                        self.tipe.max_pinjaman)
                )

        if filter_cicilan:
            raise UserError(
                _("masih memiliki cicilan aktif (%s) !") % (filter_cicilan.name)
            )
        else:
            for i in self:
                i.write({"state": "submit"})
                i.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                    subject="Form Pinjaman - %s - [Need Verify or Approve]" % (i.name),
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
        self.hitung_cicilan()
        return True

    @api.multi
    def unlink(self):
        for payment in self:
            if payment.state not in "draft":
                raise UserError(
                    _("You cannot delete a document is in %s state.") % (
                        payment.state,)
                )
        return super(HrCicilan, self).unlink()


class CicilanLine(models.Model):
    _name = "hr.cicilan.line"
    _description = "Cicilan Line"

    cicilan_id = fields.Many2one("hr.cicilan", "Cicilan ID")
    cicilan_terbayar = fields.Float("Paid")
    # angsuran_ke = fields.Integer('Installment')
    cicilan_value = fields.Float("Amount")
    sisa_cicilan = fields.Float("Remaining")
    payslip_id = fields.Many2one('hr.payslip', 'Payslip', index=True)

    @api.multi
    def name_get(self):
        res = []
        # for field in self:
        #     res.append((field.id, '(%s) %s' % (field.payment_id.name, field.name)))
        return res


class JenisCicilan(models.Model):
    _name = "jenis.cicilan"
    _description = "Jenis Cicilan"

    name = fields.Char("Type")
    max_angsur = fields.Integer("Maksimal Angsuran")
    max_pinjaman = fields.Integer("Maksimal Pinjaman")


class Employee(models.Model):
    _inherit = "hr.employee"

    cicilan_line = fields.One2many(
        "hr.cicilan",
        "employee_id",
        "Cicilan Line",
        readonly=True,
        domain=[("state", "=", "done")],
    )
    pinjaman_count = fields.Integer(
        compute="_compute_pinjaman_count", string="Pinjaman Computation Details"
    )

    @api.multi
    def _compute_pinjaman_count(self):
        for pinjaman in self:
            pinjaman.pinjaman_count = len(pinjaman.cicilan_line)


class Wizard(models.TransientModel):
    _name = 'catatan.wizard.pinjaman'
    _description = 'Wizard Revisi Pinjaman'

    text_revisi = fields.Text(string='Catatan Revisi')

    @ api.multi
    def isi_catatan(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            cicilan = self.env['hr.cicilan'].browse(active_id)
            cicilan.catatan_revisi = self.text_revisi
            cicilan.cicilan_line = ""
            cicilan.cicilan_draft()
