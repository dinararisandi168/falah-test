# -*- coding: utf-8 -*-
{
    "name": "CUSTOM HR PINJAMAN",
    "summary": """
        converted to odoo 12""",
    "description": """
        Pinjaman untuk karyawan
    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Custom Modules",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr", "hr_payroll"],
    # always loaded
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/templates.xml",
        "wizard/report_pinjaman.xml",
        "report/report_pinjaman.xml",
        # "views/action_report.xml",
        # "report/report_slip_gaji.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
    "installable": True,
    "auto_install": False,
    "application": True,
}
