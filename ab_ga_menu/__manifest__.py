{
    'name': "General Affair Menu",
    'summary': """
       General Affair Menu""",

    'description': """
        General Affair Menu
    """,

    'author': "PT. ISMATA NUSANTARA ABADI",
    'website': "http://www.ismata.co.id",

    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'stock', 'calendar','ab_role', 'ab_surat_keluarmasuk', 'ab_disposisi', 'ab_stock_mutasi','purchase','account','project'],
    'data': ['security/ir.model.access.csv', 'views/views.xml'],
    'installable': True
}
