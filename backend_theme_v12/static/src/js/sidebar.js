/* Copyright 2016, 2019 Openworx.
 * License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl). */

// Check if debug mode is active and then add debug into URL when clicking on the App sidebar
odoo.define('backend_theme_v12.Sidebar', function(require) {
    "use strict";
    var core = require('web.core');
    var session = require('web.session');
	var Widget = require('web.Widget');
	var UserMenu = require('web.UserMenu');
	var WebClient = require('web.WebClient');
	var rpc = require('web.rpc');



    $(function() {
	(function($) {
	    $.addDebug = function(url) {
		url = url.replace(/(.{4})/, "$1?debug");
		return url;
	    }
	    $.addDebugWithAssets = function(url) {
		url = url.replace(/(.{4})/, "$1?debug=assets");
		return url;
	    }
	    $.delDebug = function(url) {
		var str = url.match(/web(\S*)#/);
		url = url.replace("str/g", "");
		return url;
	    }
	}) (jQuery);
        $("#sidebar a").each(function() {
            var url = $(this).attr('href');
            if (session.debug == 1) $(this).attr('href', $.addDebug(url));
            if (session.debug == 'assets') $(this).attr('href', $.addDebugWithAssets(url));
            if (session.debug == false) $(this).attr('href', $.delDebug(url));
        });
	});
	


	UserMenu.include({
		events: {
			'click a.oneclick_logout': 'onMenuLogout', 
		},
		onMenuLogout: function() {
			var self = this;
			this.trigger_up('clear_uncommitted_changes', {
				callback: this.do_action.bind(this, 'logout'),
			});
		}
	});

	var LeftUserMenu =  UserMenu.extend({
		template: "UserLeft",
		events: {
			'click .oe_topbar_avatar': 'click_on_avatar'
		},
		init: function(parent) {
			this._super(parent);
			this.update_promise = $.Deferred().resolve();
		},
		start: function() {
			var self = this;
			this.$el.on('click', 'li a[data-menu]', function(ev) {
				ev.preventDefault();
				var f = self['on_menu_' + $(this).data('menu')];
				if (f) {
					f($(this));
				}
			});
			this.$el.parent().show();
			// return this._super.apply(this, arguments);
		},
		do_update: function () {
			var $avatar = this.$('.oe_topbar_avatar');
			if (!session.uid) {
				$avatar.attr('src', $avatar.data('default-src'));
				return $.when();
			}
			var topbar_name = session.name;
			if(session.debug) {
				topbar_name = _.str.sprintf("%s (%s)", topbar_name, session.db);
			}
			this.$('.oe_topbar_name').text(topbar_name);
			var avatar_src = session.url('/web/image', {model:'res.users', field: 'image_small', id: session.uid});
			$avatar.attr('src', avatar_src);
			// this.update_promise = this.update_promise.then(fct, fct);
		},
		click_on_avatar: function(){
			var is_mobile = $(window).width() < 768;
			if(is_mobile){
				this.on_menu_settings();
			}else {
				this.$('#user_name').click();
			}
		},
		on_menu_settings: function() {
			var self = this;
			this.getParent().clear_uncommitted_changes().then(function() {


				rpc.query({
					route:"/web/action/load",
					params: { action_id: "base.action_res_users_my" }
				}).then(function(result){
					result.res_id = session.uid;
					self.getParent().action_manager.do_action(result);
				});

				
				// self.rpc("/web/action/load", { action_id: "base.action_res_users_my" }).done(function(result) {
				// 	result.res_id = session.uid;
				// 	self.getParent().action_manager.do_action(result);
				// });
			});
		},
		// on_menu_logout: function() {
		//     this.trigger('user_logout');
		// },
		// on_menu_documentation: function () {
		//     window.open('https://www.odoo.com/documentation/user', '_blank');
		// },
		
	});


	WebClient.include({
		
		show_application: function() {
			var self = this;
			var res = this._super.apply(this, arguments);
	
			// Create the user Left
			self.user_left = new LeftUserMenu(self);

		
			var user_left_loaded = self.user_left.appendTo(this.$el.find('div.user'));
			self.user_left.do_update();

		},
	});



});
