# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'COA - Falah',
    'version': '1.1',
    'category': 'Localization',
    'description': """
This is the base module to manage the ismata accounting chart in Odoo.
==============================================================================

Install some generic chart of accounts.
    """,
    'depends': [
        'account',
        'account_parent'
    ],
    'data': [
        'data/account_data.xml',
    ],
    'demo': [
    ],
    'images': [
        'static/description/banner.png'
    ],
    'uninstall_hook': 'uninstall_hook',
}
