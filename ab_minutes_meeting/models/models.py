# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from lxml import etree
from odoo.addons.ab_role.models.models import _get_uid_from_group

class MeetingSummary(models.Model):
    _name = "meeting.summary"
    _description = "Apa saja yang dibahas dalam meeting"

    name = fields.Char(string="Discussion Item")
    action_status = fields.Selection(string="Action Status", selection=[("open", "Open"),("close", "Close")],default="open")
    status = fields.Selection(string="Status",selection=[("nta", "Need to Action"),("info", "Info")])
    pic_id = fields.Many2one("res.partner", string="PIC")
    user_id = fields.Many2one("res.users", string="Host", default="")
    # can_edit = fields.Boolean(string='can edit')
    event_id = fields.Many2one("calendar.event", string="MOM")
    is_edit = fields.Boolean(string='Can Edit',compute='_compute_is_action', default=True)
    is_action = fields.Boolean(compute='_compute_is_action', default=True)
    
    def _compute_is_action(self):
        for o in self:
            if o.event_id:
                if o.event_id.user_id == o.env.user or o.event_id.responsibility_id == o.env.user or o.pic_id == o.env.user.partner_id:
                    o.is_action = True
                elif o.event_id.is_edit:
                    o.is_action = True
                else:
                    o.is_action = False
            else:
                o.is_action = False
    
    @api.multi
    def unlink(self):
        for o in self:
            if o.event_id:
                if not o.event_id.is_edit:
                    raise UserError(_('You cannot Meeting Summary Delete this record'))

        return super(MeetingSummary, self).unlink()

    @api.model
    def create(self, vals):
        event_id = self.env['calendar.event'].browse(vals['event_id'])
        if not event_id.is_edit:
            raise UserError(_('You cannot Create Meeting Summary'))
        return super(MeetingSummary, self).create(vals)

    # no = fields.Integer(string="No")
    # compute_no = fields.Integer("invisible field", compute="_get_no")

    # @api.depends("compute_no")
    # def _get_no(self):
    #     meeting = self.env["meeting.summary"]
    #     if meeting:
    #         count = 1
    #         for rec in self:
    #             meeting_id = self.env["meeting.summary"].search([("id", "=", rec.id)])
    #             meeting_id.update({"no": count})
    #             count += 1


class CalendarEvent(models.Model):
    _inherit = "calendar.event"
    _description = "Minutes of Meetings"

    
    user_id = fields.Many2one("res.users", string="Host", default="")
    participant_type = fields.Selection([("ex", "External"), ("in", "Internal")], string="Participant Type")
    prepared_id = fields.Many2one("hr.employee", string="Prepared By", track_visibility="onchange")
    accepted_id = fields.Many2one("res.partner", string="Accepted By", track_visibility="onchange")
    project_id = fields.Many2one("project.project", string="Project Refference", track_visibility="onchange")
    summary_line = fields.One2many("meeting.summary", "event_id", string="Meeting Summary")
    attacment_line = fields.One2many("ir.attachment", "event_id", string="Attachment List")
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')
    follow_up = fields.Char(string='Follow Up Status Open', readonly=True, store=True, compute="_get_string",  track_visibility="onchange")
    manage_type = fields.Selection(string="Meeting Type", selection=[("manage", "Management"), ("project", "Project"), ("rutin", "Routine")], track_visibility="onchange")
    prepared_sign = fields.Binary(string="Prepared Signature", track_visibility="onchange", related="prepared_id.user_id.digital_signature")
    accepted_sign = fields.Binary(string="Accepted Signature", track_visibility="onchange", related="accepted_id.user_id.digital_signature")
    state = fields.Selection([('draft', 'Draft'), ('open', 'Confirmed'), ('done', 'Done')], default='draft', track_visibility='onchange')
    responsibility_id = fields.Many2one('res.users', string='Responsibility',default=lambda self:self.env.user, track_visibility="onchange")
    is_edit = fields.Boolean(string='Can Edit',compute='_compute_is_edit', default=True)
    catatan_revisi = fields.Text(string='Revision Notes', track_visibility="onchange")
    company_id = fields.Many2one('res.company', string='Company', readonly=True, states={'draft': [('readonly', False)]}, default=lambda self: self.env.user.company_id)# @api.model 
    # def create(self, vals):
    #     res = super(CalendarEvent, self).create(vals)
    #     res.method_tombol()
    #     # linemom=[]
    #     data_tambah = []
    #     print("KEPANGGIL NIH DEF CREATE NYA",self)
    #     print("KEPANGGIL NIH DEF CREATE NYA",vals)
    #     for s in vals:
    #         if s.project_id:
    #             print("ADA NIH PROJECTNYA")
    #             data_tambah.append({
    #                 'calendar_id': s.id, 
    #                 })
    #             project_id = s.env['project.project'].browse(vals['project_id'])
    #             for o in project_id:
    #                 o.write(0,0,{'mom_line' : data_tambah})
        # return super(CalendarEvent, self).create(vals)
    
    def send_email(self):
        for partner in self.partner_ids:
                self.env['send.email.notification'].send_mom(self.id, 'calendar.event', 'submit', 'user', partner.user_ids, self.name)

    @api.multi
    def write(self, vals):
        for i in self:
            # if not self.user_has_groups("base.group_system"): Jika administrator tetap bisa ubah bisa pakai ini.
            if i.user_id.id != i.env.user.id or i.responsibility_id.id != i.env.user.id:
                if vals.get("start_date") or vals.get("stop_date") or vals.get("start"):
                    raise ValidationError("You are not allowed to change!")
        return super(CalendarEvent, self).write(vals)

    @api.multi
    def unlink(self):
        for o in self:
            if o.create_uid.id != self.env.user.id:
                raise UserError(_('Only creator of this record can delete!'))

        return super(CalendarEvent, self).unlink()

    @api.model
    def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
        result = super(CalendarEvent, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(result["arch"])
        if self.env.context.get("open_mom", []) and (view_type == "tree" or view_type == "form" or view_type == "calendar"):
            if self.user_has_groups("ab_role.software_development") or self.user_has_groups("ab_role.content_development") or self.user_has_groups("ab_role.hardware_network") or self.user_has_groups("ab_role.group_project_admin") or self.user_has_groups("ab_role.group_project_leader") or self.user_has_groups("project.group_project_manager") or self.user_has_groups("ab_role.group_bussines_demand") or self.user_has_groups("ab_role.group_hod_human_resource") or self.user_has_groups("ab_role.group_hod_general_affair") or self.user_has_groups("purchase.group_purchase_manager") or self.user_has_groups("account.group_account_invoice") or self.user_has_groups("ab_role.group_tax") or self.user_has_groups("account.group_account_user") or self.user_has_groups("ab_role.group_hod_fat") or self.user_has_groups("ab_role.group_direktur"):
                for node_form in doc.xpath("//form"):
                    node_form.set("edit", "true")
                    node_form.set("create", "true")
                for tree in doc.xpath("//tree"):
                    tree.set("edit", "true")
                    tree.set("create", "true")
                # for node_cal in doc.xpath("//calendar"):
                #     node_cal("create", "true")
            else:
                # for node_cal in doc.xpath("//calendar"):
                #     node_cal("create", "false")
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("create", "false")
          
        result["arch"] = etree.tostring(doc, encoding="unicode")
        return result

    def _compute_is_edit(self):
        for o in self:
            if o.user_id == o.env.user or o.responsibility_id == o.env.user:
                o.is_edit = True
            # elif o.user_has_groups("base.group_erp_manager") or o.user_has_groups('base.group_system'):
            #     o.is_edit = True
            else:
                o.is_edit = False

    @api.multi
    def add_project(self):
        cek_related = self.env["add.mom"].search([("project_id", "=", self.project_id.id),("calendar_id", "=", self.id)])
        if cek_related:
            raise UserError('Already Added! You can not add related MOM Again!')
        else:
            view_id = self.env.ref('project.edit_project')
            self.env["add.mom"].create({'calendar_id' : self.id, 'project_id' : self.project_id.id})
            return {
                    "name": "Project Charter",
                    "view_type": "form",
                    "view_mode": "form",
                    "res_model": "project.project",
                    "view_id": view_id.id,
                    "type": "ir.actions.act_window",
                    "res_id" : self.project_id.id,
                }

    @api.multi
    def mom_done(self):
        if not self.prepared_sign or not self.accepted_sign:
            raise ValidationError(('Please sign the "prepared by" and "accepted by" first to continue this action'))
        
        for i in self.summary_line:
            if i.action_status == 'open':
                raise ValidationError(('Please make all "Action Status" to be Close in Meeting Summary to continue this action'))
            else:
                self.write({'state': 'done'})


    @api.depends("summary_line.action_status", "summary_line.pic_id")
    def _get_string(self):
        solo = ""
        hasil = ""
        akhir = ""
        for me in self:
            for line in me.summary_line:
                if line.action_status == "open" and line.pic_id:
                    solo = line.pic_id.name + ', '
                    hasil = hasil + solo
        akhir = hasil[:-2]
        me.update({"follow_up": akhir})


 # @api.model
    # def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
    #     result = super(CalendarEvent, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     active_id = self.env.context.get('active_id')
    #     print("ACTIVE ID",active_id)
    #     doc = etree.XML(result["arch"])
    #     me = self.env.context.get('uid')

    #     for i in self:
    #         print("PIC",self.pic_id)
    #         # responsible = i.responsibility_id
    #         for line in i.summary_line:
    #             if line.pic_id.user_id.id == me or i.responsibility_id.id == me:
    #                 line.can_edit = True
    #                 print("MASUK BERUBAH NIHHHH")

    #     print("=========== RES==========",self.id)
        
    #     result["arch"] = etree.tostring(doc, encoding="unicode")
    #     return result
class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    event_id = fields.Many2one("calendar.event")
    is_mom = fields.Boolean(string="mom")


class Participant(models.Model):
    _inherit = "calendar.attendee"
    _description = "Participants"

    phone = fields.Char(string="Mobile", readonly=True, related="partner_id.mobile")
    company_id = fields.Many2one("res.partner", string="Company", readonly=True, related="partner_id.parent_id")
    email = fields.Char('Email', help="Email of Invited Person", readonly=True)
    i_can_edit = fields.Boolean(string="Can Edit",compute="_compute_i_can_edit", readonly=False)


    def _compute_i_can_edit(self):
        for o in self:
            if o.event_id:
                if o.event_id.user_id == o.env.user or o.event_id.responsibility_id == o.env.user or o.partner_id == o.env.user.partner_id:
                    o.i_can_edit = True
                elif o.event_id.is_edit:
                    o.i_can_edit = True
                else:
                    o.i_can_edit = False
            else:
                o.i_can_edit = False
    
class WizardBast(models.TransientModel):
    _name = "wizard.mom"
    _description = "Wizard Revisi MOM"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def revisi_state(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            mom = self.env["calendar.event"].browse(active_id)
            if mom.write({"state": "draft"}):
                mom.catatan_revisi = self.text_revisi
            else:
                mom.catatan_revisi = self.text_revisi

