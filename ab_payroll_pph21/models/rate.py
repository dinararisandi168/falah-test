from odoo import api, fields, models


class RatePPh21(models.Model):
    _name = 'rate.pph21'
    _description = 'Rate PPh21'

    name = fields.Char(string='Name')
    position_cost = fields.Integer(string='Position Cost')
    maximum = fields.Float(string='Maximum')



class RatePph21Line(models.Model):
    _name = 'rate.pph21.line'
    _description = 'New Description'

    rate = fields.Float(string='Rate')
    minimum = fields.Float(string='Minimum')
    maximum = fields.Float(string='Maximum')