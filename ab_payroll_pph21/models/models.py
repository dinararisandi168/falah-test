# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Employee(models.Model):
    _inherit = 'hr.employee'

    ptkp_id = fields.Many2one('hr.ptkp', 'PTKP', compute='_compute_ptkp', store=True)
    jumlah_tanggungan = fields.Integer('Jumlah Tanggungan', default=0)
    punya_npwp = fields.Boolean('Punya NPWP')
    no_npwp = fields.Char('No. NPWP')

    @api.one
    @api.depends('gender', 'marital', 'jumlah_tanggungan')
    def _compute_ptkp(self):
        record_id = self.env.ref('ab_hr_payroll.tk0').id
        for x in self:
            if x.gender == 'female' and x.marital == 'married':
                record_id = x.env.ref('ab_hr_payroll.tk0').id
            if not x.marital:
                record_id = x.env.ref('ab_hr_payroll.tk0').id
            if x.marital == 'single' and x.jumlah_tanggungan == 0:
                record_id = x.env.ref('ab_hr_payroll.tk0').id
            if x.marital == 'single' and x.jumlah_tanggungan == 1:
                record_id = x.env.ref('ab_hr_payroll.tk1').id
            if x.marital == 'single' and x.jumlah_tanggungan == 2:
                record_id = x.env.ref('ab_hr_payroll.tk2').id
            if x.marital == 'single' and x.jumlah_tanggungan >= 3:
                record_id = x.env.ref('ab_hr_payroll.tk3').id
            if x.marital == 'married' and x.gender == 'male' and x.jumlah_tanggungan == 0:
                record_id = x.env.ref('ab_hr_payroll.k0').id
            if x.marital == 'married' and x.gender == 'male' and x.jumlah_tanggungan == 1:
                record_id = x.env.ref('ab_hr_payroll.k1').id
            if x.marital == 'married' and x.gender == 'male' and x.jumlah_tanggungan == 2:
                record_id = x.env.ref('ab_hr_payroll.k2').id
            if x.marital == 'married' and x.gender == 'male' and x.jumlah_tanggungan >= 3:
                record_id = x.env.ref('ab_hr_payroll.k3').id
            if x.marital == 'widower' and x.jumlah_tanggungan == 0:
                record_id = x.env.ref('ab_hr_payroll.tk0').id
            if x.marital == 'widower' and x.jumlah_tanggungan == 1:
                record_id = x.env.ref('ab_hr_payroll.tk1').id
            if x.marital == 'widower' and x.jumlah_tanggungan == 2:
                record_id = x.env.ref('ab_hr_payroll.tk2').id
            if x.marital == 'widower' and x.jumlah_tanggungan >= 3:
                record_id = x.env.ref('ab_hr_payroll.tk3').id
            x.ptkp_id = record_id


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    note_pph21 = fields.Text("Note PPH21", readonly=True)
    total_netto = fields.Float(string='Netto Bulan ini')
    netto_tahun = fields.Float(string='Netto 1 Tahun')
    ptkp = fields.Float(string='PTKP')
    pkp = fields.Float(string='PKP')
    npwp = fields.Float(string='PPh21 Terhutang NPWP')
    non_npwp = fields.Float(string='PPh21 Terhutang Non NPWP')
    pph_tahun = fields.Float(string='PPh21 Real Setahun')

    @api.multi
    def get_nilai_pajak(self, bulan_sisa, tahun, ptkp, employee, pajak_yg_sudah_dibayar, payslip_id, pendaptan_tanpa_bonus):
        pajak = 0
        data, pajak_setahun = self.env['hr.payslip'].get_nilai_pkp(tahun, ptkp, employee, payslip_id)
        data, tanpa_bonus = self.env['hr.payslip'].get_nilai_pkp(pendaptan_tanpa_bonus, ptkp, employee, payslip_id)
        pajak_sebenarnya = abs(pajak_setahun) - abs(pajak_yg_sudah_dibayar)
        payslip = self.browse(payslip_id)
        payslip.pph_tahun = pajak_sebenarnya
        pajak_teratur = tanpa_bonus/12
        pajak_tidak_teratur = pajak_setahun - tanpa_bonus
        pajak = pajak_teratur + pajak_tidak_teratur
        return pajak

    def get_data(self, a, b, c):
        sql = """
            select
            hpl.amount,hpl.code,hp.id
            from hr_payslip_line hpl
            left join hr_payslip hp on (hp.id = hpl.slip_id)
            where hpl.code in ('PPH21','BRT') and             
            date_from BETWEEN '%s-01-01' and '%s' and hp.employee_id = %s and hp.state = 'done'
        
        """ % (a, b, c)
        self.env.cr.execute(sql)
        data_sql = self.env.cr.dictfetchall()
        return data_sql

    @api.multi
    def get_nilai_pkp(self, tahun, PTKP_tarf_tahun, employee, payslip_id):
        lapis1 = 0
        lapis2 = 0
        lapis3 = 0
        lapis4 = 0
        payslip = self.browse(payslip_id)
        npwp = False
        if tahun >= PTKP_tarf_tahun:
            nilai_pkp = tahun - PTKP_tarf_tahun
            if employee.punya_npwp:
                npwp = True
                lapis1 = nilai_pkp * 0.05
                if nilai_pkp > 50000000:
                    lapis1 = 50000000 * 0.05
                    nilai_pkp -= 50000000
                    lapis2 = nilai_pkp * 0.15
                if nilai_pkp > 200000000:
                    lapis2 = 200000000 * 0.15
                    nilai_pkp -= 200000000
                    lapis3 = nilai_pkp * 0.25
                if nilai_pkp > 250000000:
                    lapis3 = 250000000 * 0.25
                    nilai_pkp -= 250000000
                    lapis4 = nilai_pkp * 0.3

            if not employee.punya_npwp:
                lapis1 = nilai_pkp * 0.06
                if nilai_pkp > 50000000:
                    lapis1 = 50000000 * 0.06
                    nilai_pkp -= 50000000
                    lapis2 = nilai_pkp * 0.18
                if nilai_pkp > 200000000:
                    lapis2 = 200000000 * 0.18
                    nilai_pkp -= 200000000
                    lapis3 = nilai_pkp * 0.3
                if nilai_pkp > 250000000:
                    lapis3 = 250000000 * 0.3
                    nilai_pkp -= 250000000
                    lapis4 = nilai_pkp * 0.36

        data = {'lapis1': lapis1, 'lapis2': lapis2, 'lapis3': lapis3, 'lapis4': lapis4}
        total = lapis1 + lapis2 + lapis3 + lapis4
        payslip.non_npwp = total if not npwp else 0
        payslip.npwp = total if npwp else 0
        return data, total


    def tunjangan_pph(self, pendapatan, allowance, tunjangan, pajak, date_from, date_to, employee, payslip_id, gaji):
        # print ('tunjangan pph berjalan')
        tunjangan = pajak

        pendapatan_temp = pendapatan + pajak
        pajak = self.compute_pph21(pendapatan_temp, allowance, date_from, date_to, employee, payslip_id, gaji)

        if not tunjangan == pajak:
            tunjangan = self.tunjangan_pph(pendapatan, allowance, tunjangan, pajak, date_from, date_to, employee, payslip_id, gaji)

        return tunjangan

    def compute_pph21(self, pendapatan_saat_ini, allowance, date_from, date_to, employee, payslip_id, gaji):
        PTKP_tarf_tahun = employee.ptkp_id.tahun
        total_bulan_sebelumnya = 0
        # Mencari Biaya jabatan
        biaya_jabatan = ((pendapatan_saat_ini * 12) + allowance) * 0.05
        if biaya_jabatan > 6000000:
            biaya_jabatan = 6000000

        # Mencari Nilai Seluruh Gaji Pada Periode Ini
        data_bruto = self.env['hr.payslip'].get_data(date_from, date_to, employee.id)
        # Mencari Nilai Pajak yang sudah dibayarkan jika sudah ada
        pajak_yg_sudah_dibayar = 0
        if data_bruto:
            pajak_yg_sudah_dibayar = sum([x['amount'] for x in data_bruto if x['code'] == 'PPH21'])
            total_bulan_sebelumnya = sum([x['amount'] for x in data_bruto if x['code'] == 'BRT'])

        bulan_ke = len(data_bruto or ['empty'])
        bulan_sisa = 13 - bulan_ke

        pensiun = gaji * ((1 / 100) + (2 / 100))
        asuransi = gaji * (0.24 + 0.3) / 100

        biaya_jabatan_tanpa_allowance = ((pendapatan_saat_ini * 12) * 0.05)
        if biaya_jabatan_tanpa_allowance > 6000000:
            biaya_jabatan_tanpa_allowance = 6000000

        # print (asuransi,'Asuransi yang dibayar')

        # print (asuransi+pendapatan_saat_ini+allowance,'Pendaptan Bruto')

        pendapatan_disetahunkan = ((pendapatan_saat_ini) * 12) + allowance
        pendapatan = pendapatan_disetahunkan - (biaya_jabatan) - (pensiun*12)
        pendaptan_tanpa_bonus = ((pendapatan_saat_ini) * 12) - (biaya_jabatan_tanpa_allowance) - (pensiun*12)
        result = self.get_nilai_pajak(bulan_sisa, pendapatan, PTKP_tarf_tahun, employee, pajak_yg_sudah_dibayar, payslip_id, pendaptan_tanpa_bonus)
        return result

# 57000000.0 Penghasilan Netto

    @api.multi
    def compute_akumulasi_pph21(self, employee_id, total_pph21):
        pph21_id = self.env['ir.model.data'].xmlid_to_object('ab_payroll_pph21.PPH21').id,
        january = '%s-01-01' % (fields.Date.today().year)
        payslip_line_ids = self.env['hr.payslip.line'].search([
            ('slip_id.date_from', '>=', january),
            ('slip_id.date_from', '<=', fields.Date.today()),
            ('slip_id.employee_id', '=', employee_id.id),
            ('slip_id.state', '=', 'done'),
            ('salary_rule_id', 'in', pph21_id)
        ])

        return sum(payslip_line_ids.mapped('total')) + total_pph21

    @api.multi
    def compute_pph_thr(self, employee_id, pajak_tahun, pajak_real):
        lapis1 = 0
        lapis2 = 0
        lapis3 = 0
        lapis4 = 0

        PTKP_tarf_tahun = employee_id.ptkp_id.tahun
        if pajak_tahun >= PTKP_tarf_tahun:
            nilai_pkp = pajak_tahun - PTKP_tarf_tahun
            lapis1 = nilai_pkp * 0.06
            if nilai_pkp > 50000000:
                lapis1 = 50000000 * 0.06
                nilai_pkp -= 50000000
                lapis2 = nilai_pkp * 0.18
            if nilai_pkp > 200000000:
                lapis2 = 200000000 * 0.18
                nilai_pkp -= 200000000
                lapis3 = nilai_pkp * 0.3
            if nilai_pkp > 250000000:
                lapis3 = 250000000 * 0.3
                nilai_pkp -= 250000000
                lapis4 = nilai_pkp * 0.36

        hasil = lapis1 + lapis2 + lapis3 + lapis4
        return hasil - pajak_real
