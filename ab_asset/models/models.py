# -*- coding: utf-8 -*-

from odoo import models, fields, api
import base64
from odoo.tools import email_re, email_split, email_escape_char, float_is_zero, float_compare, \
    pycompat, date_utils

class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    type = fields.Selection(selection_add=[('amortisation', 'Amortisation')])


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    qr_code = fields.Binary(string='QR Code', compute='_get_qr_code', store=True)
    spesification = fields.Text(string='Spesifikasi')
    harga_perolehan = fields.Monetary(string='Harga Perolehan')
    pic = fields.Many2one('hr.employee', "PIC Aset")
    location_asset_id = fields.Many2one('account.asset.location', 'Lokasi')
    code = fields.Char("Nomor Register", size=32,readonly=True)
    tipe_aset = fields.Selection([
        ("Tetap", "Aset Tetap"),
        ("Perlengkapan", "Perlengkapan"),
        ("Lainnya", "Aset Lainnya"),
    ], string="Tipe Aset")
    satuan = fields.Many2one("uom.uom", "Satuan")
    gerak = fields.Boolean("Aset Bergerak")
    kondisi = fields.Selection([("B", "Baik"), ("RR", "Rusak Ringan"), ("RB", "Rusak Berat")], string="Kondisi")
    aud_tanggal = fields.Date("Tanggal Audit Terakhir",raedonly=False)
    jenis_aset = fields.Selection(
        (
            ("tanah", "Tanah"),
            ("gedung", "Gedung"),
            ("kendaraan", "Kendaraan"),
            ("mesin", "Peralatan Dan Mesin"),
        ), string="Jenis Aset")
    company_id = fields.Many2one(comodel_name='res.company', string='Company')
    image = fields.Binary(string='Image')
    department_id = fields.Many2one(comodel_name='hr.department', string='Department')
    warehouse_id = fields.Many2one(comodel_name='stock.warehouse', string='Warehouse/Outlet')
    

    # tanah
    luas_tanah = fields.Float("Luas")
    alamat_tanah = fields.Char("Alamat")
    hak = fields.Char("Hak")
    tgl_sertifikat = fields.Date("Tanggal Sertifikat")
    no_sertifikat = fields.Char("Nomor Sertifikat")
    pengguna = fields.Char("Pengguna")
    history = fields.One2many(
        "account.asset.asset.line", "asset_id", "History"
    )

    # bangunan
    bertingkat = fields.Selection(string='Konstruksi Bertingkat', selection=[('ya', 'Ya'), ('tidak', 'Tidak'), ])
    beton = fields.Selection(string='Konstruksi Beton', selection=[('ya', 'Ya'), ('tidak', 'Tidak'), ])
    luas_lantai = fields.Float("Luas Lantai")
    alamat_gedung = fields.Char("Alamat")
    tgl_dok_gedung = fields.Date("Tanggal Dokumen Gedung")
    no_dok_gedung = fields.Char("Nomor Dokumen Gedung")
    luas_gedung = fields.Float("Luas")
    status = fields.Char("Status Tanah")
    no_tanah = fields.Char("Nomor Kode Tanah")

    # kendaraan

    merk = fields.Char("Merk")
    ukuran = fields.Char("Ukuran")
    bahan = fields.Char("Bahan")
    no_pabrik = fields.Char("Nomor Pabrik")
    no_rangka = fields.Char("Nomor Rangka")
    no_mesin = fields.Char("Nomor Mesin")
    no_polisi = fields.Char("Nomor Polisi")
    no_bpkb = fields.Char("Nomor BPKB")

    # =========================================================

    @api.depends('name')
    def _get_qr_code(self):
        # base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        # base_url += '/web#id=%d&view_type=form&model=%s' % (self.id or False, self._name)
        for x in self:
            # posisi = x.location_asset_id.name_get()
            # posisi = posisi[0][1] if posisi else ''
            if x.category_id.type == 'purchase':
                qr_string = 'Nama Aset : %s' % (x.name)
                qr_string += '\ncode : %s' % (self.code or '')           
                qr_string += '\nAsset Type : %s' % (self.category_id.name or '')
                barcode = x.env['ir.actions.report'].barcode('QR', qr_string, 128, 128, 1)
                x.qr_code = base64.b64encode(barcode)

class account_asset_asset_line(models.Model):
    _name = "account.asset.asset.line"

    asset_id = fields.Many2one("account.asset.asset", "Aset")
    outlet_id = fields.Many2one("stock.warehouse", "Outlet Asal")
    outlet_tujuan_id = fields.Many2one(
        "stock.warehouse", "Outlet Tujuan"
    )
    tanggal = fields.Date("Tanggal Perpindahan")


class AccountAssetLocation(models.Model):
    _name = 'account.asset.location'

    name = fields.Char('Nama', required=True)
    parent_id = fields.Many2one('account.asset.location', 'Parent')

    @api.multi
    def name_get(self):
        def get_names(cat):
            """ Return the list [cat.name, cat.parent_id.name, ...] """
            res = []
            while cat:
                res.append(cat.name)
                cat = cat.parent_id
            return res

        return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]

class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'


    def _prepare_move(self, line):
        category_id = line.asset_id.category_id
        account_analytic_id = line.asset_id.account_analytic_id
        analytic_tag_ids = line.asset_id.analytic_tag_ids
        depreciation_date = self.env.context.get('depreciation_date') or line.depreciation_date or fields.Date.context_today(self)
        company_currency = line.asset_id.company_id.currency_id
        current_currency = line.asset_id.currency_id
        prec = company_currency.decimal_places
        amount = current_currency._convert(
            line.amount, company_currency, line.asset_id.company_id, depreciation_date)
        asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
        move_line_1 = {
            'name': asset_name,
            'account_id': category_id.account_depreciation_id.id,
            'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'partner_id': line.asset_id.partner_id.id,
            'analytic_account_id': account_analytic_id.id if category_id.type == 'sale' else False,
            'analytic_tag_ids': [(6, 0, analytic_tag_ids.ids)] if category_id.type == 'sale' else False,
            'department_id':line.asset_id.department_id.id or False,
            'warehouse_id':line.asset_id.warehouse_id.id or False,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and - 1.0 * line.amount or 0.0,
        }
        move_line_2 = {
            'name': asset_name,
            'account_id': category_id.account_depreciation_expense_id.id,
            'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'partner_id': line.asset_id.partner_id.id,
            'department_id':line.asset_id.department_id.id or False,
            'warehouse_id':line.asset_id.warehouse_id.id or False,
            'analytic_account_id': account_analytic_id.id if category_id.type == 'purchase' else False,
            'analytic_tag_ids': [(6, 0, analytic_tag_ids.ids)] if category_id.type == 'purchase' else False,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and line.amount or 0.0,
        }
        move_vals = {
            'ref': line.asset_id.code,
            'date': depreciation_date or False,
            'journal_id': category_id.journal_id.id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
        }
        return move_vals
