# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.ab_role.models.models import _get_uid_from_group
class FormSurat(models.Model):
    _name = "form.surat"
    _inherit = "mail.thread"
    _description = "Form Surat Keluar Masuk"

    name = fields.Char("No. Surat")
    sender_id = fields.Many2one("hr.employee", string="Tertuju")
    note = fields.Char("Keterangan")
    datas_fname = fields.Char('Filename')
    type = fields.Selection([('url', 'URL'), ('binary', 'File')],
                            string='Type', required=True, default='binary', change_default=True,
                            help="You can either upload a file from your computer or copy/paste an internet link to your file.")

    # attachment_id = fields.Many2one("lampiran.skm", string="Attachment")
    attachment_ids = fields.Many2many('ir.attachment', string='Attachment List')
    # attachment_ids = fields.One2many('ir.attachment','form_surat_id', string='Attachment List')
    lampiran_id = fields.Many2one("lampiran.skm", string="List Attachment")
    date = fields.Date("Tanggal")
    subject = fields.Char("Perihal")
    department = fields.Char("Divisi")
    has_deliver = fields.Boolean()
    is_letter = fields.Boolean()
    vendor_id = fields.Many2one("res.partner", "Pengirim")

       
    @api.model
    def create(self, vals):
        res = super(FormSurat, self).create(vals)
        code = 'surat.masuk'
        if vals.get("is_letter") == True:
            code = 'surat.keluar'
        nama = self.env['ir.sequence'].next_by_code(code)
        vals["name"] = nama
        if res.attachment_ids:
                for ai in res.attachment_ids:
                    if ai.res_id == 0 or False:
                        ai.update({'res_id': res.id})
        return res

    @api.multi
    def write(self, vals):
        for i in self:
            if i.attachment_ids or vals["attachment_ids"]:
                for ai in i.attachment_ids:
                    if ai.res_id == 0 or False:
                        ai.update({'res_id': i.id})
        res = super(FormSurat, self).write(vals)
        return res


    @api.onchange("sender_id", "note", "datas_fname", "attachment_ids", "lampiran_id", "date", "subject", "department", "vendor_id")
    def show_send_notif(self):
        if self.has_deliver:
            for i in self:
                i.has_deliver = False

    @api.multi
    def surat_send_notif(self):
        user_ids = []
        if self.is_letter and self.sender_id.user_id:
            user_ids.append(self.sender_id.user_id.id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to know.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.has_deliver = True
        if not self.is_letter:
            user_hod = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
            for hod in user_hod:
                user_ids.append(hod)
            user_ceo = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
            for ceo in user_ceo:
                user_ids.append(ceo)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to know.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.has_deliver = True

class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    surat_id = fields.Many2one("form.surat",ondelete="cascade")
    form_surat_id = fields.Many2one("form.surat",ondelete="cascade")
    # skm_id = fields.Many2one("lampiran.skm",ondelete="cascade")

    def force_delete(self):
        return self.unlink()



class LampiranSkm(models.Model):
    _name = "lampiran.skm"
    _description = "File Attachment Surat Keluar Masuk"

    name = fields.Char("Name")

    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List') # Change type of field to o2m if bug and uncomment skm_id in ir.attachment
