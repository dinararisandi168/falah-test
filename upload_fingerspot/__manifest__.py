# -*- coding: utf-8 -*-
{
    "name": "Upload Fingerspot",
    "summary": """
        Upload Data Absensi dari Fingerspot """,
    "description": """
        Upload Data Absensi dari Fingerspot format .xls
    """,
    "author": "Achmad T. Zaini",
    "category": "Uncategorized",
    "version": "1.0",
    "depends": ["base", "hr_attendance"],
    "data": [
        "views/views.xml",
    ],
}
