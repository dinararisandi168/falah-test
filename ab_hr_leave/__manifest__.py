# -*- coding: utf-8 -*-
{
    "name": "Cuti FALAH",
    "summary": """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr", "hr_holidays", "ab_role", "ab_hr_attendance", "ab_hr_employees"],
    # always loaded
    "data": [
        'security/security.xml',
        'wizard/report_cuti.xml',
        "views/views.xml",
        "report/action_report.xml",
        "report/permintaan_cuti.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
