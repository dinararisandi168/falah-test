from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO as StringIO
import base64
from PIL import Image
import os 

class ReportCutiXlsx(models.AbstractModel):
    _name = 'report.ab_hr_leave.report_cuti_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_bold_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd')
        worksheet = workbook.add_worksheet('Rekap Cuti')
        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet.insert_image(
                "B2", "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
            )
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 2, 30)
        worksheet.set_column(3, 3, 50)
        worksheet.set_column(4, 4, 10)
        worksheet.set_column(5, 5, 30)
        worksheet.set_column(6, 6, 10)
        worksheet.merge_range('A7:A8', 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('D2:E3', 'REKAP CUTI', heading_format)
        value = dict(obj._fields['month'].selection).get(obj.month)
        BULAN = ("%s %s" % (value.upper(), obj.year))
        worksheet.merge_range('D4:E4', BULAN, sub_header)

        no = 1
        NO = []

        row = 7
        
        cuti_tahuanan = 15
      
        # query = """ with master_jatah as (select sum(hlr.number_of_days) as jatah,he.name as karyawan
        #         from hr_leave_report hlr
        #         left join hr_employee he on he.id = hlr.employee_id
        #         where hlr.number_of_days > 0 and hlr.employee_id is not null
        #         group by karyawan),

        #         master_cuti as(
        #         SELECT he.name as karyawan, he.nik as nik, hj.name as posisi, sum(hl.number_of_days) as cuti
        #         FROM hr_leave hl
        #         LEFT JOIN hr_employee he on he.id = hl.employee_id
        #         LEFT JOIN hr_job hj on hj.id = he.job_id
        #         WHERE hl.state = 'done' and hl.request_date_to >= '%s' AND hl.request_date_from  <= '%s'
        #         group by karyawan,nik,posisi
        #         )

        #         SELECT mc.karyawan,mc.nik,mc.posisi,mj.jatah,mc.cuti,(mj.jatah-mc.cuti) as hasil_akhir
        #         FROM master_cuti as mc
        #         left join master_jatah mj on mj.karyawan = mc.karyawan

        #          """ % (obj.date_from,obj.date_to)

        query = """ with master_jatah as (select sum(hlr.number_of_days) as jatah,he.name as karyawan
            from hr_leave_report hlr
            left join hr_employee he on he.id = hlr.employee_id
            where hlr.number_of_days > 0 and hlr.employee_id is not null and
            hlr.holiday_status_id = %s
            group by karyawan),

            master_cuti as(
            SELECT he.name as karyawan, he.nik as nik, hj.name as posisi, sum(hl.number_of_days) as cuti
            FROM hr_leave hl
            LEFT JOIN hr_employee he on he.id = hl.employee_id
            LEFT JOIN hr_job hj on hj.id = he.job_id
            WHERE hl.holiday_status_id = %s and 
            hl.state = 'done' AND hl.request_date_to >= '%s' AND hl.request_date_from  <= '%s'
            group by karyawan,nik,posisi
            )

            SELECT mc.karyawan,mc.nik,mc.posisi,mj.jatah,mc.cuti,(mj.jatah-mc.cuti) as hasil_akhir
            FROM master_cuti as mc
            left join master_jatah mj on mj.karyawan = mc.karyawan

                """ % (cuti_tahuanan, cuti_tahuanan, obj.date_from,obj.date_to)
        
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchall()
        worksheet.merge_range('B7:B8', 'Nama Karyawan', cell_text_format_top_left_right)
        worksheet.merge_range('C7:C8', 'NIK', cell_text_format_top_left_right)
        worksheet.merge_range('D7:D8', 'Posisi', cell_text_format_top_left_right)
        worksheet.merge_range('E7:E8', 'Jatah Cuti', cell_text_format_top_left_right)
        worksheet.merge_range('F7:F8', 'Cuti Yang Diambil', cell_text_format_top_left_right)
        worksheet.merge_range('G7:G8', 'Sisa Cuti', cell_text_format_top_left_right)

        for x in data:

                inc = [
                    x["karyawan"],
                    x["nik"],
                    x["posisi"],
                    x["jatah"],
                    x["cuti"],
                    x["hasil_akhir"],
                ]
                NO.append(no)
                no += 1
                row += 1
                col = 1

                for v in inc:
                    worksheet.write(row, col, v, text_style)
                    col += 1 
      
      
        worksheet.write_column('A9', NO, text_style)
