from odoo import _, api, fields, models

class TerhutangWizard(models.TransientModel):
    _name = 'disposisi.wizard'
    _description = 'Set To Draft Disposisi'

    disposisi_id = fields.Many2one('disposisi.disposisi', string='Disposisi')
    reason = fields.Text(string='Alasan')

    @api.multi
    def action_confirm(self):
        if self.disposisi_id:
            self.disposisi_id.message_post(body="Alasan : %s" % (self.reason))
            self.disposisi_id.draft()
    