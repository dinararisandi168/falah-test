from odoo import api, fields, models


class RequestWizard(models.TransientModel):
    _name = "request.wizard"
    _description = "Revision of Wizard"

    revision_text = fields.Text(string="Catatan Revisi")

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            request = self.env["hr.request.contract"].browse(active_id)
            request.catatan_revisi = self.revision_text
            request.state = "draft"
