# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree
from odoo.exceptions import UserError, RedirectWarning, ValidationError


class ItemType(models.Model):
    _name = "item.type"
    _description = "Item Type"

    name = fields.Char()


class InventoryParinventory(models.Model):
    _name = "inventory.part"
    _description = "Inventory Part"

    name = fields.Char()


class ProductTemplate(models.Model):
    _inherit = "product.template"

    item_id = fields.Many2one("item.type", string="Item Type")
    inventory_id = fields.Many2one("inventory.part", string="Inventory Part")
    serial_number = fields.Char(string="Serial Number")

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        return res

    @api.multi
    def unlink(self):
        for o in self:
            if o.user_has_groups("ab_role.group_hod_general_affair"):
                raise UserError(('General Affair tidak dapat menghapus produk.'))

        return super(ProductTemplate, self).unlink()

    @api.model
    def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
        result = super(ProductTemplate, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(result["arch"])
        if view_type == "tree" or view_type == "form":
            if self.user_has_groups("ab_role.group_hod_general_affair"):
                for tree in doc.xpath("//tree"):
                    tree.set("create", "false")
                    tree.set("edit", "false")
                for form in doc.xpath("//form"):
                    form.set("create", "false")
                    form.set("edit", "false")

        result["arch"] = etree.tostring(doc, encoding="unicode")
        return result


class ProductProduct(models.Model):
    _inherit = "product.product"

    item_id = fields.Many2one("item.type", string="Item Type")
    inventory_id = fields.Many2one("inventory.part", string="Inventory Part")
    serial_number = fields.Char(string="Serial Number")

    @api.multi
    def download_xlsx_report(self):
        template_report = 'ab_hr_inventory.product_list_xlsx'
        return self.env.ref(template_report).report_action(self)
