from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from collections import Counter


class AccountReportXlsx(models.AbstractModel):
    _name = 'report.ab_account_report.report_account_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'valign': 'vcenter', 'text_wrap': True, 'num_format': '#,##0.00'})
        text_style_numb = workbook.add_format({'font_size': 10, 'align': 'right', 'valign': 'vcenter', 'text_wrap': True, 'num_format': '#,##0.00'})
        text_style_bold = workbook.add_format({'font_size': 10, 'align': 'left', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_style_num_bold = workbook.add_format({'font_size': 10, 'valign': 'vcenter', 'text_wrap': True, 'bold': True,'num_format': '#,##0.00'})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 14})
        sub_header_colored = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 14, 'bold': True})
        sub_header = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        header_column = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        header_column.set_bg_color('#008dca')
        sub_header_colored.set_font_color('#EC4E2C')
        if obj.type == "neraca_standard":
            sheet = workbook.add_worksheet('Balance Sheet')
            sheet.set_column(1, 1, 10)
            sheet.set_column(2, 2, 40)
            sheet.set_column(3, 5, 30)
            
            header = ['CODE','DESCRIPTION','BALANCE']
            if obj.show_debit_credit:
                header = ['CODE','DESCRIPTION', 'DEBIT', 'CREDIT', 'BALANCE']
            
            cr = self._cr
            total_debit = 0
            total_credit = 0

            tanggal = obj.get_date_option( obj.date_option )['value']
            if obj.date_start:
                opening_date = obj.date_start

            where_company = "acc.company_id = %d" % obj.company_id.id
            where_posted = "1=1"
            if obj.posted:
                where_posted = "am.state = 'posted'"

            default_re = self.env['res.company']._company_default_get('account.account').account_re_id
            default_equity = self.env['res.company']._company_default_get('account.account').account_equity_id
            bs = self.env['res.company']._company_default_get('account.account').account_balance_sheet

            case_debit_date = "CASE WHEN aml.date >= '%s' AND aml.date <= '%s' AND %s THEN aml.debit ELSE 0 END" % (opening_date, tanggal, where_posted)
            case_credit_date = "CASE WHEN aml.date >= '%s' AND aml.date <= '%s' AND %s THEN aml.credit ELSE 0 END" % (opening_date, tanggal, where_posted)
            
            where_tipe = """
                            pac.code = '%s' or 
                            pac1.code = '%s' or 
                            pac2.code = '%s' or 
                            pac3.code = '%s' or 
                            pac4.code = '%s' or 
                            pac5.code = '%s' or 
                            pac6.code = '%s'
                        """ % (bs.code, bs.code, bs.code, bs.code, bs.code, bs.code, bs.code)
            sql = """
                    SELECT 
                            acc.code as "CODE",
                            acc.name as "DESCRIPTION",
                            SUM(COALESCE(%s, 0)) as "DEBIT", 
                            SUM(COALESCE(%s, 0)) as "CREDIT",
                            (SUM(COALESCE(%s, 0)) - SUM(COALESCE(%s, 0)))  as "BALANCE",
                            pac.code as "PARENT",
                            pac1.code as "PARENT1",
                            pac2.code as "PARENT2",
                            pac3.code as "PARENT3",
                            pac4.code as "PARENT4",
                            at.type as "TIPE"
                        FROM account_account acc
                        LEFT JOIN account_move_line aml on (aml.account_id = acc.id)
                        LEFT JOIN account_account pac on (pac.id = acc.parent_id)
                        LEFT JOIN account_account pac1 on (pac1.id = pac.parent_id)
                        LEFT JOIN account_account pac2 on (pac2.id = pac1.parent_id)
                        LEFT JOIN account_account pac3 on (pac3.id = pac2.parent_id)
                        LEFT JOIN account_account pac4 on (pac4.id = pac3.parent_id)
                        LEFT JOIN account_account pac5 on (pac5.id = pac4.parent_id)
                        LEFT JOIN account_account pac6 on (pac6.id = pac5.parent_id)
                        LEFT JOIN account_account_type at on (at.id = acc.user_type_id)
                        LEFT JOIN account_move am on (am.id = aml.move_id)
                        WHERE %s AND %s
                        GROUP BY acc.code, acc.name, pac.code, pac1.code, pac2.code, pac3.code, pac4.code, at.type
                        ORDER BY acc.code

            """ % (case_debit_date, case_credit_date, case_debit_date, case_credit_date, where_company, where_tipe) # where_company, where_tipe

            cr.execute(sql)
            results = cr.dictfetchall()
            data = []
            acc_child = {}
            footer_total = {}

            for line in results:
                
                total_debit += line['DEBIT']
                total_credit += line['CREDIT']

                if line['CODE'] not in acc_child:
                    acc_child[line['CODE']] = {'debit':0,'credit':0,'balance':0,'nama':line['DESCRIPTION']}
                
                if line['PARENT'] in acc_child:
                    acc_child[line['PARENT']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT']]['balance'] += line['BALANCE']
                
                if line['PARENT1'] in acc_child:
                    acc_child[line['PARENT1']]['debit'] += line['DEBIT'] 
                    acc_child[line['PARENT1']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT1']]['balance'] += line['BALANCE']
                
                if line['PARENT2'] in acc_child:
                    acc_child[line['PARENT2']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT2']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT2']]['balance'] += line['BALANCE']
                
                if line['PARENT3'] in acc_child:
                    acc_child[line['PARENT3']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT3']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT3']]['balance'] += line['BALANCE']


            last_header = {}
            last_header_2digit = {}
            last_header_3digit = {}
            row_count = 0
            profit_loss_after_tax = {}
            for line in results:
                row_count += 1
                if line['TIPE'] == 'view':
                    # Tambah baris total sebelum kepala COA baru 
                    if len(line['CODE']) == 1 and last_header:
                        template = {'CODE':"", 
                                    'DESCRIPTION':"TOTAL %s" % last_header['DESCRIPTION'], 
                                    'DEBIT':acc_child[last_header['CODE']]['debit'], 
                                    'CREDIT':acc_child[last_header['CODE']]['credit'], 
                                    'BALANCE':acc_child[last_header['CODE']]['balance'], 
                                    'TIPE':"view"}
                        data.append(template)

                    if len(line['CODE']) == 2 and last_header_2digit:
                        template = {'CODE':"", 
                                    'DESCRIPTION':"    TOTAL %s" % last_header_2digit['DESCRIPTION'], 
                                    'DEBIT':acc_child[last_header_2digit['CODE']]['debit'], 
                                    'CREDIT':acc_child[last_header_2digit['CODE']]['credit'], 
                                    'BALANCE':acc_child[last_header_2digit['CODE']]['balance'], 
                                    'TIPE':"view"}
                        data.append(template)
                    
                    if len(line['CODE']) == 3 and last_header_3digit:
                        template = {'CODE':"", 
                                    'DESCRIPTION':"    TOTAL %s" % last_header_3digit['DESCRIPTION'], 
                                    'DEBIT':acc_child[last_header_3digit['CODE']]['debit'], 
                                    'CREDIT':acc_child[last_header_3digit['CODE']]['credit'], 
                                    'BALANCE':acc_child[last_header_3digit['CODE']]['balance'], 
                                    'TIPE':"view"}
                        data.append(template)
                        
                    data.append(line)
                    # Kosongkan value untuk parent top COA (1 => ASSET, 2 => EQUITY, 3 => LIABILITIES)
                    if len(line['CODE']) in (1,2,3):
                        data[data.index(line)]['DEBIT'] = ""
                        data[data.index(line)]['CREDIT'] = ""
                        data[data.index(line)]['BALANCE'] = ""
                        if len(line['CODE']) == 1:
                            last_header = line
                        elif len(line['CODE']) == 2:
                            last_header_2digit = line
                        else:
                            last_header_3digit = line
                    # Isi value untuk parent COA
                    elif line['CODE'] in acc_child:
                        data[data.index(line)]['DEBIT'] = acc_child[line['CODE']]['debit']
                        data[data.index(line)]['CREDIT'] = acc_child[line['CODE']]['credit']
                        data[data.index(line)]['BALANCE'] = acc_child[line['CODE']]['balance']

                    if not obj.display_zero:
                        if line['BALANCE'] == 0:
                            data.remove(data[data.index(line)])
                    
                else:
                    if not obj.display_zero:
                        if line['BALANCE'] == 0:
                            continue

                    data.append(line)

                    if default_re:
                        if line['CODE'] == default_re.code:
                            awal_tahun = obj.get_date_option( obj.date_option )['value'][:4]+'-01-01'
                            start_date = awal_tahun
                            if str(obj.date_start) >= awal_tahun:
                                start_date = obj.date_start

                            pl_value = self.env['account.report.wizard'].create({
                                    'company_id': obj.company_id.id,
                                    'date_start': start_date,
                                    'date_stop': obj.get_date_option( obj.date_option )['value'],
                                    'display_zero': obj.display_zero,
                                    }).download_xlsx_report()

                            profit_loss_after_tax = {
                                                        'debit': pl_value.get('DEBIT', 0) * -1,
                                                        'credit': pl_value.get('CREDIT', 0) * -1,
                                                        'pl_balance': pl_value.get('BALANCE', 0) * -1,
                                                    }
                        
                            template = {'CODE':"", 
                                    'DESCRIPTION':"        Current %s" % default_re.name, 
                                    'DEBIT': profit_loss_after_tax.get('debit',0), 
                                    'CREDIT': profit_loss_after_tax.get('credit',0), 
                                    'BALANCE': profit_loss_after_tax.get('pl_balance',0), 
                                    'TIPE':"other"}
                            data.append(template)


                    # Tambah baris total di row record terakhir
                    if row_count == len(results):
                        if last_header:
                            template = {'CODE':"", 
                                        'DESCRIPTION':"TOTAL %s" % last_header['DESCRIPTION'], 
                                        'DEBIT':acc_child[last_header['CODE']]['debit'] + (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('debit', 0)), 
                                        'CREDIT':acc_child[last_header['CODE']]['credit'] + (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('credit', 0)), 
                                        'BALANCE':acc_child[last_header['CODE']]['balance'] + (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('pl_balance', 0)), 
                                        'TIPE':"view"}
                            data.append(template)
                        if last_header_2digit:
                            template = {'CODE':"", 
                                        'DESCRIPTION':"    TOTAL %s" % last_header_2digit['DESCRIPTION'], 
                                        'DEBIT':acc_child[last_header_2digit['CODE']]['debit'],  
                                        'CREDIT':acc_child[last_header_2digit['CODE']]['credit'], 
                                        'BALANCE':acc_child[last_header_2digit['CODE']]['balance'], 
                                        'TIPE':"view"}
                            data.append(template)
                        if last_header_3digit:
                            template = {'CODE':"", 
                                        'DESCRIPTION':"    TOTAL %s" % last_header_3digit['DESCRIPTION'], 
                                        'DEBIT':acc_child[last_header_3digit['CODE']]['debit'],  
                                        'CREDIT':acc_child[last_header_3digit['CODE']]['credit'], 
                                        'BALANCE':acc_child[last_header_3digit['CODE']]['balance'], 
                                        'TIPE':"view"}
                            data.append(template)


            len_header = len(header)
            sheet.merge_range(1, 1, 1, len_header, obj.company_id.name, heading_format)
            sheet.merge_range(2, 1, 2, len_header, 'Balance Sheet', sub_header_colored)
            sheet.merge_range(3, 1, 3, len_header, obj.get_date_option(obj.date_option)['view'], sub_header)

            for col, value in enumerate(header):
                row = 5
                sheet.write(row, col+1, value, header_column)
                
            no = 5
            liab_eq_sum = []
            for line in data:
                no += 1
                col = 0
                for i in header:
                    col += 1
                    style_font = text_style
                    content = line[i]
                    if line['TIPE'] == 'view':
                        style_font = text_style_num_bold
                    elif line['TIPE'] == 'footer':
                        style_font = text_style_num_bold
                    
                    
                    space = " "
                    if i == 'DESCRIPTION':
                        if len(line['CODE']) == 2:
                            content = "%s%s" % (space*2, line[i])
                        elif len(line['CODE']) == 3:
                            content = "%s%s" % (space*3, line[i])
                        elif len(line['CODE']) == 4:
                            content = "%s%s" % (space*4, line[i])
                        elif len(line['CODE']) == 6:
                            content = "%s%s" % (space*6, line[i])
                        elif len(line['CODE']) > 6:
                            content = "%s%s" % (space*8, line[i])

                    sheet.write(no, col, content, style_font)

                    total_le = {'debit':0,'credit':0,'balance':0}
                    for i in ('200000000', '300000000'):
                        total_le['debit'] += 0 if not acc_child.get(i) else acc_child.get(i)['debit']
                        total_le['credit'] += 0 if not acc_child.get(i) else acc_child.get(i)['credit']
                        total_le['balance'] += 0 if not acc_child.get(i) else acc_child.get(i)['balance'] 

                    # Add Current Retained Earning
                    total_le['debit'] += profit_loss_after_tax.get('debit', 0)
                    total_le['credit'] += profit_loss_after_tax.get('credit', 0)
                    total_le['balance'] += profit_loss_after_tax.get('pl_balance', 0)

                    # Add Footer LIABILITIES + EQUITY
                    sheet.write(len(data)+6, 1, "", text_style)
                    sheet.write(len(data)+6, 2, "TOTAL LIABILITIES & EQUITY", text_style_bold)
                    if obj.show_debit_credit:
                        sheet.write(len(data)+6, 3, total_le['debit'], text_style_numb)
                        sheet.write(len(data)+6, 4, total_le['credit'], text_style_numb)
                    sheet.write(len(data)+6, len_header, total_le['balance'], text_style_numb)
        else:
            sheet = workbook.add_worksheet('Profit & Loss')
            sheet.set_column(1, 1, 10)
            sheet.set_column(2, 2, 40)
            sheet.set_column(3, 5, 30)
            header = ['CODE','DESCRIPTION','BALANCE']
            if obj.show_debit_credit:
                header = ['CODE','DESCRIPTION', 'DEBIT', 'CREDIT', 'BALANCE']
            
            total_debit = 0
            total_credit = 0
            where_company = "acc.company_id = %d" % obj.company_id.id
            where_posted = "1=1"
            if obj.posted:
                where_posted = "am.state = 'posted'"
            profit = self.env['res.company']._company_default_get('account.account').account_profit_loss
            revenue = self.env['res.company']._company_default_get('account.account').account_income_id
            cogs = self.env['res.company']._company_default_get('account.account').account_cogs_id
            expense = self.env['res.company']._company_default_get('account.account').account_expense_id
            other = self.env['res.company']._company_default_get('account.account').account_other_id
            type_reverse = self.env['res.company']._company_default_get('account.account').type_reverse_ids

            if not profit:
                raise UserError('Account Profit Loss belum di set di Accounting > Configuration > Financial Report Config')
            
            if not revenue:
                raise UserError('Account Root Operating Revenue belum di set di Accounting > Configuration > Financial Report Config')
                
            if not cogs:
                raise UserError('Account Root COGS belum di set di Accounting > Configuration > Financial Report Config')

            if not expense:
                raise UserError('Account Root Operating Expenses belum di set di Accounting > Configuration > Financial Report Config')

            if not other:
                raise UserError('Account Root Other Income & Expenses belum di set di Accounting > Configuration > Financial Report Config')


            case_debit_date = "CASE WHEN aml.date >= '%s' AND aml.date <= '%s' AND %s THEN aml.debit ELSE 0 END" % (obj.date_start, obj.date_stop, where_posted)
            case_credit_date = "CASE WHEN aml.date >= '%s' AND aml.date <= '%s' AND %s THEN aml.credit ELSE 0 END" % (obj.date_start, obj.date_stop, where_posted)
            
            where_tipe = """
                            pac.code = '%s' or 
                            pac1.code = '%s' or 
                            pac2.code = '%s' or 
                            pac3.code = '%s' or 
                            pac4.code = '%s' or 
                            pac5.code = '%s' or 
                            pac6.code = '%s'
                        """ % (profit.code, profit.code, profit.code, profit.code, profit.code, profit.code, profit.code)
            sql = """
                    SELECT 
                            acc.code as "CODE",
                            acc.name as "DESCRIPTION",
                            SUM(COALESCE(%s, 0)) as "DEBIT", 
                            SUM(COALESCE(%s, 0)) as "CREDIT",
                            (SUM(COALESCE(%s, 0)) - SUM(COALESCE(%s, 0)))  as "BALANCE",
                            pac.code as "PARENT",
                            pac1.code as "PARENT1",
                            pac2.code as "PARENT2",
                            pac3.code as "PARENT3",
                            pac4.code as "PARENT4",
                            at.type as "TIPE",
                            at.id as "ID_TIPE"
                        FROM account_account acc
                        LEFT JOIN account_move_line aml on (aml.account_id = acc.id)
                        LEFT JOIN account_account pac on (pac.id = acc.parent_id)
                        LEFT JOIN account_account pac1 on (pac1.id = pac.parent_id)
                        LEFT JOIN account_account pac2 on (pac2.id = pac1.parent_id)
                        LEFT JOIN account_account pac3 on (pac3.id = pac2.parent_id)
                        LEFT JOIN account_account pac4 on (pac4.id = pac3.parent_id)
                        LEFT JOIN account_account pac5 on (pac5.id = pac4.parent_id)
                        LEFT JOIN account_account pac6 on (pac6.id = pac5.parent_id)
                        LEFT JOIN account_account_type at on (at.id = acc.user_type_id)
                        LEFT JOIN account_move am on (am.id = aml.move_id)
                        WHERE %s AND %s
                        GROUP BY acc.code, acc.name, pac.code, pac1.code, pac2.code, pac3.code, pac4.code, at.type, at.id
                        ORDER BY acc.code

            """ % (case_debit_date, case_credit_date, case_debit_date, case_credit_date, where_company, where_tipe ) # where_company, where_tipe

            self._cr.execute(sql)
            results = self._cr.dictfetchall()
            data = []
            acc_child = {}
            footer_total = {}

            for line in results:
                if line['ID_TIPE'] in type_reverse.ids:
                    line['DEBIT'] *= -1
                    line['CREDIT'] *= -1
                    line['BALANCE'] *= -1
                
                total_debit += line['DEBIT']
                total_credit += line['CREDIT']

                if line['CODE'] not in acc_child:
                    acc_child[line['CODE']] = {'debit':0,'credit':0,'balance':0,'nama':line['DESCRIPTION']}
                
                if line['PARENT'] in acc_child:
                    acc_child[line['PARENT']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT']]['balance'] += line['BALANCE']
                
                if line['PARENT1'] in acc_child:
                    acc_child[line['PARENT1']]['debit'] += line['DEBIT'] 
                    acc_child[line['PARENT1']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT1']]['balance'] += line['BALANCE']
                
                if line['PARENT2'] in acc_child:
                    acc_child[line['PARENT2']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT2']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT2']]['balance'] += line['BALANCE']
                
                if line['PARENT3'] in acc_child:
                    acc_child[line['PARENT3']]['debit'] += line['DEBIT']
                    acc_child[line['PARENT3']]['credit'] += line['CREDIT']
                    acc_child[line['PARENT3']]['balance'] += line['BALANCE']

            gross = {'CODE':"", 
                    'DESCRIPTION':"GROSS PROFIT", 
                    'DEBIT': acc_child[revenue.code]['debit'] - acc_child[cogs.code]['debit'],  
                    'CREDIT': acc_child[revenue.code]['credit'] - acc_child[cogs.code]['credit'], 
                    'BALANCE':acc_child[revenue.code]['balance'] - acc_child[cogs.code]['balance'], 
                    'TIPE':"view"}

            income_operation = {'CODE':"", 
                                'DESCRIPTION': "INCOME FROM OPERATION", 
                                'DEBIT': gross['DEBIT'] - acc_child[expense.code]['debit'],  
                                'CREDIT': gross['CREDIT'] - acc_child[expense.code]['credit'], 
                                'BALANCE': gross['BALANCE'] - acc_child[expense.code]['balance'], 
                                'TIPE':"view"}
            
            pl_before_tax = {'CODE':"", 
                            'DESCRIPTION': "NET PROFIT/LOSS (Before Tax)", 
                            'DEBIT': income_operation['DEBIT'] + acc_child[other.code]['debit'],  
                            'CREDIT': income_operation['CREDIT'] + acc_child[other.code]['credit'], 
                            'BALANCE': income_operation['BALANCE'] + acc_child[other.code]['balance'], 
                            'TIPE':"view"}

            pl_after_tax = {'CODE':"", 
                            'DESCRIPTION': "NET PROFIT/LOSS (After Tax)", 
                            'DEBIT': pl_before_tax['DEBIT'],  
                            'CREDIT': pl_before_tax['CREDIT'], 
                            'BALANCE': pl_before_tax['BALANCE'], 
                            'TIPE':"view"}


            last_header = {}
            last_header_3digit = {}
            row_count = 0
            # profit_loss_after_tax = {}
            for line in results:
                row_count += 1
                if line['TIPE'] == 'view':
                    # Tambah baris total sebelum kepala COA baru 
                    # if len(line['CODE'].strip('0')) == 1 and last_header:
                    if len(line['CODE']) == 1 and last_header:
                        template = {'CODE':"", 
                                    'DESCRIPTION':"TOTAL %s" % last_header['DESCRIPTION'], 
                                    'DEBIT':acc_child[last_header['CODE']]['debit'], 
                                    'CREDIT':acc_child[last_header['CODE']]['credit'], 
                                    'BALANCE':acc_child[last_header['CODE']]['balance'], 
                                    'TIPE':"view"}
                        data.append(template)
                    
                    if len(line['CODE']) == 3 and last_header_3digit:
                        template = {'CODE':"", 
                                    'DESCRIPTION':"    TOTAL %s" % last_header_3digit['DESCRIPTION'], 
                                    'DEBIT':acc_child[last_header_3digit['CODE']]['debit'], 
                                    'CREDIT':acc_child[last_header_3digit['CODE']]['credit'], 
                                    'BALANCE':acc_child[last_header_3digit['CODE']]['balance'], 
                                    'TIPE':"view"}
                        data.append(template)

                    if line['CODE'] == expense.code:
                        data.append(gross)
                    elif line['CODE'] == other.code:
                        data.append(income_operation)
                        
                    data.append(line)
                    # Kosongkan value untuk parent top COA (1 => ASSET, 2 => EQUITY, 3 => LIABILITIES)
                    # if len(line['CODE'].strip('0')) == 1:
                    if len(line['CODE']) in (1,3):
                        data[data.index(line)]['DEBIT'] = ""
                        data[data.index(line)]['CREDIT'] = ""
                        data[data.index(line)]['BALANCE'] = ""
                        if len(line['CODE']) == 1:
                            last_header = line
                        else:
                            last_header_3digit = line
                    # Isi value untuk parent COA
                    elif line['CODE'] in acc_child:
                        data[data.index(line)]['DEBIT'] = acc_child[line['CODE']]['debit']
                        data[data.index(line)]['CREDIT'] = acc_child[line['CODE']]['credit']
                        data[data.index(line)]['BALANCE'] = acc_child[line['CODE']]['balance']

                    if not obj.display_zero:
                        if line['BALANCE'] == 0:
                            data.remove(data[data.index(line)])
                    
                else:
                    if not obj.display_zero:
                        if line['BALANCE'] == 0:
                            continue

                    data.append(line)
            if row_count == len(results):
                if last_header_3digit:
                    template = {'CODE':"", 
                                'DESCRIPTION':"    TOTAL %s" % last_header_3digit['DESCRIPTION'], 
                                'DEBIT':acc_child[last_header_3digit['CODE']]['debit'],  
                                'CREDIT':acc_child[last_header_3digit['CODE']]['credit'], 
                                'BALANCE':acc_child[last_header_3digit['CODE']]['balance'], 
                                'TIPE':"view"}
                    data.append(template)
                if last_header:
                    template = {'CODE':"", 
                                'DESCRIPTION':"TOTAL %s" % last_header['DESCRIPTION'], 
                                'DEBIT':acc_child[last_header['CODE']]['debit'], #+ (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('debit', 0)), 
                                'CREDIT':acc_child[last_header['CODE']]['credit'], #+ (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('credit', 0)), 
                                'BALANCE':acc_child[last_header['CODE']]['balance'], #+ (0 if last_header['CODE'] != default_equity.code else profit_loss_after_tax.get('pl_balance', 0)), 
                                'TIPE':"view"}
                    data.append(template)

                template = {'CODE':"", 
                            'DESCRIPTION': pl_before_tax['DESCRIPTION'], 
                            'DEBIT': pl_before_tax['DEBIT'],
                            'CREDIT': pl_before_tax['CREDIT'], 
                            'BALANCE': pl_before_tax['BALANCE'], 
                            'TIPE':"view"}
                data.append(template)

                template = {'CODE':"", 
                            'DESCRIPTION': pl_after_tax['DESCRIPTION'], 
                            'DEBIT': pl_after_tax['DEBIT'],
                            'CREDIT': pl_after_tax['CREDIT'], 
                            'BALANCE': pl_after_tax['BALANCE'], 
                            'TIPE':"view"}
                data.append(template)

            len_header = len(header)
            sheet.merge_range(1, 1, 1, len_header, obj.company_id.name, heading_format)
            sheet.merge_range(2, 1, 2, len_header, 'PROFIT & LOSS (STANDARD)', sub_header_colored)
            sheet.merge_range(3, 1, 3, len_header, 'From %s to %s' % (obj.date_start, obj.date_stop), sub_header)

            for col, value in enumerate(header):
                row = 5
                sheet.write(row, col+1, value, header_column)
                
            no = 5
            for line in data:
                no += 1
                col = 0
                for i in header:
                    col += 1
                    style_font = text_style
                    content = line[i]
                    if line['TIPE'] == 'view':
                        style_font = text_style_num_bold
                    elif line['TIPE'] == 'footer':
                        style_font = text_style_num_bold
                    
                    space = " "
                    if i == 'DESCRIPTION':
                        if len(line['CODE']) == 3:
                            content = "%s%s" % (space*2, line[i])
                        elif len(line['CODE']) == 4:
                            content = "%s%s" % (space*4, line[i])
                        elif len(line['CODE']) == 6:
                            content = "%s%s" % (space*6, line[i])
                        elif len(line['CODE']) > 6:
                            content = "%s%s" % (space*8, line[i])

                    sheet.write(no, col, content, style_font)
            
            return pl_after_tax