
from odoo import _, api, fields, models
from io import BytesIO as StringIO
import base64
from xlsxwriter.utility import xl_cell_to_rowcol
from xlsxwriter.utility import xl_rowcol_to_cell
from xlsxwriter.utility import xl_range


class ReportBpjsExcel(models.Model):
    _name = 'report.ab_hr_payroll.report_bpjstk'
    _inherit = 'report.report_xlsx.abstract'
    _description = 'Report Bpjs TK Excel'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 12, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'num_format': '#,##0.00' })
        num_style = workbook.add_format({'font_size': 12, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True })
        text_style_heading = workbook.add_format({'font_size': 12, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True })
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12,'text_wrap': True,})
        sub_header = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        text_style_heading.set_border()
        date_style = workbook.add_format({'font_size': 12, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True,'num_format': 'd mmm yyyy' })
        heading_format.set_border()
        heading_format.set_align('center')
        heading_format.set_bg_color('#AEB9C8')
        merge_format = workbook.add_format({'bold': 1,'border': 1,'align': 'center','valign': 'vcenter'})
        worksheet1 = workbook.add_worksheet('REKAP BPJS TK %s'% obj.period_start.strftime("%b"))

        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet1.insert_image(
            "A2", "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
        )
        header = ['NO','NOMOR KPJ','NIK','NO KTP','NAMA LENGKAP','TGL LAHIR','JUMLAH UPAH','JKK (0.24%)','JKM (0.3%)','IURAN JHT PEMBERI KERJA (3.7%)','IURAN JHT TENAGA KERJA (2%)','IURAN JP PEMBERI KERJA (2%)','IURAN JP TENAGA KERJA (1%)','Total dari Pemberi Kerja','Total dari Tenaga Kerja']
        
        self._cr.execute("""
                       select 
                        row_number() over (order by he.name) as no,
                        he.bpjs_tk as no_kjp,
                        he.nik as nik,
                        he.identification_id as ktp,
                        he.name as nama,
                        he.birthday as tgl_lahir,
                        COALESCE(hc.wage,0.0) as gaji,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'JKK'),0.0) as bpjs_jkk,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'JKM'),0.0) as bpjs_jkm,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'JHT'),0.0) as bpjs_jht_comp,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'BPJSTKHT'),0.0) as bpjs_jht,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'JP'),0.0) as bpjs_jp_com,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'BPJSTKJP'),0.0) as bpjs_jp,
                        COALESCE(sum(abs(hpl.total)) FILTER(WHERE hpl.code = 'BPJSTK'),0.0) as bpjs_pot_karyawan,
                        COALESCE(sum(abs(hpl.total)::int) FILTER(WHERE hpl.code in ('KTK','TKJP')),0.0) as bpjs_pot_comp
                        
                        FROM hr_payslip_line as hpl
                        LEFT join hr_payslip as hp on hpl.slip_id = hp.id
                        LEFT join hr_employee as he on he.id = hp.employee_id
                        left join hr_contract as hc on hc.id = hp.contract_id 
                        WHERE hp.date_from >= '%s' and hp.date_to <= '%s' and hp.state = 'done'
                        group by he.id,hc.wage 
                        """ % (obj.period_start, obj.period_end))

        data = self._cr.dictfetchall()

        combine_value = {k: [d.get(k) for d in data] for k in list(data[0].keys())}
        col_header = 0
        length_data = 0
        for x,y in zip(header,combine_value): 
            data_list = combine_value[y]
            to_str = map(str, data_list) # convert to str if item of list is not str (to get)
            length_data = len(max(list(to_str), key=len))
            worksheet1.set_column(col_header,col_header, max(len(x),length_data)+1.5)
            worksheet1.merge_range(6,col_header,9,col_header,x,heading_format)
            worksheet1.write_column(10,col_header,data_list,text_style_heading if y != 'tgl_lahir' else date_style)
            col_header += 1
        
        worksheet1.set_column(col_header,col_header, max(len(x),length_data)+1.5)
        worksheet1.merge_range(6,col_header,9,col_header,'TOTAL',heading_format)
        for row in range(len(combine_value['nama'])):
            worksheet1.write_formula(10+row, col_header, '=SUM(%s)' % (xl_range(10+row, 7, 10+row , col_header-3)), text_style_heading)
