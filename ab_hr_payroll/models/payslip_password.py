from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError

class PayslipPassword(models.TransientModel):
    _name = 'payslip.password'

    password = fields.Char(string='Password')

    @api.multi
    def input_password(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            payslip = self.env["hr.payslip"].browse(active_id)
            if self.password != payslip.employee_id.nik:
                raise UserError(("Password salah!"))
            return payslip.print_payslip()
            