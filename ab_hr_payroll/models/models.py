# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,time
from dateutil import relativedelta
import calendar
import numpy as np

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    is_vendor_payroll = fields.Boolean(compute='_compute_is_vendor_payroll', string='Hide Submit')
    
    @api.depends('payslip_batches_line')
    def _compute_is_vendor_payroll(self):
        for i in self:
            i.is_vendor_payroll = True if i.payslip_batches_line else False

    payslip_batches_line = fields.One2many('hr.payslip.run', 'invoice_id', string='Payslip')    

class HrPayslipRun(models.Model):
    _inherit = "hr.payslip.run"

    state = fields.Selection([
        ("draft", "Draft"),
        ("approve", "Approved"),
        ("close", "Close"),
    ], string="Status", index=True, readonly=True, copy=False, default="draft",)

    filename = fields.Char()
    data_file = fields.Binary()
    invoice_id = fields.Many2one('account.invoice', string='Invoice', copy=False)

    def create_bill(self):
        if self.journal_id.code != 'BILL':
            raise ValidationError(('Salary Jurnal Harus Vendor Bills!'))

        code_salary_rule = []
        salary_rule = []
        bill_line = []
        bill = self.env['hr.salary.rule'].search([('in_bill', '=', True)]).mapped('code')

        # Tampung nama kode salary rule dan namanya untuk cari per salary rule
        if self.slip_ids:
            for line in self.slip_ids:
                for sr in line.details_by_salary_rule_category:
                    if sr.code in bill and sr.code not in code_salary_rule:
                        code_salary_rule.append(sr.code)
                        salary_rule.append(sr.name)
        # NGUMPULIN DATA LINE NYA
        if self.slip_ids:
            for x in range(0, len(code_salary_rule)):
                uang = 0
                account = self.env['hr.salary.rule'].search([('code', '=', code_salary_rule[x])], limit=1).account_credit
                for line in self.slip_ids:
                    for sr in line.details_by_salary_rule_category:
                        # bill = self.env['hr.salary.rule'].search([('in_bill', '=', True),('code', '=', code_salary_rule[x])]).mapped('code')
                        # if sr.code == code_salary_rule[x] and sr.code in bill:
                        if sr.code == code_salary_rule[x]:
                            uang += sr.amount

                res = ((0, 0, {
                    'name': salary_rule[x],
                    'quantity': 1,
                    'price_unit': uang,
                    'account_id': account.id,
                }))
                bill_line.append(res)

        # BUAT VENDOR BILL
        if bill_line:
            bill = self.env['account.invoice'].create(self._prepare_vendor_bil())
            if bill:
                bill.update({'invoice_line_ids': bill_line})
                self.invoice_id = bill.id

    # BUAT HEADER VENDOR BILL
    def _prepare_vendor_bil(self):
        self.ensure_one()
        # journal_id = self.env['account.journal'].search([('code', '=', 'BILL')])

        # if not journal_id:
        #     raise ValidationError(('Salary Jurnal Harus Vendor Bills!'))

        bill_vals = {
            'partner_id': self.env.user.company_id.id,
            'company_id': self.env.user.company_id.id,
            'user_id': self.env.user.id,
            'type': 'in_invoice',
            'currency_id': self.env.user.company_id.currency_id.id,
            'journal_id': self.journal_id.id,
        }
        return bill_vals

    def close_payslip_run(self):
        for payslip_ids in self.slip_ids:
            payslip_ids.action_payslip_done()
        return self.write({"state": "close"})

    def approve(self):
        for payslip_ids in self.slip_ids:
            payslip_ids.action_payslip_done()
            self.write({"state": "approve"})

    def draft_payslip_run(self):
        for payslip_ids in self.slip_ids:
            payslip_ids.action_payslip_draft()
            self.write({"state": "draft"})
        # if all([line.state == "done" for line in self.slip_ids]):

        #     self.write({"state": "approve"})
        # else:
        #     raise ValidationError(_("Masih ada payslip yang belum done."))


class HrPayslipLine(models.Model):
    _inherit = "hr.payslip.line"

    payslip_run_id = fields.Many2one('hr.payslip.run', string='Payslip Batches', store=True, related='slip_id.payslip_run_id')
    department_id = fields.Many2one(
        comodel_name="hr.department",
        string="Department",
        related="employee_id.department_id",
        store=True,
    )
    tanggal = fields.Date(string="Tanggal", related="slip_id.date_to", store=True)


class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    filename = fields.Char()
    data_file = fields.Binary()
    tus = fields.Float('Tunjangan Uang Saku')
    tl = fields.Float('Tunjangan Lainnya')

    @api.multi
    def action_payslip_done(self):
        self.compute_sheet()
        return self.write({'state': 'done'})

    @api.multi
    def action_payslip_draft(self):
        self.compute_sheet()
        return self.write({'state': 'draft'})

    def to_draft(self):
        self.write({"state": "draft"})

    def print_payslip(self):
        return self.env.ref("hr_payroll.action_report_payslip").report_action(self)

    @api.multi
    def action_payslip_done(self):
        res = super(HrPayslip, self).action_payslip_done()

        return res

    @api.multi
    def action_payslip_draft(self):
        res = super(HrPayslip, self).action_payslip_draft()

        return res

    def get_input_other(self, a, b):
        if a.contract_id:
            return sum(x.amount for x in a.input_line_ids if x.code == b)
        else:
            return 0

    def check(self, data):
        print("========================", data)
    
    def get_overtime(self):
        first_date = self.date_from.replace(day=1)
        last_date = calendar.monthrange(self.date_from.year,self.date_from.month)[1]
        self._cr.execute("""
        SELECT SUM(LEMBUR)
        FROM HR_OVERTIME
        WHERE EMPLOYEE_ID = %s
	        AND DATE_OVERTIME >= '%s'
	        AND DATE_OVERTIME <= '%s-%s-%s'
            AND STATE = 'paid'
        """ % (self.employee_id.id, str(first_date),str(self.date_from.year),str(self.date_from.month),str(last_date)))
        # (self.employee_id.id, str(self.date_from),str(self.date_to)))
        res = self._cr.fetchone()
        return res[0] or 0.0

    @api.multi
    def compute_input_line_ids(self, result):
        for payslip in self:
            input_generate_code = [x["code"] for x in result]
            for input2 in payslip.input_line_ids:
                if input2.amount == 0:
                    input2.unlink()

            list_amount_exist = [
                x.code for x in payslip.input_line_ids if x.amount != 0
            ]
            if self.contract_id:
                for x in result:
                    if x["code"] not in list_amount_exist:
                        payslip.input_line_ids.create(x)

    @api.multi
    def get_result_data(self):
        result = []
        for payslip in self:
            result.append(
                {
                    "name": "Koreksi Tunjangan",
                    "code": "KT",
                    "amount": 0,
                    "contract_id": payslip.contract_id.id,
                    "payslip_id": payslip.id,
                }
            )
            result.append(
                {
                    "name": "Koreksi Potongan",
                    "code": "KP",
                    "amount": 0,
                    "contract_id": payslip.contract_id.id,
                    "payslip_id": payslip.id,
                }
            )
        return result

    @api.multi
    def compute_sheet(self):
        for payslip in self:
            number = payslip.number or self.env["ir.sequence"].next_by_code(
                "salary.slip"
            )
            # delete old payslip lines
            payslip.line_ids.unlink()
            # set the list of contract for which the rules have to be applied
            # if we don't give the contract, then the rules to apply should be for all current contracts of the employee

            result = payslip.get_result_data()
            payslip.compute_input_line_ids(result)
            contract_ids = payslip.contract_id.ids or self.get_contract(
                payslip.employee_id, payslip.date_from, payslip.date_to
            )
            lines = [
                (0, 0, line)
                for line in self._get_payslip_lines(contract_ids, payslip.id)
            ]
            payslip.write({"line_ids": lines, "number": number})
        return True

    @api.multi
    def compute_akumulasi_netto(self, employee_id, total_netto):
        rule_netto_id = self.env['ir.model.data'].xmlid_to_object('ab_hr_payroll.total_netto').id,
        january = '%s-01-01' % (fields.Date.today().year)
        payslip_line_ids = self.env['hr.payslip.line'].search([
            ('slip_id.date_from', '>=', january),
            ('slip_id.date_from', '<=', fields.Date.today()),
            ('slip_id.employee_id', '=', employee_id.id),
            ('slip_id.state', '=', 'done'),
            ('salary_rule_id', 'in', rule_netto_id)
        ])

        return sum(payslip_line_ids.mapped('total')) + total_netto

    def compute_basic_salary(self):
        period_start = fields.Date.start_of(self.date_to,'month')
        period_end = fields.Date.end_of(self.date_to,'month')
        working_day = np.busday_count(period_start,period_end + relativedelta.relativedelta(days=1) ,weekmask=[1,1,1,1,1,0,0]) # jumlah hari kerja periode bulan saat ini
        
        ct_period_from = self.date_from # cut-off period from
        ct_period_to = self.date_to # cut-off period to
        total_hari_kerja_sebulan = 0
        total_jumlah_hari_kerja = 0
        holiday = [i.date_from.date() for i in self.contract_id.resource_calendar_id.global_leave_ids]
        self._cr.execute("""
                    select 
                        count(*) FILTER(WHERE attendance_category is null) as masuk,
                        count(*) FILTER(WHERE attendance_category = 'sakit') as sakit,
                        count(*) FILTER(WHERE attendance_category = 'izin') as izin,
                        count(*) FILTER(WHERE attendance_category = 'cuti') as cuti,
                        count(*) FILTER(WHERE attendance_category = 'tdk_masuk') as tdk_masuk,
                        count(*) FILTER(WHERE attendance_category = 'tanpa_keterangan') as tanpa_keterangan
                        
                    from 
                        hr_attendance 
                    where 
                        employee_id = '%s' and check_in between '%s' and '%s'""" % (self.employee_id.id,datetime.combine(ct_period_from, time.min), datetime.combine(ct_period_to, time.max)))
        work = self._cr.dictfetchall()[0]
    
        if period_start <= self.contract_id.date_start <= period_end:
            total_jumlah_hari_kerja = np.busday_count(self.contract_id.date_start,period_end + relativedelta.relativedelta(days=1),weekmask=[1,1,1,1,1,0,0],holidays=holiday)
            total_jumlah_hari_kerja -= work['tanpa_keterangan']
            hari_kerja = working_day - work['cuti'] if work['cuti'] else working_day
            total_hari_kerja_sebulan = hari_kerja
            return (self.contract_id.wage / total_hari_kerja_sebulan) * total_jumlah_hari_kerja
        
        if ct_period_from <= self.contract_id.date_start  <= ct_period_to: # Case karyawan masuk setelah cut-off period from
            total_jumlah_hari_kerja = np.busday_count(self.contract_id.date_start,period_end + relativedelta.relativedelta(days=1),weekmask=[1,1,1,1,1,0,0],holidays=holiday)
            total_jumlah_hari_kerja -= work['tanpa_keterangan']
            hari_kerja = working_day - work['cuti'] if work['cuti'] else working_day
            total_hari_kerja_sebulan = hari_kerja
            return (self.contract_id.wage / total_hari_kerja_sebulan) * total_jumlah_hari_kerja

        if self.contract_id.date_end and period_start <= self.contract_id.date_end <= period_end: # case employee resign
            total_jumlah_hari_kerja = np.busday_count(period_start,self.contract_id.date_end + relativedelta.relativedelta(days=1),weekmask=[1,1,1,1,1,0,0],holidays=holiday)
            total_jumlah_hari_kerja -= work['tanpa_keterangan']
            total_hari_kerja_sebulan = working_day
            return (self.contract_id.wage / total_hari_kerja_sebulan) * total_jumlah_hari_kerja
        
        if period_start > self.contract_id.date_start < ct_period_from:
            total_jumlah_hari_kerja = np.busday_count(period_start,period_end + relativedelta.relativedelta(days=1),weekmask=[1,1,1,1,1,0,0],holidays=holiday)
            total_jumlah_hari_kerja -= work['tanpa_keterangan']
            total_hari_kerja_sebulan = working_day
            return (self.contract_id.wage / total_hari_kerja_sebulan) * total_jumlah_hari_kerja


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    in_bill = fields.Boolean(string="In Bill", default=True)

    def _compute_rule(self, localdict):
        import pdb
        localdict['pdb'] = pdb
        localdict['self'] = self
        localdict['print'] = print
        res = super(HrSalaryRule, self)._compute_rule(localdict)
        return res
