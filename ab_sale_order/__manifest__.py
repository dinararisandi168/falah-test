# -*- coding: utf-8 -*-
{
    "name": "ab_sale_order",
    "summary": """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. ISMATA NUSANTARA ABADI",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "sale","ab_attachment", "account", "mail", "project", "ab_role","account","ab_send_email"],
    # always loaded
    "data": [
        'data/payment_term.xml',
        'security/ir.model.access.csv',
        "report/invoice.xml",
        "views/views.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}