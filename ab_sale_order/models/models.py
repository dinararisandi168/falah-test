# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.ab_role.models.models import _get_uid_from_group

class SaleOrder(models.Model):
    _inherit = "sale.order"

    no_quotation = fields.Char("Nomor")
    project_id = fields.Many2one("project.project", string="Project Reference")

    # state = fields.Selection([
    #     ('draft', 'Draft'),
    #     ('sent', 'Submitted'),
    #     ('sale', 'Reviewed'),
    #     ('done', 'Reviewed'),
    #     ('cancel', 'Quotation'),
    # ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submitted', 'Submitted'),
        ('pm_reviewed', 'PM Reviewed'),
        ('bd_reviewed', 'FAT Reviewed'),
        ('quotation', 'Quotation'),
        ('quotation_submit', 'Submitted'),
        ('fat_reviewed_so', 'FAT Reviewed'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Order Status', readonly=True, copy=False, store=True, default='draft')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachment Line')
    type = fields.Selection([
        ('quot', 'Quotation'),
        ('order', 'Sales Order'),
    ], string='Type', readonly=True, copy=False, store=True, default='quot')
    
    state_a = fields.Selection(related='state', store=False, track_visibility=False)
    state_b = fields.Selection(related='state', store=False, track_visibility=False)

    @api.multi
    def action_submit(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("project.group_project_manager").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'project.group_project_manager')
        self.write({"state": "submitted"})

    @api.multi
    def action_review(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_direktur").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to review.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'review', 'group', 'ab_role.group_direktur')
        self.write({"state": "bd_reviewed"})

    @api.multi
    def action_review1(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_direktur").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'ab_role.group_direktur')
        self.write({"state": "bd_reviewed"})

    @api.multi
    def action_approve(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("account.group_account_invoice").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'sale.order', 'submit', 'group', 'account.group_account_invoice')
        self.write({"state": "quotation"})

    @api.multi
    def action_cancel(self):
        self.write({"state": "cancel"})

    def action_quotation_fa_review(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_direktur").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "fat_reviewed_so"})

    def action_quotation_submit(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_hod_fat").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "quotation_submit"})

    @api.model
    def create(self, vals):
        vals['no_quotation'] = self.env['ir.sequence'].next_by_code(
            'seq.sale.order')
        # user_ids = _get_uid_from_group(
        #     self, self.env.ref("ab_role.group_direktur").id
        # )
        # partner_ids = [
        #     self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        # ]
        # self.message_post(
        #     body="Check",
        #     subject=self.name,
        #     partner_ids=partner_ids,
        #     message_type="notification",
        #     subtype="mail.mt_comment",
        # )
        return super(SaleOrder, self).create(vals)

    def _get_users(self, group_user):
        res = []
        temp_group = []

        for group in group_user:
            users = self.env['ir.model.data'].xmlid_to_object(group).users
            if users:
                for i in users:
                    temp_group.append((4, i.partner_id.id))

        for i in temp_group:
            if not i in res:
                res.append(i)

        return res

    def write(self, vals):
        if vals.get('state'):
            users = self._get_users(['sales_team.group_sale_manager', 'sales_team.group_sale_salesman', 'sales_team.group_sale_salesman_all_leads'])

            self.env['mail.message'].create({
                'message_type': "email",
                "subtype": self.env.ref("mail.mt_comment").id,
                'body': 'Hai, Sale Order anda sudah berstatus <b>{}</b>'.format(vals['state']),
                'subject': 'Sale Order',
                'needaction_partner_ids': users,
                'starred_partner_ids': users,
                'model': self._name,
                'res_id': self.id
            })

        return super(SaleOrder, self).write(vals)

    @api.onchange('project_id')
    def _onchange_project_id(self):
        if self.project_id:
            self.partner_id = self.project_id.partner_id

    @api.multi
    def action_confirm(self):
        users = self._get_users(['project.group_project_manager', 'ab_role.group_project_leader', 'ab_role.group_project_admin'])
        partner_ids = [x[1] for x in users]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction state has been sales order. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        return super(SaleOrder, self).action_confirm()

    @api.multi
    def print_quotation(self):
        return self.env.ref('sale.action_report_saleorder').with_context(discard_logo_check=True).report_action(self)

    @api.multi
    @api.returns('mail.message', lambda value: value.id)
    def message_post(self, **kwargs):
        if self.env.context.get('mark_so_as_sent'):
            self.env.user.company_id.set_onboarding_step_done('sale_onboarding_sample_quotation_state')
        return super(SaleOrder, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)


class InvoiceDataReportWithPay(models.AbstractModel):

    _name = "report.account.report_invoice_with_payments"
    _description = "Kirim data untuk report"

    def terbilang_(self, n):
        n = abs(int(n))

        satuan = [
            "",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas",
        ]

        if n >= 0 and n <= 11:
            hasil = [satuan[n]]
        elif n >= 12 and n <= 19:
            hasil = self.terbilang_(n % 10) + ["Belas"]
        elif n >= 20 and n <= 99:
            hasil = self.terbilang_(
                n / 10) + ["Puluh"] + self.terbilang_(n % 10)
        elif n >= 100 and n <= 199:
            hasil = ["Seratus"] + self.terbilang_(n - 100)
        elif n >= 200 and n <= 999:
            hasil = self.terbilang_(n / 100) + \
                ["Ratus"] + self.terbilang_(n % 100)
        elif n >= 1000 and n <= 1999:
            hasil = ["seribu"] + self.terbilang_(n - 1000)
        elif n >= 2000 and n <= 999999:
            hasil = self.terbilang_(n / 1000) + \
                ["Ribu"] + self.terbilang_(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            hasil = (
                self.terbilang_(n / 1000000) +
                ["Juta"] + self.terbilang_(n % 1000000)
            )
        elif n >= 1000000000 and n <= 999999999999:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Milyar"]
                + self.terbilang_(n % 1000000000)
            )
        else:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Triliun"]
                + self.terbilang_(n % 100000000000)
            )
        return hasil

    def terbilang(self, n):

        if n == 0:
            return "nol"
        t = self.terbilang_(n)
        while "" in t:
            t.remove("")
        return " ".join(t)

    @api.model
    def _get_report_values(self, docids, data=None):

        return {
            "doc_ids": docids,
            "doc_model": "account.invoice",
            "docs": self.env["account.invoice"].browse(docids),
            "report_type": data.get("report_type") if data else "",
            "terbilang": self.terbilang,
        }


class InvoiceDataReportWitouthPay(models.AbstractModel):

    _name = "report.account.report_invoice"
    _description = "Kirim data untuk report"

    def terbilang_(self, n):
        n = abs(int(n))

        satuan = [
            "",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas",
        ]

        if n >= 0 and n <= 11:
            hasil = [satuan[n]]
        elif n >= 12 and n <= 19:
            hasil = self.terbilang_(n % 10) + ["Belas"]
        elif n >= 20 and n <= 99:
            hasil = self.terbilang_(
                n / 10) + ["Puluh"] + self.terbilang_(n % 10)
        elif n >= 100 and n <= 199:
            hasil = ["Seratus"] + self.terbilang_(n - 100)
        elif n >= 200 and n <= 999:
            hasil = self.terbilang_(n / 100) + \
                ["Ratus"] + self.terbilang_(n % 100)
        elif n >= 1000 and n <= 1999:
            hasil = ["seribu"] + self.terbilang_(n - 1000)
        elif n >= 2000 and n <= 999999:
            hasil = self.terbilang_(n / 1000) + \
                ["Ribu"] + self.terbilang_(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            hasil = (
                self.terbilang_(n / 1000000) +
                ["Juta"] + self.terbilang_(n % 1000000)
            )
        elif n >= 1000000000 and n <= 999999999999:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Milyar"]
                + self.terbilang_(n % 1000000000)
            )
        else:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Triliun"]
                + self.terbilang_(n % 100000000000)
            )
        return hasil

    def terbilang(self, n):

        if n == 0:
            return "nol"
        t = self.terbilang_(n)
        while "" in t:
            t.remove("")
        return " ".join(t)

    @api.model
    def _get_report_values(self, docids, data=None):
        # name = "account.report_invoice_document"
        # name = "ab_sale_order.custom_print_invoice"
        # report = self.env["ir.actions.report"]._get_report_from_name(name)

        # subtotal = 0
        # pajak = 0
        # n = 0
        # if invoice_line_ids:
        #     for i in invoice_line_ids:
        #         for t in i.invoice_line_tax_ids:
        #             pajak += (t.amount / 100) * i.price_subtotal
        #     total += i.price_subtotal
        # n = total + pajak

        return {
            "doc_ids": docids,
            "doc_model": "account.invoice",
            "docs": self.env["account.invoice"].browse(docids),
            "report_type": data.get("report_type") if data else "",
            "terbilang": self.terbilang,
        }
