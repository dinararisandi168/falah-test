# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, RedirectWarning, ValidationError

class AccountBudgetPost(models.Model):
    _inherit = "account.budget.post"
    
    name = fields.Selection([('Project', 'Project'), ('General', 'General')], required=True)
    account_ids = fields.Many2many(
        "account.account",
        "account_budget_rel",
        "budget_id",
        "account_id",
        "Accounts",
        domain=lambda self:[("deprecated", "=", False),('user_type_id','=',self.env.ref('account.data_account_type_liquidity').id)],
    )

class Wizard(models.TransientModel):
    _name = 'catatan.wizard.budget'
    _description = 'Wizard Revisi Budget'

    text_revisi = fields.Text(string='Revision Notes')

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            budget = self.env['crossovered.budget'].browse(active_id)
            budget.catatan_revisi = self.text_revisi
            budget.action_budget_draft()

class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    catatan_revisi = fields.Char("Revision Notes", readonly=True,track_visibility="onchange")

#     @api.constrains('general_budget_id.account_ids','date_from','date_to')
#     def _check_budget_account_ids(self):
#         if self.general_budget_id:
#             sql = """            
#             select 
#             aa.name,abp.name as budget,cbl.date_to
#             from crossovered_budget_lines cbl
#             left join account_budget_post abp on (cbl.general_budget_id = abp.id)
#             left join account_budget_rel abr on (abr.budget_id = abp.id)
#             left join account_account aa on (aa.id = abr.account_id)
#             where abr.account_id in %s and cbl.date_to >= %s and cbl.id != %s and cbl.analytic_account_id = %s or
#             abr.account_id in %s and cbl.date_to >= %s and cbl.id != %s and cbl.analytic_account_id is null
#             limit 5
#             """
#             params = [
#                 tuple(self.general_budget_id.account_ids.ids) or (None,),self.date_from,self.id,self.analytic_account_id.id,
#                 tuple(self.general_budget_id.account_ids.ids) or (None,),self.date_from,self.id
#                 ]
#             self._cr.execute(sql,tuple(params))
#             hasil = self._cr.dictfetchall()            
#             message = False
#             string =  ''
#             if hasil:
#                 for x in hasil:                    
#                     string += "Akun %s sudah ada di budget %s sampai tanggal %s\n" % (x['name'],x['budget'],str(x['date_to']))
#                     message = True

#             if message:
#                 raise ValidationError(string)


class CrossoveredBudgetLines(models.Model):
    _inherit = "crossovered.budget.lines"

    @api.onchange('analytic_account_id')
    def _onchange_analytic_account_id(self):
        if self.analytic_account_id:
            project_id = self.env['project.project'].search([('analytic_account_id', '=', self.analytic_account_id.id)], limit = 1)
            if project_id:
                self.planned_amount = project_id.budget_assumption
