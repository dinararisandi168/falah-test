# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree
from odoo.addons.ab_role.models.models import _get_uid_from_group

class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "Atachment"

    contact_id = fields.Many2one("res.partner")
    is_contact = fields.Boolean()


class Contact(models.Model):
    _inherit = "res.partner"

    attacment_line = fields.One2many("ir.attachment", "contact_id", string="Attachment List")
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            partner_id = "%s" % (record.name)
            result.append((record.id, partner_id))
        return result
    
    # @api.model
    # def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
    #     result = super(Contact, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     doc = etree.XML(result["arch"])
    #     # Vendor
    #     if self.env.context.get("supplier", []) and view_type == "form" or view_type == "tree" or view_type == "kanban":
    #         if self.user_has_groups("purchase.group_purchase_manager") or self.user_has_groups("ab_role.group_hod_fat"):
    #             for node_form in doc.xpath("//form"):
    #                 node_form.set("edit", "true")
    #                 node_form.set("create", "true")
    #             for form in doc.xpath("//kanban"):
    #                 form.set("create", "true")
    #                 form.set("delete", "true")
    #             for form in doc.xpath("//tree"):
    #                 form.set("create", "true")
    #                 form.set("delete", "true")
    #         else:
    #             for node_form in doc.xpath("//form"):
    #                 node_form.set("edit", "false")
    #                 node_form.set("create", "false")
    #             for form in doc.xpath("//kanban"):
    #                 form.set("create", "false")
    #                 form.set("delete", "false")
    #             for form in doc.xpath("//tree"):
    #                 form.set("create", "false")
    #                 form.set("delete", "false")

    #     # Customer
    #     if self.env.context.get("customer", []) and view_type == "form" or view_type == "tree" or view_type == "kanban":
    #         if self.user_has_groups("ab_role.group_project_admin") or self.user_has_groups("purchase.group_purchase_manager") or self.user_has_groups("ab_role.group_hod_fat") or self.user_has_groups("account.group_account_invoice"):
    #             for form in doc.xpath("//form"):
    #                 form.set("edit", "True")
    #                 form.set("create", "True")
    #             for kanban in doc.xpath("//kanban"):
    #                 kanban.set("create", "True")
    #                 kanban.set("delete", "True")
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "True")
    #                 tree.set("delete", "True")
    #         else:
    #             for node_form in doc.xpath("//form"):
    #                 node_form.set("edit", "false")
    #                 node_form.set("create", "false")
    #             for kanban in doc.xpath("//kanban"):
    #                 kanban.set("create", "false")
    #                 kanban.set("delete", "false")
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "false")
    #                 tree.set("delete", "false")

    #     result["arch"] = etree.tostring(doc, encoding="unicode")
    #     return result
