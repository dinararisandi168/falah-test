from odoo import models, fields, api, _
from dateutil import relativedelta
from datetime import datetime, timedelta
import datetime
import base64
import xlwt
from io import BytesIO as StringIO
import re
import xlsxwriter
from xlsxwriter.utility import xl_range


class StockMutasi(models.TransientModel):
    _name = "stock.mutasi"
    _inherit = "report.report_xlsx.abstract"

    name = fields.Char("File Name")
    date_start = fields.Date(string="Date Start", default=fields.Date.today)
    # , default=datetime.now().strftime("%Y-%m-01")
    date_end = fields.Date(string="Date End", default=fields.Date.today)
    # , default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10]
    location_id = fields.Many2one(comodel_name="stock.location", string="Location")
    data_file = fields.Binary("File", readonly=True)

    def generate_xlsx_report(self, workbook):
        today = str(datetime.datetime.now())[:10]
        date = str(self.date_end + relativedelta.relativedelta(days=1))[:10]
        lokasi = self.location_id.id

        # master_code as (
        #     select pt.default_code as kode_barang, sm.product_id,
        #     sum(case when sm.location_dest_id = %s and date >= '%s' and date <= '%s' and state = 'done'
        #     then product_uom_qty else 0 end) as total_masuk,
        #     sum(case when sm.location_id = %s and date >= '%s' and date <= '%s' and state = 'done'
        #     then product_uom_qty else 0 end) as total_keluar

        #     from product_product pp
        #     left join stock_move sm on sm.product_id = pp.id
        #     --left join product_product pp on sm.product_id = pp.id
        #     left join product_template pt on pp.product_tmpl_id = pt.id

        #     group by product, product_id
        # ),

        # select pt.default_code as kode_barang,
        #                         select pt.description as keterangan,
        #                         select pt.uom_id.name as ket,ang,
        query = """ with master_stock as (
                        select pt.name as product, sm.product_id,
                        sum(case when sm.location_dest_id = %s and date >= '%s' and date <= '%s' and state = 'done' 
                        then product_uom_qty else 0 end) as total_masuk,
                        sum(case when sm.location_id = %s and date >= '%s' and date <= '%s' and state = 'done' 
                        then product_uom_qty else 0 end) as total_keluar
                        
                        from product_product pp
                        left join stock_move sm on sm.product_id = pp.id
                        --left join product_product pp on sm.product_id = pp.id
                        left join product_template pt on pp.product_tmpl_id = pt.id
                        
                        group by product, product_id
                    ),

                    master_code as (
                        select pt.default_code as kode_barang, sm.product_id,
                        sum(case when sm.location_dest_id = %s and date >= '%s' and date <= '%s' and state = 'done'
                        then product_uom_qty else 0 end) as total_masuk,
                        sum(case when sm.location_id = %s and date >= '%s' and date <= '%s' and state = 'done'
                        then product_uom_qty else 0 end) as total_keluar

                        from product_product pp
                        left join stock_move sm on sm.product_id = pp.id
                        --left join product_product pp on sm.product_id = pp.id
                        left join product_template pt on pp.product_tmpl_id = pt.id

                        group by kode_barang, product_id
                    ),

                    master_stock_selisih as (
                        
                        select sm.product_id,
                        coalesce(sum(case when sm.location_dest_id = %s then product_uom_qty else 0 end)-sum(case when sm.location_id = %s then product_uom_qty else 0 end),0) as stock_selisih
                        from stock_move sm                                   
                        where date >= '%s' and date <= '%s'  and state = 'done' group by sm.product_id
                    ),

                    master_stock_quant as (
                        select product_id, sum(quantity) as qty from stock_quant where location_id = %s group by product_id
                    )
                    
                    select ms.product,
                    (coalesce(msq.qty,0)-coalesce(stock_selisih,0)) - (ms.total_masuk-ms.total_keluar) as stock_awal,
                    ms.total_masuk,ms.total_keluar,
                    coalesce(msq.qty,0)-coalesce(stock_selisih,0) as stock_akhir

                    from master_stock ms 
                    left join master_stock_selisih mss on ms.product_id = mss.product_id
                    left join master_stock_quant msq on ms.product_id = msq.product_id 
                    order by ms.product """ % (
            lokasi,
            self.date_start,
            self.date_end,
            lokasi,
            self.date_start,
            self.date_end,
            lokasi,
            self.date_start,
            self.date_end,
            lokasi,
            self.date_start,
            self.date_end,
            lokasi,
            lokasi,
            date,
            today,
            lokasi,
        )

        self.env.cr.execute(query)
        stock_data = self.env.cr.dictfetchall()

        judul = "Stock Mutasi %s s/d %s.xls" % (self.date_start, self.date_end)
        # style = xlwt.easyxf("font: bold 1", "#,##0.00")
        style_currency = xlwt.easyxf("", "#,##0.00")
        currency_style = xlwt.easyxf(
            "borders: left thin, right thin, top thin, bottom thin", "#,##0.00"
        )
        letter_style = xlwt.easyxf(
            "borders: left thin, right thin, top thin, bottom thin",
        )
        heading = xlwt.easyxf(
            "align: vert centre, horiz centre;  font: bold 1", "#,##0.00"
        )
        centered_align = xlwt.easyxf(
            "align: vert centre, horiz centre; font: bold off, color black;\
                                borders: top_color black, bottom_color black, right_color black, left_color black,\
                                left thin, right thin, top thin, bottom thin;"
        )
        thead = xlwt.easyxf(
            "align: vert centre, horiz centre; font: bold on, color black;\
                                borders: top_color black, bottom_color black, right_color black, left_color black,\
                                left thin, right thin, top thin, bottom thin;"
        )
        # style = xlwt.XFStyle()
        bg = xlwt.Pattern()
        bg.pattern = bg.SOLID_PATTERN  # NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
        bg.pattern_fore_colour = 22
        # style.pattern = bg
        # centered_align.pattern = bg

        user = self.env["res.users"].browse(self.env.uid)
        partner = self.env["res.partner"].browse(user.company_id.id)
        partner_name = partner.name
        book = xlwt.Workbook()
        sheet = book.add_sheet("Stock Mutasi")
        sheet.col(0).width = 60 * 18
        sheet.col(1).width = 60 * 55
        sheet.col(2).width = 60 * 150
        sheet.col(3).width = 60 * 140
        sheet.col(4).width = 60 * 40
        sheet.col(5).width = 60 * 40
        sheet.col(6).width = 60 * 50
        sheet.col(7).width = 60 * 50
        sheet.col(8).width = 60 * 50
        sheet.col(9).width = 60 * 50
        sheet.col(10).width = 60 * 50

        # sheet.write(2, 4, "LAPORAN STOCK OPNAME", style)
        # sheet.write(3, 4, partner_name, style)
        sheet.write_merge(1, 1, 0, 9, "LAPORAN STOCK OPNAME", heading)
        sheet.write_merge(2, 2, 0, 9, partner_name, heading)
        # sheet.write(3, 3, 0, 9, date, heading)

        # sheet.write_merge(3, 3, 0, 9, "PER", date, heading)

        # file_data = BytesIO()
        # workbook = xlsxwriter.Workbook(file_data, {"in_memory": True})
        # cell = workbook.add_worksheet("Data")
        # logo = BytesIO(base64.b64decode(partner.image))
        # cell.insert_image(
        #     0, 0, "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
        # )
        # cell.set_column(3, 3, 22)

        # buf_image = BytesIO(partner.image)
        # cell.insert_image(
        #     1, 1, base64.b64encode(buf_image.getvalue()), {"image_data": buf_image}
        # )

        field = [
            "No",
            "Kode Barang",
            "Rincian Barang",
            "Keterangan",
            "QTY",
            "Ket",
        ]
        for d in field:
            sheet.write_merge(5, 6, field.index(d), field.index(d), d, thead)

        stock_head = [
            "Stock Awal",
            "Pengurangan",
            "Penambahan",
            "Stock Akhir",
        ]
        for s in stock_head:
            sheet.write(5, stock_head.index(s) + 6, s, thead)
            sheet.write(6, stock_head.index(s) + 6, "QTY", thead)

        # sheet.write(5, 6, "Stock Awal", letter_style)
        # sheet.write(5, 7, "Pengurangan", centered_align)
        # sheet.write(5, 8, "Penambahan", centered_align)
        # sheet.write(5, 9, "Stock Akhir", centered_align)

        data_tambah = []
        for x in stock_data:
            data_obj = self.env["product.template"].search(
                [("name", "=", x["product"])]
            )
            if data_obj:
                for d in data_obj:

                    v = {
                        "kode_barang": d.default_code,
                        "keterangan": d.description,
                        "ket": d.uom_id.name,
                    }
                    data_tambah.append(v)

        rowcode = 7
        for kodebar in data_tambah:
            cd = [kodebar["kode_barang"]]
            for code in cd:
                sheet.write(rowcode, 1, code, centered_align)
                rowcode += 1
        rowcode = 7
        for notebar in data_tambah:
            ket = [notebar["keterangan"]]
            for note in ket:
                sheet.write(rowcode, 3, note, letter_style)
                rowcode += 1
        rowcode = 7
        for notebar in data_tambah:
            ket = [notebar["ket"]]
            for note in ket:
                sheet.write(rowcode, 5, note, centered_align)
                rowcode += 1
        row = 6
        for p in stock_data:
            produk = [p["product"]]
            row += 1
            for cetak in produk:
                sheet.write(row, 2, cetak, letter_style)
        row = 6
        for s in stock_data:
            stock = [s["stock_awal"]]
            row += 1
            for cetak in stock:
                sheet.write(row, 4, cetak, centered_align)
        row = 6
        no = 1
        for x in stock_data:
            d = [
                x["stock_awal"],
                x["total_keluar"],
                x["total_masuk"],
                x["stock_akhir"],
            ]
            row += 1
            col = 6
            sheet.write(row, 0, no, centered_align)
            no += 1
            for v in d:
                sheet.write(row, col, v, centered_align)
                col += 1
        row += 1

        file_data = StringIO()
        i = book.save(file_data)
        out = base64.encodestring(file_data.getvalue())
        self.write({"data_file": out, "name": judul})

        view = self.env.ref("ab_stock_mutasi.stock_mutasi_view_form")
        return {
            "name": _("Mutasi Stock"),
            "view_type": "form",
            "view_mode": "form",
            "res_model": "stock.mutasi",
            "views": [(view.id, "form")],
            "res_id": self.id,
            "type": "ir.actions.act_window",
            "target": "new",
        }