{
    "name": "Stock Mutasi",
    "summary": """
        Stock Mutasi
        """,
    "description": """
        Stock Mutasi
    """,
    "author": "PT. Ismata Nusantara Abadi",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "12.0.1.0.0",
    # any module necessary for this one to work correctly
    "depends": ["base", "stock", "report_xlsx"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": ["demo/demo.xml"],
}
