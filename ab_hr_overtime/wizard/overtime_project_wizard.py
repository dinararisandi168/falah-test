from odoo import _, api, fields, models
from datetime import date

class OvertimeProjectWizard(models.TransientModel):
    _name = 'overtime.project.wizard'
    _description = 'Overtime Project Wizard'

    month = fields.Selection([
        ('1', 'Januari'),
        ('2', 'Febuari'),
        ('3', 'Maret'),
        ('4', 'April'),
        ('5', 'Mei'),
        ('6', 'Juni'),
        ('7', 'Juli'),
        ('8', 'Agustus'),
        ('9', 'September'),
        ('10', 'Oktober'),
        ('11', 'November'),
        ('12', 'Desember'),
    ], string='Bulan', default=str(date.today().month))

    year = fields.Selection(sorted([(x, x) for x in range(1930, date.today().year + 1)], reverse=True), string='Tahun', default=date.today().year)
    
    @api.multi
    def action_download(self):
        return self.env.ref('ab_hr_overtime.report_overtime_project_xlsx').report_action(self)