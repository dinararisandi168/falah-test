# -*- coding: utf-8 -*-
{
    "name": "CUSTOM HR OVERTIME",
    "category": "Custom Modules",
    "summary": """
       Custom Module OVERTIME
       """,
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. Ismata Nusantara Abadi",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr_payroll", "hr", "hr_contract", "hr_attendance", "calendar", "ab_role", "project", "ab_hr_employees"],
    # always loaded
    "data": [
        "security/menu_security.xml",
        "security/ir.model.access.csv",
        "views/data.xml",
        "views/views.xml",
        "views/hr_overtime_rule.xml",
        "report/report_spl.xml",
        "report/report.xml",
        'wizard/report_overtime_wizard.xml',
        'wizard/overtime_project_views.xml',
    ],
    # only loaded in demonstration
}
