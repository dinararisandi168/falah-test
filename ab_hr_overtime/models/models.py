from _datetime import datetime, date
from odoo import api, fields, models, _
from odoo import SUPERUSER_ID, api
from odoo.exceptions import UserError
from dateutil import relativedelta
from odoo.tools.safe_eval import safe_eval
import logging
from odoo.addons.ab_role.models.models import _get_uid_from_group

_logger = logging.getLogger(__name__)


class hr_overtime(models.Model):
    _name = "hr.overtime"
    _inherit = "mail.thread"

    name = fields.Char(
        "Reference",
        required=True,
        readonly=True,
        select=True,
        default="/",
        states={"draft": [("readonly", False)]},
        track_visibility="onchange",
    )
    date = fields.Date(
        "Create Date",
        required=True,
        select=True,
        default=date.today(),
        readonly=True,
        states={"draft": [("readonly", False)]},
        track_visibility="onchange",
    )

    date_overtime = fields.Date(
        "Overtime Date",
        required=True,
        select=True,
        default=date.today(),
        # default=datetime.now(),
        readonly=True,
        states={"draft": [("readonly", False)]},
        track_visibility="onchange",
    )

    keperluan = fields.Char(
        "Keperluan",
        required=True,
        readonly=True,
        states={"draft": [("readonly", False)]},
        track_visibility="onchange",
    )
    employee_id = fields.Many2one(
        "hr.employee",
        string="Pelaksana Tugas",
        required=True,
        default=lambda self: self.env["hr.employee"].search(
            [("user_id", "=", self.env.uid)], limit=1
        ),
        readonly=True,
        states={"draft": [("readonly", False)]},
        # domain="[('peneliti', '=', False)]",
    )
    parent_id = fields.Many2one(
        "hr.employee",
        "Pemberi Tugas",
        readonly=True,
        states={"draft": [("readonly", False)]},
        default=lambda self: self.env["hr.employee"]
        .search([("user_id", "=", self.env.user.id)])
        .parent_id.id,
    )
    department_id = fields.Many2one(
        "hr.department",
        string="Department",
        readonly=True,
        related="employee_id.department_id",
        states={"draft": [("readonly", False)]},
    )
    approval_id = fields.Many2one('res.users', string='Next Approver', readonly=True,track_visibility="onchange")
    time_start = fields.Float("Waktu Awal", track_visibility="onchange")
    time_stop = fields.Float("Waktu Akhir (Pagi)", track_visibility="onchange")
    time_start2 = fields.Float("Waktu Awal", track_visibility="onchange")
    time_stop2 = fields.Float("Waktu Akhir (Sore)", track_visibility="onchange")
    durasi = fields.Float("Durasi", digits=(2, 2), track_visibility="onchange")
    durasi_absen = fields.Float("Durasi Absen", digits=(2, 2), track_visibility="onchange", readonly=True, states={"draft": [("readonly", False)]})
    lembur = fields.Float("Amount")
    payslip_id = fields.Many2one("hr.payslip", "Payslip", readonly=True)
    lembur_file = fields.Char()
    datas_fname = fields.Char('Filename')
    is_overtime = fields.Boolean("")
    is_project = fields.Boolean(string='Is a Overtime With Employee Project', related='employee_id.is_project')
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit_lod", "Submitted"),
            ("submit_hod", "Submitted"),
            ("approval", "Approval"),
            ("confirm1", "LOD Confirmed"),
            ("confirm2", "HOD Confirmed"),
            ("submit2_lod", "Submitted Evidance"),
            ("submit2_hod", "Submitted Evidance"),
            ("review", "Review"),
            ("confirm3", "LOD Reviewed"),
            ("confirm4", "HOD Reviewed"),
            ("validate", "ACC Validate"),
            ("verify", "HR Verified"),
            ("paid", "FAT Approved"),
            ("cancel", "Cancel"),
            ("done", "Done"),
        ],
        "State",
        readonly=True,
        select=True,
        track_visibility="onchange",
        default="draft",
    )
    state_a = fields.Selection(related='state', store=False, track_visibility=False)
    state_b = fields.Selection(related='state', store=False, track_visibility=False)
    type_overtime_id = fields.Many2one("hr.overtime.rule", string="Jenis Lembur", track_visibility="onchange")
    durasi_progresif = fields.Float("Durasi Progresif", track_visibility="onchange", readonly=True, states={"draft": [("readonly", False)]})
    project_id = fields.Many2one("project.project", string="Project", track_visibility="onchange", readonly=True, states={"draft": [("readonly", False)]})
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")

    is_hrd = fields.Boolean(compute='_compute_is_hrd', string='Is HRD')
    is_reviewed = fields.Boolean(string='Reviewed', copy=False)
    is_hod = fields.Boolean(string='HOD')
    show_verify_for_hod = fields.Boolean(compute='_compute_show_verify_for_hod', string='Show verify for HOD')
    is_approval = fields.Boolean(compute='_compute_is_approval', string='Is Approval')
    is_reviewer = fields.Boolean(compute='_compute_is_reviewer', string='Is Reviewer')
    time_to_submit_evidance = fields.Boolean(string='Time to submit evidance', copy=False)
    show_button_evidance = fields.Boolean(compute='_compute_show_button_evidance', string='Show Button Evidance')
    
    @api.depends('state')
    def _compute_show_button_evidance(self):
        for i in self:
            i.show_button_evidance =  i.state == 'approval' and i.time_to_submit_evidance

    @api.depends('state')
    def _compute_is_approval(self):
        for i in self:
            if not i.time_to_submit_evidance:
                i.is_approval = i.env.user.id == i.approval_id.id
    
    @api.depends('state')
    def _compute_is_reviewer(self):
        for i in self:
            if i.time_to_submit_evidance:
                i.is_reviewer = i.env.user.id == i.approval_id.id

    def _compute_show_verify_for_hod(self):
        for i in self:
            if i.is_reviewed == True and i.state == 'review':
                i.show_verify_for_hod = True

    @api.depends('state')
    def _compute_is_hrd(self):
        for i in self:
            i.is_hrd = i.user_has_groups('ab_role.group_hod_human_resource') or i.user_has_groups('ab_role.group_staff_human_resource')

    def stock_draft(self):
        return self.write({"state": "draft"})

    def stock_cancel(self):
        return self.write({"state": "cancel"})

    def stock_cancel_in_done(self):
        return self.write({"state": "cancel"})

    def stock_confirm(self):
        return self.write({"state": "done"})

    def action_submit(self):
        # hod_project = self.env.ref("project.group_project_manager")
        ceo = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)

        if not self.is_project:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.write({"state": "submit_hod"})
        elif self.is_project:
            if self.employee_id.parent_id.user_id.id in ceo:
                if not self.lembur_file:
                    raise UserError(('Anda belum upload file lembur.'))
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
                self.is_reviewed = True
                self.write({"state": "review"})
            else:
                if not self.employee_id.parent_id:
                    raise UserError(('Pelaksana tugas overtime tidak memiliki atasan.'))
                partner = self.employee_id.parent_id.user_id.partner_id.id
                if partner:
                    self.message_post(
                        body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                        subject="%s - [Need Verify or Approve]" % (self.name),
                        partner_ids=[(6, 0, [partner])],
                        message_type="notification",
                        subtype="mail.mt_comment",)
                self.approval_id = self.employee_id.parent_id.user_id.id
                self.env['send.email.notification'].send(self.id, 'hr.overtime', 'submit', 'user', self.employee_id.parent_id.user_id)
                self.write({"state": "approval"})
            
    def action_approve(self):
        ceo = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        atasan = self.env.user.employee_ids.parent_id.user_id.mapped("id")[0]
        if atasan in ceo:
            partner = self.employee_id.user_id.partner_id.id
            if partner:
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve evidance. \nThanks",
                    subject="%s" % (self.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",)
            self.time_to_submit_evidance = True
            self.approval_id = False
        else:
            self.approval_id = atasan
            partner = self.approval_id.partner_id.id
            if partner:
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to approve. \nThanks",
                    subject="%s" % (self.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",)
                self.env['send.email.notification'].send(self.id, 'hr.overtime', 'submit', 'user', self.employee_id.parent_id.user_id)

    def review(self):
        ceo = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        atasan = self.env.user.employee_ids.parent_id.user_id.mapped("id")[0]
        if atasan in ceo:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.is_reviewed = True
            self.approval_id = False
        else:
            self.approval_id = atasan
            partner = self.approval_id.partner_id.id
            if partner:
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to approve. \nThanks",
                    subject="%s" % (self.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",)

    def submit_evidance(self):
        if not self.lembur_file:
            raise UserError(('Anda belum upload file lembur.'))
        partner = self.employee_id.parent_id.user_id.partner_id.id
        if partner:
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                subject="%s - [Need Review]" % (self.name),
                partner_ids=[(6, 0, [partner])],
                message_type="notification",
                subtype="mail.mt_comment",)
        self.approval_id = self.employee_id.parent_id.user_id.id
        self.write({"state": "review"})


    def action_confirm1(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "confirm2"})

        # else:
        #     user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
        #     partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        #     self.message_post(
        #         body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
        #         subject=self.name,
        #         partner_ids=partner_ids,
        #         message_type="notification",
        #         subtype="mail.mt_comment",
        #     )
        #     return self.write({"state": "confirm1"})

    def action_confirm2(self):
        user_ids = _get_uid_from_group(self, self.env.user.id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "confirm2"})

    def action_submit2(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "submit2_hod"})

    def action_confirm3(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "confirm4"})

    def action_confirm4(self):
        user_ids = _get_uid_from_group(self, self.env.ref("account.group_account_user").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "confirm4"})

    def action_validate(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "validate"})

    def action_verify(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "verify"})

    def action_paid(self):
        if self.lembur == 0:
            raise UserError(('Field Amount masih kosong, silahkan compute amount terlebih dahulu!'))
        else:
            return self.write({"state": "paid"})

    def durasi_pro(self, durasi):
        total_dur_pro = 0
        max_duration = 0
        if durasi < 1:
            fix_duration = int(str(durasi)[2])
        else:
            fix_duration = durasi
        remaining = fix_duration
        for overtime in self:
            first_rule = overtime.type_overtime_id.overtime_line[0]
            for y in overtime.type_overtime_id.overtime_line:
                dur_pro = 0
                if fix_duration <= first_rule.duration:
                    total_dur_pro = fix_duration * first_rule.factor_progresive
                    break
                elif max_duration < fix_duration:
                    dur = 0
                    max_duration += y.duration
                    if remaining > y.duration:
                        dur = y.duration
                        remaining -= y.duration
                    else:
                        dur = remaining
                    dur_pro = dur * y.factor_progresive
                total_dur_pro += dur_pro
        return total_dur_pro

    @api.onchange("project_id", "date_overtime")
    def change_type_overtime(self):

        obj_rule = self.env['hr.overtime.rule']
        hari = self.date_overtime.strftime('%A')
        if (hari == "Saturday" or hari == "Sunday") and self.project_id:
            self.is_overtime = True
            tipe_lembur = obj_rule.search([('type_overtime', '=', 'otend')], limit=1)
        elif (hari == "Saturday" or hari == "Sunday") and not self.project_id:
            self.is_overtime = False
            tipe_lembur = obj_rule.search(
                [('type_overtime', '=', 'non-otend')], limit=1)
        elif (hari != "Saturday" or hari != "Sunday") and self.project_id:
            self.is_overtime = True
            tipe_lembur = obj_rule.search([('type_overtime', '=', 'otday')], limit=1)
        else:
            self.is_overtime = False
            tipe_lembur = obj_rule.search(
                [('type_overtime', '=', 'non-otday')], limit=1)
        return {
            "value": {
                "type_overtime_id": tipe_lembur.id,
            }
        }

    @api.multi
    def print_spl(self):
        return self.env.ref("ab_hr_overtime.surat_perintah_lembur").report_action(self)

    @api.multi
    def unlink(self):
        for record in self:
            if record.state not in "draft":
                raise UserError(
                    _("You cannot delete a document is in %s state.") % (
                        record.state,)
                )

        return super(hr_overtime, self).unlink()

    @api.onchange("employee_id")
    def onchange_parent(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        for i in self:
            if i.employee_id:
                i.is_hod = i.employee_id.parent_id.user_id.id in user_ids
                i.parent_id = i.employee_id.parent_id

    @api.onchange("time_start", "time_stop", "time_stop2", "time_start2")
    def onchange_time(self):
        start2 = self.time_start2
        stop2 = self.time_stop2
        return {
            "value": {
                "durasi": (stop2 - start2) + (self.time_stop - self.time_start)
            }
        }

    # def get_time2(self, start, stop):
    #     start = start
    #     stop = stop
    #     durasi1 = 0
    #     if start and stop:
    #         start = datetime.strptime(
    #             str(start), "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         stop = datetime.strptime(
    #             str(stop), "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         delta = stop - start
    #         durasi1 = delta.total_seconds() / 3600
    #     return durasi1

    # @api.onchange("time_start", "time_stop", "time_start2", "time_stop2")
    # def onchange_time(self):
    #     start = self.time_start
    #     stop = self.time_stop
    #     durasi2 = 0
    #     durasi1 = self.get_time2(self.time_start2, self.time_stop2)
    #     if start and stop:
    #         start3 = datetime.strptime(
    #             str(start), "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         stop3 = datetime.strptime(
    #             str(stop), "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         delta = stop3 - start3
    #         durasi2 = delta.total_seconds() / 3600

    #     return {
    #         "value": {"date_overtime": self.time_start, "durasi": durasi1 + durasi2}
    #     }

    @api.one
    @api.constrains("durasi")
    def onchange_time_check(self):
        if self.durasi < 0:
            raise UserError("Jam akhir lebih kecil daripada jam awal")

    def get_compute_pagi(self, check_in, start, stop):
        if check_in > start:
            delta = check_in - stop
        else:
            delta = start - stop
        hasil = delta.total_seconds() / 3600
        return hasil

    def cek(self, data):
        _logger.info(data)

    def compute(self):
        def get_gaji_pokok():
            gaji = 0
            domain = [
                ('employee_id', '=', self.employee_id.id),
                '|',
                ('date_end', '=', False),
                ('date_end', '>=', self.date_overtime),
            ]
            hasil = self.env['hr.contract'].search(domain, limit=1)
            if hasil:
                gaji = hasil.wage
            return gaji
        obj_attendance = self.env['hr.attendance']
        obj_workingtime = self.env['resource.calendar.attendance']
        for overtime in self:
            date = overtime.date_overtime
            absen = obj_attendance.search([('check_in', '<=', date), ('check_in', '>=', date),
                                           ('check_out', '!=', False), ('employee_id', '=', overtime.employee_id.id)])
            check_out = absen.check_out
            check_in = absen.check_in
            waktu_kerja = obj_workingtime.search(
                [('tanggal', '=', date), ('calendar_id.working_time', '!=', False)])
            if absen and waktu_kerja:
                jam_masuk = waktu_kerja.hour_from
                jam_pulang = waktu_kerja.hour_to
                jam_check_in = check_in.time()
                jam_check_in = (jam_check_in.hour + jam_check_in.minute/60)+7
                jam_check_out = check_out.time()
                jam_check_out = (jam_check_out.hour + jam_check_out.minute/60)+7
                durasi_absen = jam_check_out - jam_pulang
                durasi_absen_pagi = 0
                if overtime.time_start2 and overtime.time_stop2:
                    durasi_absen_pagi = jam_masuk - jam_check_in
                overtime.durasi_absen = durasi_absen + durasi_absen_pagi
                durasi = durasi_absen
                if durasi_absen > overtime.durasi:
                    durasi = overtime.durasi
            dur_pro = overtime.durasi
            jam_kerja = overtime.type_overtime_id.pembagi
            progresif = overtime.durasi_pro(dur_pro)
            overtime.durasi_progresif = progresif
            amount = progresif * get_gaji_pokok() / jam_kerja if not overtime.type_overtime_id.fixed else progresif * overtime.type_overtime_id.fixed
            overtime.lembur = amount

    # def compute(self):
    #     for overtime in self:
    #         company = overtime.env.user.company_id
    #         code = company.code_overtime
    #         configuration_progresive = False
    #         localdict = {
    #             "durasi": overtime.durasi,
    #             "durasi_progresif": overtime.durasi_progresif,
    #             "time_start_pagi": overtime.time_start,
    #             "time_stop_pagi": overtime.time_stop,
    #             "time_start_sore": overtime.time_start2,
    #             "time_stop_sore": overtime.time_stop,
    #             "overtime": overtime,
    #             "env": self.env,
    #             "relativedelta": relativedelta,
    #             "datetime": datetime,
    #             "configuration_progresive": configuration_progresive,
    #             "print": self.cek,
    #         }
    #         safe_eval(code, localdict, mode="exec", nocopy=True)
    #         result = localdict["result"]
    #         overtime.lembur = result

    # @api.one
    # @api.multi
    # def compute(self):
    #     lembur = self.employee_id.golruang_id.gapok / 173
    #     transport = 0
    #     makan = 0
    #     real_durasi = 0
    #     amount = 0
    #     filters_overtime = [
    #         ("employee_id", "=", self.employee_id.id),
    #         ("check_out", ">=", self.date_overtime),
    #         ("check_in", "<=", self.date_overtime),
    #     ]

    #     filter_days = [
    #         (
    #             "tanggal",
    #             "=",
    #             self.date,
    #         ),
    #         ("status", "=", "kerja"),
    #     ]
    #     # mencari nilai working time
    #     working_time = self.env["hari.line"].search(filter_days)
    #     absen_overtime = self.env["hr.attendance"].search(filters_overtime, limit=1)
    #     print(absen_overtime.check_in)

    #     if not absen_overtime:
    #         raise UserError(
    #             _("Di SPL NO : %s Employee : %s Tidak Ada Absen pada hari %s ditemukan")
    #             % (self.name, self.employee_id.name, self.time_start)
    #         )

    #     berhenti = 0
    #     start = 0
    #     durasi_sore = 0.00
    #     if self.time_stop and self.time_start:

    #         stop2 = datetime.strftime(
    #             self.time_stop, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         start = datetime.strftime(
    #             self.time_start, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         stop = datetime.strftime(
    #             absen_overtime.check_out, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         if stop > stop2:
    #             berhenti = stop2
    #         else:
    #             berhenti = stop

    #         delta = berhenti - start

    #         durasi_sore = delta.total_seconds() / 3600

    #     durasi_pagi = 0.00
    #     if self.time_start2 and self.time_stop2:
    #         check_in = datetime.strftime(
    #             absen_overtime.check_in, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         start_pagi = datetime.strftime(
    #             self.time_start2, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         stop_pagi = datetime.strftime(
    #             self.time_stop2, "%Y-%m-%d %H:%M:%S"
    #         ) + relativedelta.relativedelta(hours=7)
    #         durasi_pagi = self.get_compute_pagi(check_in, start_pagi, stop_pagi)
    #         print(check_in)

    #     real_durasi = durasi_sore + abs(durasi_pagi)

    #     if real_durasi < 0.5:
    #         self.lembur = 0
    #     else:
    #         if working_time:

    #             if 0.5 <= real_durasi < 1.5:
    #                 amount = real_durasi * 1 * lembur

    #             elif 2.5 > real_durasi >= 1.5:
    #                 amount = real_durasi * 1.5 * lembur

    #             elif real_durasi >= 2.5:
    #                 amount = real_durasi * 2 * lembur

    #             if real_durasi >= 2:
    #                 makan = self.employee_id.golruang_id.tunj_makan / 22
    #         else:

    #             transport = self.employee_id.golruang_id.tunj_transport / 22
    #             if 8 > real_durasi >= 0.5:
    #                 amount = real_durasi * 1.5 * lembur
    #             else:
    #                 amount = real_durasi * 2 * lembur
    #             if real_durasi >= 4:
    #                 makan = self.employee_id.golruang_id.tunj_makan / 22

    #     self.lembur = amount + makan + transport

    @api.model
    def create(self, vals):
        # Replace '/' dengan sequence object sebenarnya
        vals["name"] = self.env["ir.sequence"].get("hr.overtime")
        return super(hr_overtime, self).create(vals)


class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    @api.multi
    def action_payslip_done(self):
        res = super(HrPayslip, self).action_payslip_done()
        filters = [("employee_id", "=", self.employee_id.id),
                   ("state", "=", "done")]
        for overtime in self.env["hr.overtime"].search(filters):
            overtime.write({"payslip_id": self.id, "state": "paid"})
        return res


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def _compute_manual_attendance(self):
        for employee in self:
            employee.manual_attendance = False

    @api.multi
    def _inverse_manual_attendance(self):
        manual_attendance_group = self.env.ref(
            "hr_attendance.group_hr_attendance")
        for employee in self:
            if employee.user_id:
                if employee.manual_attendance:
                    pass


# Tambah Field Text di Attendance>Configuration>Attendance
class ResConfig(models.TransientModel):
    _inherit = "res.config.settings"
    _description = "Custom Config Attendance"

    config_id = fields.Many2one("hr.overtime")
    code_overtime = fields.Text(
        string="Code", store=True, readonly=False, related="company_id.code_overtime"
    )


class Company(models.Model):
    _inherit = "res.company"

    code_overtime = fields.Text(
        string="Code Overtime",
    )


class WizardOvertime(models.TransientModel):
    _name = "wizard.overtime"
    _description = "Wizard Overtime"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def revisi_state_draft(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["hr.overtime"].browse(active_id)
            if project.write({"state": "draft"}):
                project.catatan_revisi = self.text_revisi
            else:
                project.catatan_revisi = self.text_revisi
