# -*- coding: utf-8 -*-
import datetime
from odoo import _, api, fields, models


class ResourceCalendar(models.Model):
    _inherit = "resource.calendar"

    bulan = fields.Char(string="Bulan")
    date_from = fields.Date(string="Starting Date")
    date_to = fields.Date(string="End Date")
    hari_kerja = fields.Integer("Hari kerja")
    working_time = fields.Boolean(string="Working Time")
    # attendance_ids = fields.One2many("resource.calendar.attendance", "name")

    @api.onchange("attendance_ids")
    def onchange_line(self):
        l = [x for x in self.attendance_ids if x.status == "kerja"]
        self.hari_kerja = len(l)

    @api.onchange("date_from", "date_to")
    def onchange_date(self):
        self.attendance_ids = False
        # angka = 0
        hari = 0
        if self.date_from and self.date_to:
            start = datetime.datetime.strptime(str(self.date_from), "%Y-%m-%d")
            end = datetime.datetime.strptime(str(self.date_to), "%Y-%m-%d")
            date_generated = [
                start + datetime.timedelta(days=x)
                for x in range(0, (end - start).days + 1)
            ]
            result = []
            for date in date_generated:
                x = date.strftime("%d-%m-%Y")
                tanggal = date.strftime("%Y-%m-%d")
                day = datetime.datetime.strptime(x, "%d-%m-%Y").weekday()
                if not day > 4:
                    hari += 1
                    data = {
                        "dayofweek": str(day),
                        "tanggal": tanggal,
                        "name": hari,
                        "day_period": "morning",
                        "status": "kerja",
                        "hour_from": 9.00,
                        "hour_to": 17.00,
                    }
                    result.append((0, 0, data))
            return {"value": {"attendance_ids": result, "hari_kerja": hari}}


class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    dayofweek = fields.Selection(
        [
            ("0", "Senin"),
            ("1", "Selasa"),
            ("2", "Rabu"),
            ("3", "Kamis"),
            ("4", "Jumat"),
            ("5", "Sabtu"),
            ("6", "Minggu"),
        ],
        "Day of Week",
        required=True,
        index=True,
        default="0",
    )

    status = fields.Selection(
        [
            ("kerja", "Kerja"),
            ("libur", "Libur"),
        ],
        "Status",
    )

    keterangan = fields.Char("Keterangan")
    tanggal = fields.Date(string="Tanggal")
