# -*- coding: utf-8 -*-
{
    "name": "CUSTOM HR WORKING TIME",
    "category": "Custom Modules",
    "summary": """
       Custom Module HR untuk LPEM UI
       """,
    "description": """
        modul calendar kerja
    """,
    "author": "PT. Ismata Nusantara Abadi ",
    "website": "ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr", "hr_attendance"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/resource_calendar.xml",
        # "views/employee.xml",
    ],
    # only loaded in demonstration
}