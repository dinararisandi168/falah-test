# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, RedirectWarning, ValidationError


class AccountMove(models.Model):
    _inherit = "account.move"

    name = fields.Char(string='Journal ID', required=True, copy=False, default='/')
    ref = fields.Char(string='Journal No', copy=False)

class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    invoice_id = fields.Many2one('account.invoice', string='Invoice')

    @api.model
    def create(self, vals):
        res = super(AccountBankStatement, self).create(vals)
        if res.invoice_id:
            res.invoice_id.statement_id = res.id
        return res

    # def check_confirm_bank(self):
    #     res = super(AccountBankStatement, self).check_confirm_bank()

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    statement_journal_id = fields.Many2one('account.journal', string='Journal', domain=[('type', 'in', ['cash', 'bank'])])
    
    def action_create_statement(self):
        if not self.statement_journal_id:
            raise ValidationError(('Journal belum dipilih!'))
        line_ids = []
        
        line_ids = [(0,0, {
                "date": self.date_invoice,
                "name": 'Customer Invoice - ' + self.number,
                "amount": self.amount_total,
                "partner_id": self.partner_id.id,
                "account_id": self.account_id.id
                                    })]

        return {
            'name': ('Create Statement'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.bank.statement',
            'view_id': self.env.ref("account.view_bank_statement_form").id,
            'type': 'ir.actions.act_window',
            'context': {'default_name': self.number,
                        'default_invoice_id': self.id,
                        'default_journal_id': self.statement_journal_id.id,
                        'default_line_ids': line_ids,
                        'default_accounting_date': self.date_invoice,
                        },
            'target':'current'
            }    

class CurrencyRates(models.Model):
    _inherit = 'res.currency.rate'

    rate = fields.Float(digits=(12, 11), default=1.0, help='The rate of the currency to the currency of rate 1')
    kurs = fields.Monetary("IDR", default=1.0)

    @api.onchange('kurs')
    def _onchange_(self):
        self.rate = 1 / self.kurs

    