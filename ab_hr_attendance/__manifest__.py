# -*- coding: utf-8 -*-
{
    "name": "ab_hr_attendance",
    "summary": """
        Custom absen untuk FALAH """,
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. Ismata Nusantara Abadi",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr_attendance", "hr_contract", "ab_hr_working_time", "report_xlsx"],
    # always loaded
    "data": [
        "security/group.xml",
        "wizards/summary_attendance_views.xml",
        "security/ir.model.access.csv",
        "views/views.xml",
        "report/report.xml"
        # "report/report_detail_attendance.xml",
        # "report/cetak.xml",
    ],
}
