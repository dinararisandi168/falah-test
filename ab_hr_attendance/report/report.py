from odoo import models, fields, api, _
import time
from dateutil import relativedelta
import datetime
import base64
import os
from PyPDF2 import PdfFileMerger


class HrAttendanceDetailReport(models.TransientModel):
    _name = "hr.attendance.report"

    date_from = fields.Date(
        string="Date From",
        required=True,
        default=time.strftime("%Y-%m-01"),
    )
    date_to = fields.Date(
        string="Date To",
        required=True,
        default=str(
            datetime.datetime.now()
            + relativedelta.relativedelta(months=+1, day=1, days=-1)
        )[:10],
    )
    employee_id = fields.Many2one(
        "hr.employee",
        string="Employee",
        default=lambda self: self.env["hr.employee"].search(
            [("user_id", "=", self.env.uid)], limit=1
        ),
        states={"draft": [("readonly", False)]},
    )

    all = fields.Boolean("All?")
    filename = fields.Char("File Name")
    data_file = fields.Binary("File")

    @api.multi
    def print_detail(self):
        # return self.env['report'].get_action(self, 'training_odoo.laporan_sesi')
        return self.env.ref("ab_hr_attendance.print_attendance").report_action(self)

    # @api.multi
    # def print_detail(self):
    #     return self.env["report"].get_action(
    #         self, "ab_hr_attendance.print_attendance"
    #     )


class DetailAttendanceReport(models.AbstractModel):
    _name = "report.ab_hr_attendance.print_attendance"

    @api.multi
    def get_day(self, object):
        day = datetime.datetime.strptime(object, "%Y-%m-%d %H:%M:%S").weekday()
        dayofweek = [
            ("Senin"),
            ("Selasa"),
            ("Rabu"),
            ("Kamis"),
            ("Jumat"),
            ("Sabtu"),
            ("Minggu"),
        ]
        return dayofweek[int(day)]

    @api.multi
    def datang_awal(self, object):
        result = 0
        check_in = datetime.datetime.strptime(
            object.check_in, "%Y-%m-%d %H:%M:%S"
        ) + datetime.timedelta(hours=7)
        cekin = datetime.datetime.strptime(str(check_in)[11:16], "%H:%M")
        datang = datetime.datetime.strptime("08:00", "%H:%M")
        delta = datang - cekin
        val = delta.total_seconds() / 60
        if val >= 1:
            result = val
            return result

    @api.multi
    def get_time(self, object):
        if object:
            date = datetime.datetime.strptime(
                object, "%Y-%m-%d %H:%M:%S"
            ) + relativedelta.relativedelta(hours=7)
            time = datetime.datetime.strptime(str(date)[11:16], "%H:%M")
            return time.strftime("%H:%M")

    def get_float_time(self, object):
        result = "0"
        if object >= 1:
            menit = object / 60
            result = str(datetime.timedelta(hours=menit)).rsplit(":", 1)[0]
            return result

    def get_hr_attendance(self, date_from, to):
        sql = (
            """ SELECT id FROM hr_attendance WHERE check_in >= '%s' AND check_in <= '%s' order by check_in """
            % (date_from, to)
        )
        self.env.cr.execute(sql)
        result = [
            self.env["hr.attendance"].browse(x["id"])
            for x in self.env.cr.dictfetchall()
        ]
        return result

    @api.multi
    def get_lines(self, object):
        date = datetime.datetime.strptime(
            object.date_to, "%Y-%m-%d"
        ) + relativedelta.relativedelta(days=1)
        return self.get_hr_attendance(object.date_from, date)

    def get_employees(self, object):
        employees = []
        if object.all:
            date = datetime.datetime.strptime(
                object.date_to, "%Y-%m-%d"
            ) + relativedelta.relativedelta(days=1)
            result = self.get_hr_attendance(object.date_from, date)
            for x in result:
                if x.employee_id not in employees:
                    employees.append(x.employee_id)
        else:
            employees.append(object.employee_id)
        return sorted(employees, key=lambda employee: employee.name)

    @api.multi
    def get_object(self, object):
        return object

    def get_lembur_pagi(self, cekin, datang):
        delta = cekin - datang
        hasil = delta.total_seconds() / 60
        return hasil

    def get_lembur(self, object):
        result = 0
        pagi = 0
        cekin = ""
        cekot = ""
        if object.check_in:
            check_in = datetime.datetime.strptime(
                object.check_in, "%Y-%m-%d %H:%M:%S"
            ) + datetime.timedelta(hours=7)

            cekin = datetime.datetime.strptime(str(check_in)[11:16], "%H:%M")
            datang = datetime.datetime.strptime("08:00", "%H:%M")
            pagi = abs(self.get_lembur_pagi(cekin, datang))
        if object.check_out:
            check_out = datetime.datetime.strptime(
                object.check_out, "%Y-%m-%d %H:%M:%S"
            ) + datetime.timedelta(hours=7)
            cekot = datetime.datetime.strptime(str(check_out)[11:16], "%H:%M")
            pulang = datetime.datetime.strptime("17:00", "%H:%M")

        if cekin and cekot:
            delta = cekot - pulang
            val = delta.total_seconds() / 60
            if val >= 1:
                result = val + pagi
                return result

    def get_overtime(self, employee, chekin, chekot):
        if chekin and chekot:
            sql = (
                """ SELECT name,durasi FROM hr_overtime WHERE employee_id = '%s' AND time_start <= '%s' AND time_stop >= '%s' """
                % (employee, chekot, chekot)
            )
            self.env.cr.execute(sql)
            return self.env.cr.dictfetchall()

    def get_spl(self, object, employee):
        result = self.get_overtime(employee.id, object.check_out, object.check_in)
        if result:
            for x in result:
                return x["name"]

    def get_durasi_lembur(self, object, employee):
        result = self.get_overtime(employee.id, object.check_out, object.check_in)
        if result:
            for x in result:
                return x["durasi"] * 60

    def get_tidak_masuk(self, object, employee):
        result = 0
        filter_absen = [
            ("check_in", ">=", object.date_from),
            ("check_in", "<=", object.date_to),
            ("employee_id", "=", employee.id),
        ]
        sql = (
            """ SELECT hari_kerja FROM hr_working_time WHERE date_from >= '%s' AND date_to <= '%s '"""
            % (object.date_from, object.date_to)
        )
        self.env.cr.execute(sql)
        data_kerja = self.env.cr.fetchone()
        absen = len(self.env["hr.attendance"].search(filter_absen))
        if data_kerja and absen:
            result = data_kerja[0] - absen
            if result < 0:
                result = 0
            return result

    @api.model
    def render_html(self, docids, data=None):
        object_attendance = self.env["hr.attendance.report"].browse(docids)
        data_date = self.get_object(object_attendance)
        date_attendance = self.get_lines(object_attendance)
        data_employees = self.get_employees(object_attendance)
        data = {
            "lines": data_date,
            "employees": data_employees,
            "attendance": date_attendance,
            "get_time": self.get_time,
            "get_day": self.get_day,
            "get_float_time": self.get_float_time,
            "get_datang_awal": self.datang_awal,
            "get_lembur": self.get_lembur,
            "get_spl": self.get_spl,
            "get_durasi_lembur": self.get_durasi_lembur,
            "get_tidak_masuk": self.get_tidak_masuk,
        }
        return self.env["report"].render("ab_hr_attendance.ab_hr_attendance", data)
