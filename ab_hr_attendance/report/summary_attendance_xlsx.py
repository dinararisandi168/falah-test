from odoo import fields, models, api
from datetime import timedelta

class SummaryAttendanceXlsx(models.AbstractModel):
    _name = 'report.ab_hr_attendance.summary_attendance_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 10})
        worksheet = workbook.add_worksheet('Attendances Summary')
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 3, 20)
        worksheet.set_column(7, 7, 15)
        header = ['No', 'Name', 'Emp ID', 'Division', 'Day', 'Off', 'Absent', 'Non Clock', 'Sick', 'Leave', 'Izin(x)', 'Izin(mnt)', 'Late(x)', 'Late(mnt)', 'BaseOvt', 'Ovt']
        title = 'Time Periode Summary from'
        start_date = obj.start_date.strftime('%d %B %Y')
        end_date = obj.end_date.strftime('%d %B %Y')
        title2 = '%s to %s' %(start_date, end_date)
        worksheet.merge_range(0, 0, 0, 0 + len(header) - 1, title, heading_format)
        worksheet.merge_range(1, 0, 1, 0 + len(header) - 1, title2, heading_format)
        worksheet.write_row(3, 0, header, heading_format)

        NO = []
        NAME = []
        EMP_ID = []
        DIV = []
        DAY = []
        OFF = []
        ABSENT = []
        NON_CLOCK = []
        SICK = []
        LEAVE = []
        IZIN_X = []
        IZIN_MNT = []
        LATE_X = []
        LATE_MNT = []
        BASE_OVT = []
        OVT = []

        data = self.get_attendaces(obj.start_date, obj.end_date)
        off = self.get_off_day(obj.start_date, obj.end_date)
        no = 1
        for x in data:
            ovt = self.get_overtime(x['id'], obj.start_date, obj.end_date)
            NO.append(no)
            NAME.append(x['name'] or '')
            EMP_ID.append(x['nik'] or '')
            DIV.append(x['division'] or '')
            DAY.append(x['day'] or '-')
            OFF.append(off or '-')
            ABSENT.append(x['absent'] or '-')
            NON_CLOCK.append('-')
            SICK.append(x['sick'] or  '-')
            LEAVE.append(x['leave']  or '-')
            IZIN_X.append(x['izin_x'] or '-')
            IZIN_MNT.append('-')
            LATE_X.append(x['late_x'] or '-')
            LATE_MNT.append(x['late_mnt'] or '-')
            BASE_OVT.append(ovt[0]['durasi'] if ovt else '-')
            OVT.append(ovt[0]['progresif'] if ovt else '-')
            no += 1
        
        worksheet.write_column(4, 0, NO, text_no_border)
        worksheet.write_column(4, 1, NAME, text_no_border)
        worksheet.write_column(4, 2, EMP_ID, text_no_border)
        worksheet.write_column(4, 3, DIV, text_no_border)
        worksheet.write_column(4, 4, DAY, text_no_border)
        worksheet.write_column(4, 5, OFF, text_no_border)
        worksheet.write_column(4, 6, ABSENT, text_no_border)
        worksheet.write_column(4, 7, NON_CLOCK, text_no_border)
        worksheet.write_column(4, 8, SICK, text_no_border)
        worksheet.write_column(4, 9, LEAVE, text_no_border)
        worksheet.write_column(4, 10, IZIN_X, text_no_border)
        worksheet.write_column(4, 11, IZIN_MNT, text_no_border)
        worksheet.write_column(4, 12, LATE_X, text_no_border)
        worksheet.write_column(4, 13, LATE_MNT, text_no_border)
        worksheet.write_column(4, 14, BASE_OVT, text_no_border)
        worksheet.write_column(4, 15, OVT, text_no_border)

       
    def get_attendaces(self, start_date, end_date):
        sql ="""
            SELECT 
                emp.id,
                emp.name AS name,
                emp.nik AS nik,
                dp.name AS division,
                COALESCE(SUM(CASE WHEN att.attendance_category IS NULL THEN 1 END), 0) AS day,
                COALESCE(SUM(CASE WHEN att.attendance_category IN ('tanpa_keterangan', 'tdk_absen') THEN 1 END), 0) AS absent, 
                COALESCE(SUM(CASE WHEN att.attendance_category = 'sakit' THEN 1 END), 0) AS sick,
                COALESCE(SUM(CASE WHEN att.attendance_category = 'cuti' THEN 1 END), 0) AS leave,
                COALESCE(SUM(CASE WHEN att.attendance_category = 'izin' THEN 1 END), 0) AS izin_x,
                COALESCE(SUM(CASE WHEN att.attendance_category = 'terlambat' THEN 1 END), 0) AS late_x,
                COALESCE(SUM(CASE WHEN att.attendance_category = 'terlambat' THEN telat END), 0) AS late_mnt 
            FROM hr_attendance att
            LEFT JOIN hr_employee emp ON emp.id = att.employee_id
            LEFT JOIN hr_department dp ON dp.id = emp.department_id 
            WHERE DATE(att.check_in) BETWEEN '%s' AND '%s'
            GROUP BY emp.id, emp.name, dp.name
        """ % (start_date, end_date)

        self._cr.execute(sql)
        return self._cr.dictfetchall()

    def get_off_day(self, start_date, end_date):
        sql = """
            SELECT COUNT(*) FROM resource_calendar_attendance
            WHERE status = 'libur' AND tanggal BETWEEN '%s' AND '%s'
        """ %(start_date, end_date)
        
        self._cr.execute(sql)
        result = self._cr.dictfetchall()
        
        off_day = result[0]['count']

        delta = timedelta(days=1)
        weekend = 0
        while start_date <= end_date:
            if start_date.weekday() > 4:
                weekend+=1
            start_date += delta

        return off_day + weekend
    
    def get_overtime(self, employee_id, start_date, end_date):
        sql = """
            SELECT SUM(COALESCE(durasi, 0)) AS durasi, SUM(COALESCE(durasi_progresif,0)) AS progresif FROM hr_overtime
            WHERE employee_id = %s AND
            date_overtime BETWEEN '%s' AND '%s' AND
            state not in ('draft', 'cancel')
            GROUP BY employee_id 
        """ % (employee_id, start_date, end_date)
        
        self._cr.execute(sql)
        return self._cr.dictfetchall()
