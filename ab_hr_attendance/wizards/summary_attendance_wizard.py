from odoo import _, api, fields, models
from datetime import datetime
from dateutil import relativedelta

class SummaryAttendanceWizard(models.TransientModel):
    _name = 'summary.attendance.wizard'
    _description = 'Summary Attendate Wizard'

    start_date = fields.Date(string='Start Date', default=datetime.now().strftime('%Y-%m-01'))
    end_date = fields.Date(string='End Date', default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1)))

    @api.multi
    def action_download(self):
        return self.env.ref('ab_hr_attendance.report_attendance_xlsx').report_action(self)
    