# -*- coding: utf-8 -*-
{
    "name": "Custom Project Falah",
    "summary": """
       Submit Project Charter""",
    "description": """
        Custom modul project
    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base","calendar", "ab_role","hr", "project", "ab_attachment", 
    "ab_request_contract","uom", "hr_timesheet", "ab_role", "report_xlsx",
    "ab_roman_sequence", "account", "helpdesk_lite", "sale_timesheet","ab_minutes_meeting" , 
    "ab_account", "ab_send_email"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "security/security.xml",
        "views/views.xml",
        "views/role_member_list.xml",
        "data/data.xml",
        "views/closure_summary.xml",
        "views/solution_design.xml",
        "views/menu_contract.xml",
        "report/action_report.xml",
        "report/closure_summary_report.xml",
        "report/report_rab_modal.xml",
        "report/report_rab_quot.xml",
        "report/report_technique.xml"
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
