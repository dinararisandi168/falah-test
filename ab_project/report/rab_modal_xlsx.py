from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo.http import content_disposition, request
from odoo.modules.module import get_resource_path
from io import BytesIO as StringIO
import base64


class RabModalXlsx(models.AbstractModel):
    _name = 'report.ab_project.rab_modal_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        heading_format = workbook.add_format(
            {'align': 'center', 'valign': 'vleft', 'bold': True, 'size': 12})
        sub_heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True,
                                                  'size': 9, 'color': 'white', 'bg_color': '#4472c4', 'border_color': 'white', 'border': 1})
        bold_data = workbook.add_format({'bold': True})
        header_colum = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bg_color': '#4781fd', 'border': 1, 'bold': True})
        normal_num_bold_no_currency = workbook.add_format({'bold': True, 'num_format': '#,###0.00'})

        # add sheet
        worksheet = workbook.add_worksheet('RAB Modal Kerja')

        # add sheet
        row = 1
        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet.insert_image("B2", "logo.png", {"image_data": logo, "x_scale": 0.15, "y_scale": 0.15})

        # setting width of the column
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 5)
        worksheet.set_column('C:C', 45)
        worksheet.set_column('D:D', 10)
        worksheet.set_column('E:E', 10)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)

        worksheet.merge_range('B7:B8', 'No', header_colum)
        worksheet.merge_range(
            'C7:C8', 'RINCIAN BARANG DAN PEKERJAAN', header_colum)
        worksheet.merge_range('D7:D8', 'Satuan Ukur', header_colum)
        worksheet.merge_range('E7:E8', 'Volume', header_colum)
        worksheet.merge_range('F7:F8', 'Harga Per Satuan Ukur', header_colum)
        worksheet.merge_range('G7:G8', 'Jumlah Biaya', header_colum)

        no = 1
        number = []
        item = []
        spesifikasi = []
        mom = []
        qty_rab = []
        unit_price = []
        sub_total = []
        row = 9

        for x in obj:
            for y in x.project_rab_line:
                worksheet.merge_range('B2:G2', 'RAB ' + str(x.name), heading_format)
                worksheet.merge_range('B3:G3', 'Project Estimation Period: ' + str(x.start_date) + ' s/d ' + str(x.end_date), heading_format)
                worksheet.merge_range('B4:G4', 'Client Name: ' + str(x.main_con_id.name), heading_format)

                worksheet.write('B' + str(row), str(no), workbook.add_format({'align': 'center', 'border': 1}))
                worksheet.write('C' + str(row), y.templ_id.name, workbook.add_format({'border': 1}))
                worksheet.write('C' + str(row + 1), y.specification, workbook.add_format({'text_wrap': 'wrap'}))

                worksheet.write('D' + str(row), y.qty_rab, workbook.add_format({'align': 'center', 'border': 1}))
                worksheet.write('E' + str(row), y.uom_id.name, workbook.add_format({'align': 'center', 'border': 1}))
                worksheet.write('F' + str(row), y.unit_price, workbook.add_format({'border': 1, 'num_format': '"Rp"  #,##0'}))
                worksheet.write('G' + str(row), y.sub_total, workbook.add_format({'border': 1, 'num_format': '"Rp"  #,##0'}))
                no += 1

                row += 2

            row += 1
            worksheet.merge_range('C' + str(row) + ':F' + str(row), 'Jumlah', workbook.add_format({'border': 1}))
            worksheet.write('G' + str(row), x.amount_total, workbook.add_format({'border': 1, 'num_format': '"Rp"  #,##0'}))

            row += 1
            worksheet.merge_range('C' + str(row) + ':F' + str(row), 'Ppn 10%', workbook.add_format({'border': 1}))
            worksheet.write('G' + str(row), x.tax_mod, workbook.add_format({'border': 1, 'num_format': '"Rp"  #,##0'}))

            row += 1
            worksheet.merge_range('C' + str(row) + ':F' + str(row), 'Total', workbook.add_format({'border': 1}))
            worksheet.write('G' + str(row), x.total_mod, workbook.add_format({'border': 1, 'num_format': '"Rp"  #,##0'}))
