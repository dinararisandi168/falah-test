from odoo import api, fields, models
from odoo.addons.ab_role.models.models import _get_uid_from_group

class MenuContract(models.Model):
    _name = 'menu.contract'
    _inherit = 'mail.thread'
    _description = 'Kontrak Kesepakatan Project'

    name = fields.Char(string="Reference", readonly=True, default="/", track_visibility="onchange")
    project_id = fields.Many2one('project.project', string='Project Reference', required=True, track_visibility="onchange")
    no_contract = fields.Char(string='Nomor Kontrak', track_visibility="onchange")
    title_contract = fields.Char(string='Judul Kontrak', track_visibility="onchange")
    start_date = fields.Date(string='Period Kontrak', track_visibility="onchange")
    end_date = fields.Date(string='To', track_visibility="onchange")
    partner_id = fields.Many2one("res.partner", string="Nama PPK / Direktur", track_visibility="onchange")
    organization_id = fields.Many2one(string="Organization", related="partner_id.parent_id")
    position = fields.Char(string='Posisi', track_visibility="onchange")
    pic_project_ids = fields.Many2many("res.partner", "res_partner_pic_proj_rel", string="PIC Project", track_visibility="onchange")
    pic_project2_ids = fields.Many2many("res.partner", "res_partner_pic_proj2_rel", string="PIC Project", track_visibility="onchange")
    end_user_id = fields.Many2one("res.partner", string="End User", track_visibility="onchange")
    street_end_user = fields.Char(string="Alamat", related="end_user_id.street", track_visibility="onchange")
    street = fields.Char(string="Alamat Organization", related="partner_id.street", track_visibility="onchange")
    attachment_ids = fields.Many2many('ir.attachment', track_visibility="onchange")
    rab_quotation_line = fields.One2many("project.rab.quot", "contract_id", string="Project RAB(Quotation)", track_visibility="onchange")
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("review", "PM Reviewed"),
            ("done", "CEO Approved"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        track_visibility='onchange'
    )

    # PROJECT CHARTER
    user_id = fields.Many2one("res.users", string="Project Leader", track_visibility="onchange")
    main_con_id = fields.Many2one("res.partner", string="Main Con/Project Partner", track_visibility="onchange")
    date_prepared = fields.Date(string="Date Prepared", default=fields.Date.today(), track_visibility="onchange")
    partner_id = fields.Many2one('res.partner', string='Customer Name',track_visibility="onchange")
    customer_org_id = fields.Many2one("res.partner", string="Customer Organization", domain=[("customer", "=", True)], track_visibility="onchange")
    customer_id = fields.Many2one("res.partner", string="Customer Name")
    end_user_id = fields.Many2one("res.partner", string="End User", track_visibility="onchange")
    bin_item_id = fields.Many2one("res.partner", string="Bin Item", track_visibility="onchange")
    tag_ids = fields.Many2many("project.tags", string="Tags", oldname="categ_ids")

    start_date = fields.Date(string="Estimasi Project Period", track_visibility="onchange")
    end_date = fields.Date(string="To", track_visibility="onchange")
    project_duration = fields.Char(string="Project Duration", track_visibility="onchange", store=True)
    project_purpose = fields.Text(string="Project Purpose / Justification", track_visibility="onchange")
    project_desc = fields.Text(string="Project Description", track_visibility="onchange")
    hl_requirement = fields.Text(string="High Level Requirements", track_visibility="onchange")
    hl_risk = fields.Text(string="High Level Risk", track_visibility="onchange")
    hl_solution = fields.Text(string="High Level Solution Concept", track_visibility="onchange")
    assumption_work = fields.Text(string="Assumption Scope of Work", track_visibility="onchange")
    currency_id = fields.Many2one("res.currency", string="Currency", required=True, default=lambda self: self.env.user.company_id.currency_id)
    budget_assumption = fields.Monetary(string="Budget Assumption", currency_field="currency_id", track_visibility="onchange")
    target_cost = fields.Monetary(string="Target Project Cost", currency_field="currency_id", track_visibility="onchange")


    @api.multi
    def action_submit(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'menu.contract', 'approve', 'group', 'project.group_project_manager')
        self.write({"state": "submit"})

    @api.multi
    def action_review(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] Dear User, This is outstanding transaction that need your action to verify or approve. Thanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'menu.contract', 'approve', 'group', 'ab_role.group_direktur')
        self.write({"state": "review"})    

    @api.multi
    def action_done(self):
        user_ids = []

        pm_group = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        for i in pm_group:
            user_ids.append(i)
        pa_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_admin").id)
        for j in pa_group:
            user_ids.append(j)
        pl_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_leader").id)
        for k in pl_group:
            user_ids.append(k)
        
        no_double = []
        for i in user_ids:
            if i not in no_double:
                no_double.append(i)

        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in no_double]
        sendto_list = [self.env["res.users"].browse(uid).id for uid in no_double]
        for p in sendto_list:
            email = self.env['send.email.notification']
            sendto = self.env['res.users'].search([('id', '=', p)])
            email.send_contract_project(self.title_contract, self.no_contract, 
            self.partner_id.name, self.start_date, self.end_date, 'user', sendto)
        self.message_post(
            body="Contract has been approved by CEO!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "done"})    


    # untuk membuat outo save dari Reference
    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("menu.contract")
        return super(MenuContract, self).create(vals)

    @api.onchange('project_id')
    def _onchange_project(self):
        self.end_user_id = self.project_id.end_user_id.id
        self.street_end_user = self.project_id.end_user_id.street
        self.user_id = self.project_id.user_id.id
        self.main_con_id = self.project_id.main_con_id.id
        self.date_prepared = self.project_id.date_prepared
        self.partner_id = self.project_id.partner_id.id
        self.customer_org_id = self.project_id.customer_org_id.id
        self.end_user_id = self.project_id.end_user_id.id
        self.bin_item_id = self.project_id.bin_item_id.id
        self.tag_ids = [(6, 0, self.project_id.tag_ids.ids)]

        self.start_date = self.project_id.start_date
        self.end_date = self.project_id.end_date
        self.project_duration = self.project_id.project_duration
        self.project_purpose = self.project_id.project_purpose
        self.project_desc = self.project_id.project_desc
        self.hl_requirement = self.project_id.hl_requirement
        self.hl_risk = self.project_id.hl_risk
        self.hl_solution = self.project_id.hl_solution
        self.assumption_work = self.project_id.assumption_work
        self.budget_assumption = self.project_id.budget_assumption
        self.target_cost = self.project_id.target_cost
        self.currency_id = self.project_id.currency_id.id


    @api.onchange("project_id")
    def _change_line(self):
        if self:
            contract_line = self.env["project.project"].search([("id", "=", self.project_id.id)])
            if contract_line:
                for x in contract_line:
                    self.rab_quotation_line = x.rab_quot_line

class RabQuotation(models.Model):
    _inherit = "project.rab.quot"

    contract_id = fields.Many2one('menu.contract')


class NotesWizard(models.TransientModel):
    _name = "notes.wizard"
    _description = "Revision of Wizard"

    revision_text = fields.Text(string="Catatan Revisi")

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            request = self.env["menu.contract"].browse(active_id)
            request.catatan_revisi = self.revision_text
            request.state = "draft"

