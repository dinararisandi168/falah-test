from odoo import models, fields, api
from odoo.addons.ab_role.models.models import _get_uid_from_group


class RequirementsList(models.Model):
    _name = "requirments.list"
    _description = "Requirment List"

    name = fields.Char()
    functional = fields.Char(string="Functional")
    description = fields.Char(string="Description")
    mandatory = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
    ], string="Mandatory")
    remark = fields.Text(string="Remarks")
    project_id = fields.Many2one("project.project")


class ProjectProject(models.Model):
    _inherit = "project.project"

    note1 = fields.Text()
    note = fields.Text()
    requirments_line = fields.One2many("requirments.list", "project_id", string="Requirment List", track_visibility="onchange")
    specification_line = fields.One2many("specification.teknik", "project_id", string="Specification Teknik", track_visibility="onchange")
    project_rab_line = fields.One2many("project.rab", "project_id", string="Project RAB", track_visibility="onchange")
    rab_quot_line = fields.One2many("project.rab.quot", "project_id", string="Project RAB(Quotation)", track_visibility="onchange")
    amount_total = fields.Float(string="Total", store=True, compute="_amount_all", track_visibility="onchange")
    amount_total_quot = fields.Float(string="Jumlah", store=True, compute="_amount_all", track_visibility="onchange")
    tax_mod = fields.Float(string="Ppn 10%", store=True, compute="_amount_all", track_visibility="onchange")
    tax_req = fields.Float(string="Ppn 10%", store=True, compute="_amount_all", track_visibility="onchange")
    total = fields.Float(string="TOTAL", store=True, compute="_amount_all", track_visibility="onchange")
    total_mod = fields.Float(string="TOTAL", store=True, compute="_amount_all", track_visibility="onchange")

    @api.depends("project_rab_line.sub_total", "rab_quot_line.sub_total")
    def _amount_all(self):
        tax_ten = 0
        tax_ten_mod = 0
        for plan in self:
            cost_total = sum([i.sub_total for i in plan.project_rab_line])
            cost_total_quot = sum([i.sub_total for i in plan.rab_quot_line])
        tax_ten_mod = 0.1 * cost_total
        tax_ten = 0.1 * cost_total_quot
        self.update({
            "amount_total": cost_total,
            "tax_mod": tax_ten_mod,
            "total_mod": cost_total + tax_ten_mod,
            "amount_total_quot": cost_total_quot,
            "tax_req": tax_ten,
            "total": cost_total_quot + tax_ten,
        })

    @api.onchange("project_rab_line")
    def _ganti_productharga(self):
        index = 0
        urutan = 0
        harga_mk = []
        prod_mk = []
        for ambil in self.project_rab_line:
            isi = ambil.unit_price
            harga_mk.append(isi)
        for data in self.rab_quot_line:
            data.unit_price = harga_mk[index]
            index += 1
        
        for ambil_product in self.project_rab_line:
            vol = ambil_product.templ_id
            prod_mk.append(vol)
        for datas in self.rab_quot_line:
            datas.templ_id = prod_mk[urutan]
            urutan += 1

    def action_submit2(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', 'review', 'group', 'project.group_project_manager')
        return self.write({"state": "submit2"})

    def action_review2(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        return self.write({"state": "review2"})

    def action_verify2(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        return self.write({"state": "verify2"})

    def action_submitceo(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', 'review', 'group', 'ab_role.group_direktur')
        return self.write({"state": "approve2"})

    def action_approve(self):
        self.env['send.email.notification'].send(self.id, 'project.project', 'Submit', 'user', self.user_id)
        return self.write({"state": "ps", "project_on": "ps"})

    @api.onchange("specification_line")
    def _change_rab(self):
        line = []
        if self.project_rab_line:
            self.update({"project_rab_line": [(5, 0, 0)]})
        if self.rab_quot_line:
            self.update({"rab_quot_line": [(5, 0, 0)]})
        for i in self.specification_line:
            isi = {
                # "templ_id": i.product_id.id,
                "uraian": i.name,
                "qty_rab": i.quantity,
                "uom_id": i.uom_id.id,
                "specification": i.specification,
            }
            line.append(isi)
        if line:
            self.update({"project_rab_line": line})
            self.update({"rab_quot_line": line})

    # @api.multi
    # def write(self,vals):
    #     data = super(ProjectProject, self).write(vals)
    #     line = []
    #     print("==============================================================")
    #     for i in self:
    #         i.project_rab_line.unlink()
    #         i.rab_quot_line.unlink()
    #         # i.project_rab_line=[(5, 0, 0)]
    #         # i.rab_quot_line=[(5, 0, 0)]
    #         # i.update({"project_rab_line":[(5,0,0)]})
    #         # i.update({"rab_quot_line":[(5,0,0)]})
    #         for j in i.specification_line:
    #             isi = {
    #                 "object_id": j.device_id,
    #                 "qty_rab": j.quantity,
    #                 "uom_id": j.uom_id.id,
    #                 }
    #             line.append(isi)
    #         print("6789baisjbc89=====aknsj",line)
    #     if line:
    #         i.project_rab_line = line
    #         # self.update({"rab_quot_line": line})
    #     return data

    @api.multi
    def print_spek_tech(self):
        return self.env.ref('ab_project.cetak_report_technique').report_action(self)

    @api.multi
    def print_rab_mod(self):
        return self.env.ref('ab_project.cetak_rab_modal').report_action(self)

    @api.multi
    def print_rab_quot(self):
        return self.env.ref('ab_project.cetak_rab_quot').report_action(self)

    @api.multi
    def rab_modal_xlsx(self):
        return self.env.ref('ab_project.rab_modal_xlsx').report_action(self)

    @api.multi
    def rab_quotation_xlsx(self):
        return self.env.ref('ab_project.rab_quotation_xlsx').report_action(self)


class SpecificationTeknik(models.Model):
    _name = "specification.teknik"
    _description = "Specification Teknik"

    name = fields.Char(string='Uraian')
    specification = fields.Char("Spesifikasi")
    quantity = fields.Float("Jumlah")
    uom_id = fields.Many2one("uom.uom", string="Satuan")
    referensi = fields.Text(string='Referensi To Brosur', compute="_get_string", readonly=True,store=True)
    remark = fields.Text(string="Remarks")
    merk = fields.Char("Merk")
    attachment_ids = fields.Many2many('ir.attachment')
    project_id = fields.Many2one('project.project', string='Project')

    @api.depends("attachment_ids")
    def _get_string(self):
        solo = ""
        hasil = ""
        akhir = ""
        for me in self:
            for ids in me.attachment_ids:
                if ids.datas_fname:
                    solo = ids.datas_fname + ',\n '
                    hasil = hasil + solo
        akhir = hasil[:-2]
        # self.referensi=akhir
        self.update({"referensi": akhir})
        
    def action_show_details(self):
        self.ensure_one()
        view = self.env.ref('ab_project.spektek_form_view')
        return {
            'name': ('Upload Specification Technique'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'specification.teknik',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.id,
        }

class ProjectRab(models.Model):
    _name = "project.rab"
    _description = "Project RAB Modal Kerja"

    name = fields.Char(string='Name',default="RAB(Modal Kerja)")
    uraian = fields.Char(string='Item/Uraian Kegiatan')
    project_id = fields.Many2one("project.project", string="Project RAB")
    specification = fields.Text("Spesifikasi")
    templ_id = fields.Many2one("product.template", string="Product")
    qty_rab = fields.Float(string="Jumlah", readonly=True)
    uom_id = fields.Many2one("uom.uom", string="Satuan")
    unit_price = fields.Float(string="Harga Satuan")
    # , related="templ_id.standard_price"
    sub_total = fields.Float(string="Total", compute="_sub_total")
    delivery_date = fields.Date(string="Delivery Date")
    attachment_ids = fields.Many2many('ir.attachment')

    @api.onchange("templ_id")
    def _relat_price(self):
        for i in self:
            i.unit_price = i.templ_id.standard_price

    @api.depends("qty_rab", "unit_price")
    def _sub_total(self):
        for a in self:
            a.sub_total = a.qty_rab * a.unit_price

class ProjectRabQuot(models.Model):
    _name = "project.rab.quot"
    _description = "Project RAB Quot"

    name = fields.Char(string='Name',default="Project RAB Quot")
    uraian = fields.Char(string='Item/Uraian Kegiatan')
    project_id = fields.Many2one("project.project", string="Project RAB")
    specification = fields.Text("Spesifikasi")
    templ_id = fields.Many2one("product.template", string="Product")
    qty_rab = fields.Float(string="Jumlah")
    uom_id = fields.Many2one("uom.uom", string="Satuan")
    unit_price = fields.Float(string="Harga Satuan")
    sub_total = fields.Float(string="Total", compute="_sub_total")
    delivery_date = fields.Date(string="Delivery Date")
    # project_reference_line = fields.One2many("ir.attachment", "project_rab2_id")
    project_id = fields.Many2one("project.project", string="Project RAB")
    attachment_ids = fields.Many2many('ir.attachment')

    @api.depends("qty_rab", "unit_price")
    def _sub_total(self):
        for a in self:
            a.update({"sub_total": a.qty_rab * a.unit_price})

# class UploadForm(models.Model):
#     _name = 'form'
#     _description = 'form'
#     name = fields.Char(string='Name')
#     attachment_ids = fields.Many2many('ir.attachment', track_visibility="onchange")
    

class AttachmentRab(models.Model):
    _inherit = "ir.attachment"

    name = fields.Char()
    project_rab_id = fields.Many2one("project.rab")
    project_rab2_id = fields.Many2one("project.rab")
    reference = fields.Char(string="Reference")
    doc_type = fields.Binary(string="Document")
    unit_price = fields.Float(string="Unit Price")
