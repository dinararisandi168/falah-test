from odoo import api, fields, models

class AddMOM(models.Model):
    _name = 'add.mom'
    _description = 'Add MOM in Project'

    calendar_id = fields.Many2one('calendar.event', string='Meeting Subject')
    start_date = fields.Date(string='Start Date', related="calendar_id.start_date")
    stop_date = fields.Date(string='End Date',related="calendar_id.stop_date")
    date = fields.Char(string='Meeting Date', compute="_get_date")
    meeting_type = fields.Selection(string="Meeting Type", selection=[("manage", "Management"), ("project", "Project"), ("rutin", "Routine")], related="calendar_id.manage_type")
    user_id = fields.Many2one('res.users', string='Responsibility',related="calendar_id.responsibility_id")
    
    project_id = fields.Many2one('project.project', string='Related Project')
    
    @api.depends('calendar_id')
    def _get_date(self):
        statement = ""
        for record in self:
            for cal in record.calendar_id:
                if cal.allday:
                    statement = "%s sd %s" % (cal.start_date.strftime("%m %B %Y"), cal.stop_date.strftime("%m %B %Y"))
                else:
                    statement = "%s Durasi %i Jam" % (cal.start_datetime.strftime("%m %B %Y"),cal.duration)
            record.update({"date": statement})
