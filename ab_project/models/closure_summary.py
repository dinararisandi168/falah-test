# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from datetime import datetime
from odoo.addons.ab_role.models.models import _get_uid_from_group


class ClosureSummary(models.Model):
    _name = "closure.summary"
    _inherit = "mail.thread"
    _description = "Closure Summary"

    name = fields.Char(string="Name", track_visibility="onchange")
    project_id = fields.Many2one("project.project", string="Nama Project", track_visibility="onchange")
    contract_id = fields.Many2one('menu.contract', string='Judul Kontrak', track_visibility="onchange")
    no_contract = fields.Char(string="Nomor kontrak", track_visibility="onchange")
    ket_project_line = fields.One2many("project.summary", "summary_id", string="Proyek")
    # ket_project = fields.Text(string="keterangan Project", track_visibility="onchange")
    project_date = fields.Date(string="Mulai Project", related="contract_id.start_date", readonly=True, track_visibility="onchange")
    project_end = fields.Date(string="Project Selesai", related="contract_id.end_date", readonly=False, track_visibility="onchange")
    ruang_lingkup_line = fields.One2many("ruang.lingkup", "summary_id", string="Ruang Lingkup Pekerjaan")
    customer_id = fields.Many2one("res.partner", string="Organization", related="project_id.customer_org_id", track_visibility="onchange",)
    direktur_customer_id = fields.Many2one("res.partner", string="Direktur", track_visibility="onchange")
    pic_project_ids = fields.Many2many("res.partner", "res_partner_pic1_rel", string="PIC Project", track_visibility="onchange")
    # purchasing_ids = fields.Many2many("res.partner", "res_partner_purchasing_rel", string="Purchasing", track_visibility="onchange")
    alamat_customer = fields.Char(string="Alamat Organization", track_visibility="onchange")
    end_user_id = fields.Many2one("res.partner", string="End User", track_visibility="onchange")
    # leader_id = fields.Many2one("res.partner", string="Leader", track_visibility="onchange")
    pic_project2_ids = fields.Many2many("res.partner", "res_partner_pic2_rel", string="PIC Project", track_visibility="onchange")
    alamat_customer2 = fields.Char(string="Alamat", track_visibility="onchange")
    vendor_line = fields.One2many("vendor.summary", "summary_id", string="Vendor")
    scope_of_work = fields.Text(string="Scope Of Work", related="project_id.assumption_work", track_visibility="onchange")
    catatan_revisi = fields.Text(string="Catatan Revisi", track_visibility="onchange")
    # state = fields.Selection([("draft", "Pembuatan"), ("done", "Selesai")], string="Status", readonly=True, copy=False, default="draft", track_visibility="onchange")
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("review", "PM Reviewed"),
            ("done", "CEO Approved"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
    )

    @api.multi
    def action_submit(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'closure.summary', 'review', 'group', 'project.group_project_manager')
        self.write({"state": "submit"})

    @api.multi
    def action_review(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'closure.summary', 'approve', 'group', 'ab_role.group_direktur')
        self.write({"state": "review"})

    @api.multi
    def action_done(self):
        user_ids = []

        pm_group = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        for i in pm_group:
            user_ids.append(i)
        pa_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_admin").id)
        for j in pa_group:
            user_ids.append(j)
        pl_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_leader").id)
        for k in pl_group:
            user_ids.append(k)

        no_double = []
        for i in user_ids:
            if i not in no_double:
                no_double.append(i)

        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in no_double]
        sendto_list = [self.env["res.users"].browse(uid).id for uid in no_double]
        for p in sendto_list:
            email = self.env['send.email.notification']
            sendto = self.env['res.users'].search([('id', '=', p)])
            email.send_cs(self.contract_id.name, self.no_contract, 
            self.customer_id.name, 'user', sendto)
        self.message_post(
            body="Contract has been approved by CEO!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "done"})

    # state = fields.Selection([("draft", "Pembuatan"), ("confirm", "Konfirmasi"), ("done", "Selesai")],string="Status",readonly=True, copy=False,default="draft",track_visibility="onchange")

    @api.onchange("project_id")
    def _change_data(self):
        data = self.env["menu.contract"].search([("project_id", "=", self.project_id.id)], limit=1)
        if data:
            self.contract_id = data
            self.project_date = data.start_date
            self.no_contract = data.no_contract
            self.project_end = data.end_date
            # CLIENT COMPANY
            self.customer_id = data.organization_id
            self.direktur_customer_id = data.partner_id
            self.alamat_customer = data.street
            self.pic_project2_ids = [(6, 0, [y.id for y in data.pic_project2_ids])]
            # END USER
            self.end_user_id = data.end_user_id
            self.alamat_customer2 = data.street_end_user
            self.pic_project_ids = [(6, 0, [x.id for x in data.pic_project_ids])]

        uraian = self.env["project.rab"].search([("project_id", "=", self.project_id.id)], limit=1)
        isi_uraian = []
        if uraian:
            for line in uraian:
                isi = {"name": line.templ_id.name, }
            isi_uraian.append(isi)
            self.update({"ket_project_line": [(5, 0, 0)]})
            self.update({"ket_project_line": isi_uraian})

        vendor = self.env["product.partner"].search([("project_id", "=", self.project_id.id)], limit=1)
        isi_vendor = []
        # for data in self.project_id.partner_product_line:
        if vendor:
            for data in vendor:
                ise = {
                    "vendor_id": data.partner_id,
                    "pic_id": data.pic_id,
                    "product": data.produk,
                }
            isi_vendor.append(ise)
            self.update({"vendor_line": [(5, 0, 0)]})
            self.update({"vendor_line": isi_vendor})

    @api.multi
    def cetak_cs(self):
        return self.env.ref("ab_project.cetak_closure_summary").report_action(self)

    @api.multi
    def action_cancel(self):
        self.write({"state": "draft"})

    # @api.multi
    # def action_done(self):
    #     self.write({"state": "done"})

    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("c.s")
        return super(ClosureSummary, self).create(vals)


class ProyekSummary(models.Model):
    _name = "project.summary"
    _description = "Project Summary Closure Summary"

    name = fields.Char(string="Proyek")
    summary_id = fields.Many2one("closure.summary", string="Summary")


class RuangLingkup(models.Model):
    _name = "ruang.lingkup"
    _description = "Ruang Lingkup Closure Summary"

    name = fields.Char(string="Ruang Lingkup Pekerjaan")
    summary_id = fields.Many2one("closure.summary", string="Summary")


class VendorSummary(models.Model):
    _name = "vendor.summary"
    _description = "Vendors Summary"

    vendor_id = fields.Many2one("res.partner", string="Name")
    pic_id = fields.Many2one("res.partner", string="PIC")
    product = fields.Char(string="Product")
    summary_id = fields.Many2one("closure.summary", string="Summary")


class Wizard(models.TransientModel):
    _name = "catatan.wizard.cs"
    _description = "Wizard Revisi"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            closure = self.env["closure.summary"].browse(active_id)
            closure.write({"state": "draft"})
            closure.catatan_revisi = self.text_revisi
